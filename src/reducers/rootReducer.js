// you can combine multiple reducers see the commented code in this file
import { combineReducers } from 'redux';
// import { RESET, RESET_WORKFLOW } from '../types';
// import sidebarActive from './sidebarToggle';
import searchData from './searchReducer';
import auctions from './auctionReducer';
import profile from './profileReducer';


// Combine all reducers.
const appReducer = combineReducers({
  searchData,
  auctions,
  profile,
});

const rootReducer = (state, action) => {
  // RESET REDUCER
  // eslint-disable-next-line no-param-reassign
  //   if (action.type === RESET) state = undefined;
  //   // RESET WORKFLOW
  //   // eslint-disable-next-line no-param-reassign
  //   else if (action.type === RESET_WORKFLOW) {
  //     return appReducer({ ...state, workFlows: undefined }, action);
  //   }

  return appReducer(state, action);
};
export default rootReducer;
