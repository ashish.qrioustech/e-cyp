import {
  SET_SEARCH_DATA,
  SET_SEARCH_DROP,
  SET_SEARCH_SELECTION,
} from '../type';

const initialState = {
  list: [],
  search_drop: [],
  selection: {
    state: '',
    city: '',
    category: '',
    subcategory: '',
    contenthead1: '',
    fromprice: '1',
    toprice: '100000000000',
  },
};

const SearchReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_SEARCH_DATA:
      return {
        ...state,
        list: action.list,
      };
    case SET_SEARCH_DROP:
      return {
        ...state,
        search_drop: action.list,
      };
    case SET_SEARCH_SELECTION:
      return {
        ...state,
        selection: action.list,
      };
    default:
      return state;
  }
};

export default SearchReducer;
