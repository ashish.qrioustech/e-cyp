import {
  SET_PROFILE_DATA,
  SET_BIDS_ACTIVE_DATA,
  SET_BIDS_WON_DATA,
  SET_BIDS_LOST_DATA,
  SET_FAVOURITES_DATA, 
  SET_LANGUAGE
} from '../type';

const initialState = {
  user_profile: [],
  bids: {
    active_bids: [],
    lost_bids: [],
    won_bids: [],
  },
  favourites: [],
  language:[]
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_PROFILE_DATA:
      return {
        ...state,
        user_profile: action.list,
      };

    case SET_BIDS_ACTIVE_DATA:
      return {
        ...state,
        bids: { ...state.bids, active_bids: action.list },
      };

    case SET_BIDS_WON_DATA:
      return {
        ...state,
        bids: { ...state.bids, won_bids: action.list },
      };

    case SET_BIDS_LOST_DATA:
      return {
        ...state,
        bids: { ...state.bids, lost_bids: action.list },
      };

    case SET_FAVOURITES_DATA:
      return {
        ...state,
        favourites: action.list,
      };

      case SET_LANGUAGE:
        return {
        ...state,
        language: action.list
        };
        
              default:
      return state;
  }
};

export default profileReducer;
