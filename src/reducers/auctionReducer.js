import {
  SET_UPCOMMING_AUCTION,
  SET_NEXT_AUCTION,
  SET_CURRENT_AUCTION,
} from '../type';

const initialState = {
  current_auction: [],
  upcomming_auction: [],
  next_auction: [],
};

const auctionReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_UPCOMMING_AUCTION:
      return {
        ...state,
        upcomming_auction: action.list,
      };

    case SET_NEXT_AUCTION:
      return {
        ...state,
        next_auction: action.list,
      };
    case SET_CURRENT_AUCTION:
      return {
        ...state,
        current_auction: action.list,
      };
    default:
      return state;
  }
};

export default auctionReducer;
