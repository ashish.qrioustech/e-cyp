import React, { useEffect } from 'react';
import { useHistory, Route, Redirect } from 'react-router';
import {
  getAuctionToken,
  getTimeSpand,
  storeToken,
  storeTimeSpand,
} from '../../storeData';
import PropTypes from 'prop-types';

export default function PrivateRoute({ component: Component, ...rest }) {
  const history = useHistory();
  useEffect(() => {
    window.scrollTo(0, 120);
    const time = getTimeSpand();
    if (time) {
      // console.log(typeof parseInt(time) , typeof (new Date().getTime() - 86400000));
      if (parseInt(time) <= new Date().getTime() - 86400000) {
        storeToken();
        storeTimeSpand();
      }
    }
  }, [history.location.pathname]);
  return (
    <Route
      {...rest}
      render={(props) =>
        getAuctionToken() ? <Component {...props} /> : <Redirect to="/login" />
      }
    />
  );
}

PrivateRoute.prototype = {
  component: PropTypes.oneOfType([
    PropTypes.instanceOf(Array),
    PropTypes.instanceOf(Object),
  ]).isRequired,
};
