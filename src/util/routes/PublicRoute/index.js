import React, { useEffect } from 'react';
import { useHistory, Route, Redirect } from 'react-router';
import { getAuctionToken } from '../../storeData';
import PropTypes from 'prop-types';

export default function PublicRoute({ component: Component, ...rest }) {
  const history = useHistory();
  useEffect(() => {
    window.scrollTo(0, 0);
  }, [history.location.pathname]);
  return (
    <Route
      {...rest}
      render={(props) =>
        getAuctionToken() ? <Redirect to="/" /> : <Component {...props} />
      }
    />
  );
}

PublicRoute.prototype = {
  component: PropTypes.oneOfType([
    PropTypes.instanceOf(Array),
    PropTypes.instanceOf(Object),
  ]).isRequired,
};
