import React, { useEffect } from 'react';
import { useHistory, Route } from 'react-router';
// import { getAuctionToken } from '../../storeData';
import PropTypes from 'prop-types';
import { getTimeSpand, storeToken, storeTimeSpand } from '../../storeData';

export default function ProtectedRoute({ component: Component, ...rest }) {
  const history = useHistory();
  useEffect(() => {
    window.scrollTo(0, 0);
    const time = getTimeSpand();
    if (time) {
      // console.log(typeof parseInt(time) , typeof (new Date().getTime() - 86400000));
     
      if (parseInt(time) <= new Date().getTime() - 86400000) {
        storeToken();
        storeTimeSpand();
      }
    }
    // (1000 * 60 * 60 * 24)
  }, [history.location.pathname]);
  return <Route {...rest} render={(props) => <Component {...props} />} />;
}

ProtectedRoute.prototype = {
  component: PropTypes.oneOfType([
    PropTypes.instanceOf(Array),
    PropTypes.instanceOf(Object),
  ]).isRequired,
};
