import openSocket from 'socket.io-client';
let url = process.env.REACT_APP_SOCKET_URL
const  socket = openSocket(url);
const subscribeToTimer = (cb) => socket.on('sliservertime', timestamp => cb(null, timestamp));

const bidUpdate= (cb) => socket.on('bidAddtime', bidUpdateData => cb(null, bidUpdateData));
  

export {subscribeToTimer,bidUpdate}