const API_URL = process.env.REACT_APP_API;
//'https://forwardapi.auctionsoftware.com/mobileapi/'

export default {
  BASE_URL: {
    API: `${API_URL}`,
  },
  API: {
    LOGIN: {
      LOGIN: 'loginsave',
      REGISTERATION: 'register',
      FORGOT_PASSWORD: 'forgot_password',
    },
    SEARCH_AUCTION: {
      SEARCH: 'mobilesearch',
      DROPDOWN_LIST:'getoverallcatgeory',
    },
    AUCTIONS: {
      CURRENT_AUCTION: 'mobilesearch',
      UPCOMMING_AUCTION: 'mobilesearch',
      NEXT_AUCTION: 'mobilesearch',
    },
    PROFILE: {
      GET_PROFILE: 'getUserProfileDetails',
      GET_MYBIDS:'buyerDashboard',
      
    },
    LANGUAGE: {
      GET_LANGUAGE: `language/english`
    }
  },
};
