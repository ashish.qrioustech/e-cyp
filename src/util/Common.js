import moment from "moment";

const getTimeInterval = (starts, ends, serverTimeSocket) => {
 
    let dateEnded = ends;
    serverTimeSocket = serverTimeSocket;
    let duration = "";
    if(starts > serverTimeSocket) {
      let duration = moment.duration(moment(starts).diff(moment(serverTimeSocket)));      
     
      let timer = {};
      timer.days = Math.floor(duration.asDays()) + "D";
      timer.hours = duration.hours() + "H";
      timer.minutes = duration.minutes() + "M ";
      timer.seconds = duration.seconds() + "S ";
      timer.text = "STARTS";
      timer.smallText = "starts";
      return timer
    } else if (serverTimeSocket < dateEnded) {
      // duration = moment.duration(dateEnded - serverTimeSocket);
      // console.log("duration ,----", duration);
      // console.log(dateEnded.diff(serverTimeSocket, 'days'));
    

      let duration = moment.duration(moment(dateEnded).diff(moment(serverTimeSocket)));      
      
      let timer = {};
      timer.days = Math.floor(duration.asDays()) + "D";
      timer.hours = duration.hours() + "H";
      timer.minutes = duration.minutes() + "M ";
      timer.seconds = duration.seconds() + "S ";
      timer.text = "ENDS";
      timer.smallText = "Ends";
      return timer
    }else{
      let timer = {};
      timer.text = "CLOSED";
      timer.smallText = "Closed";
      return 'Closed'
    }
  };

  export {getTimeInterval}