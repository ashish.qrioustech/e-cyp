export const storeToken = (_token) => {
  if (_token) {
    localStorage.setItem('auctionToken', _token);
  } else {
    localStorage.removeItem('auctionToken');
  }
};

export const storeTempTokenOTP = (_token) => {
  if (_token) {
    localStorage.setItem('tempTokenOTP', _token);
  } else {
    localStorage.removeItem('tempTokenOTP');
  }
};
export const getTempTokenOTP = () => {
  return localStorage.getItem('tempTokenOTP');
};

export const storeTimeSpand = (_timespand) => {
  if (_timespand) {
    localStorage.setItem('LogTimeSpand', _timespand);
  } else {
    localStorage.removeItem('LogTimeSpand');
  }
};


export const getTimeSpand = () => {
  return localStorage.getItem('LogTimeSpand');
};

export const getAuctionToken = () => {
  return localStorage.getItem('auctionToken');
};

