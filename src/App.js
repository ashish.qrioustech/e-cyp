import React, { useEffect } from 'react';
import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import PublicRoute from './util/routes/PublicRoute';

import Login from './Pages/Loing/Login';
import SignUp from './Pages/Loing/SignUp';
import Otp from './Pages/Loing/OTP'
// import Header from './Components/Header';
// import Footer from './Components/Footer';
import PageComponent from './PageComponent';
import ForgotPasssword from './Pages/Loing/ForgotPasssword';
import {getLanguage} from './actions/profileAction';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

function App(props) {
  useEffect(()=>{
    console.log(props)
    props.getLanguage()
  },[])
  return (
    <div className="App">
      <BrowserRouter>
        <Switch>
          <PublicRoute path="/login" exact component={Login} />
          <PublicRoute path="/sign-up" component={SignUp} />
          <PublicRoute path="/forgot-password" component={ForgotPasssword} />
          <PublicRoute path="/otp" component={Otp} />
          <PageComponent />
        </Switch>
      </BrowserRouter>
    </div>
  );
}
const mapStateToProps = () => ({
  // currentAuctions: getCurrentAuctionSelecter(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getLanguage: getLanguage,
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(App);
