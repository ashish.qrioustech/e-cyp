import { getAuctionToken } from "../util/storeData";

export const getGlobalLangSelecter = (state) => state.profile.language;
export const getIsLoginStatus = () => getAuctionToken();



export const getCurrentAuctionSelecter = (state) =>
  state.auctions.current_auction;
export const getUpcommingAuctionSelecter = (state) =>
  state.auctions.upcomming_auction;

export const getgetDistrictSelecter = (state) =>
  Object.keys(state.searchData.search_drop).length !== 0 &&
  state.searchData.search_drop.constructor === Object
    ? state.searchData.search_drop.district.map((x) => {
        return { ...x, id: x.location_id, value: x.name };
      })
    : [];

export const getCitySelector = (state) =>
  Object.keys(state.searchData.selection.state).length !== 0 &&
  state.searchData.selection.state.constructor === Object
    ? state.searchData.search_drop.city.cityAdmin[
        state.searchData.selection.state.id
      ].map((x, index) => {
        return { id: index, value: x };
      })
    : [];

export const getTypeSelector = (state) =>
  Object.keys(state.searchData.search_drop).length !== 0 &&
  state.searchData.search_drop.constructor === Object
    ? state.searchData.search_drop.category_val.map((x) => {
        return { ...x, value: x.name };
      })
    : [];

export const getSubTypeSelector = (state) =>
  Object.keys(state.searchData.selection.category).length !== 0 &&
  state.searchData.selection.category.constructor === Object
    ? state.searchData.selection.category.filters.map((x) => {
        return { ...x, value: x.name };
      })
    : [];

export const getselectionSelector = (state) => state.searchData.selection;

export const getCatagorySelector = (state) =>
  Object.keys(state.searchData.search_drop).length !== 0 &&
  state.searchData.search_drop.constructor === Object
    ? state.searchData.search_drop.extraCategorey.map((x) => {
        return { ...x, value: x.name };
      })
    : [];

export const getuserProfileSelecter = (state) => state.profile.user_profile;

export const getBitActiveSelecter = (state) => state.profile.bids.active_bids;
export const getBitWonSelecter = (state) => state.profile.bids.won_bids;
export const getBitLostSelecter = (state) => state.profile.bids.lost_bids;

export const getFavouritesSelecter = (state) => state.profile.favourites;

export const getLanguages = (state) => state.common.userLanguages;
