import React from 'react';
import { Switch } from 'react-router-dom';
import PrivateRoute from './util/routes/PrivateRoute';

import ProtectedRoute from './util/routes/ProtectedRoute';

import Header from './Components/Header';
import Footer from './Components/Footer';
import Auctions from './Pages/All/Auctions';

import AfterBidProperties from './Pages/All/AfterBidProperties';

import MakeAnOffer from './Pages/All/MakeAnOffer';
import Home from './Pages/All/Home';
import InvestmentList from './Pages/All/InvestmentList';
import PropertiesShortList from './Pages/All/PropertiesShortList';
import Profile from './Pages/Profile/Profile';
import HowItWork from './Pages/All/HowItWork';
import AboutUs from './Pages/All/AboutUs';
import ContactUs from './Pages/All/ContactUs';
import SearchResult from './Pages/All/SearchResult';
import NewListing from './Pages/All/NewListing';
import BuyersGuide from './Pages/All/BuyersGuide';
import FAQ from './Pages/All/FAQ';
import WhyCyprus from './Pages/All/WhyCyprus';
import PlacesMostVisit from './Pages/All/PlacesMostVisit';
import CitiesDetails from './Pages/All/CitiesDetails';
import OurServices from './Pages/All/OurServices';
import AllCypursProperties from './Pages/All/AllCypursProperties';
import City from './Pages/All/City';
import NewsAndBlock from './Pages/All/NewsAndBlock';
import CyprusResidency from './Pages/All/CyprusResidency';
import SellWithUs from './Pages/All/SellWithUs';
import JoinOurTeam from './Pages/All/JoinOurTeam';
import InvestmentCommerciaProperties from './Pages/All/InvestmentCommerciaProperties';
export default function PageComponent() {
  return (
    <>
      <Header />
      <Switch>
        <ProtectedRoute path="/why-cyprus" exact component={WhyCyprus} />
        <ProtectedRoute
          path="/why-cyprus/:id"
          exact
          component={CitiesDetails}
        />
        <ProtectedRoute path="/our-services" exact component={OurServices} />
        <ProtectedRoute path="/investment-commercia-properties" exact component={InvestmentCommerciaProperties} />

        {/*  */}
        <ProtectedRoute
          path="/places-most-visit"
          exact
          component={PlacesMostVisit}
        />

        <ProtectedRoute path="/auctions" exact component={Auctions} />

        <ProtectedRoute
          path="/propertyview/:id"
          exact
          component={AfterBidProperties}
        />

        <ProtectedRoute
          path="/all-cypurs-properties"
          exact
          component={AllCypursProperties}
        />
        <ProtectedRoute
          path="/all-cypurs-properties/:id"
          exact
          component={City}
        />
        <ProtectedRoute path="/news_blog" exact component={NewsAndBlock} />
        <ProtectedRoute path="/sell-with-us" exact component={SellWithUs} />
        <ProtectedRoute
          path="/cyprus-residency-grid"
          exact
          component={CyprusResidency}
        />
        <ProtectedRoute path="/seller-guide" exact component={SellWithUs} />
        <ProtectedRoute path="/join-our-team" exact component={JoinOurTeam} />

        <ProtectedRoute path="/new-listing" exact component={NewListing} />

        <ProtectedRoute path="/buyers-guide" exact component={BuyersGuide} />
        <ProtectedRoute path="/faq" exact component={FAQ} />
        <PrivateRoute path="/profile/" component={Profile} />

        <ProtectedRoute path="/make-an-offer" exact component={MakeAnOffer} />
        <ProtectedRoute path="/" exact component={Home} />
        <ProtectedRoute
          path="/investment-list"
          exact
          component={InvestmentList}
        />
        <ProtectedRoute path="/search-result" exact component={SearchResult} />
        <ProtectedRoute
          path="/properties-short-list"
          exact
          component={PropertiesShortList}
        />
        <ProtectedRoute path="/how-it-works" exact component={HowItWork} />
        <ProtectedRoute path="/about-us" exact component={AboutUs} />
        <ProtectedRoute path="/contact" exact component={ContactUs} />
      </Switch>
      <Footer />
    </>
  );
}
