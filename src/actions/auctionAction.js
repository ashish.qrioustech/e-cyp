import {
  SET_UPCOMMING_AUCTION,
  SET_NEXT_AUCTION,
  SET_CURRENT_AUCTION,
} from '../type';
import fetchClient from '../util/axiosConfig';
import constants from '../util/constants';

export const setCurrentAuctionDataList = (list) => ({
  type: SET_CURRENT_AUCTION,
  list,
});
export const setNextAuctionDataList = (list) => ({
  type: SET_NEXT_AUCTION,
  list,
});
export const setUpcommingAuctionDataList = (list) => ({
  type: SET_UPCOMMING_AUCTION,
  list,
});
export const getCurrentAuctionApiAction = (param) => {
  return (dispatch) => {
    /** Here there will be api calling for Getting WorkFlow list */
    fetchClient
      .post(constants.API.AUCTIONS.CURRENT_AUCTION, param)
      .then((_response) => {
        fetchClient
          .post(constants.API.AUCTIONS.CURRENT_AUCTION, {
            ...param,
            storedate:
              _response.data.upcomingauctionlist &&
              _response.data.upcomingauctionlist.length
                ? _response.data.upcomingauctionlist.map((x) => x.up_date)
                : [],
          })
          .then((_res) => {
            if (_res.data.success === 'yes') {
              if (_res.data.results.length) {
                //   const { totalCount } = _response.data.data[0];
                dispatch(setUpcommingAuctionDataList(_res.data.results));
              } else {
                throw new Error('Next Auction List is Empty');
              }
            } else {
              throw new Error('Next Auction Api Failure');
            }
          })
          .catch((_error) => {
            // console.log(_error);
            dispatch(setUpcommingAuctionDataList([]));
          });
        if (_response.data.success ==='yes') {
          if (_response.data.results.length) {
            //   const { totalCount } = _response.data.data[0];
            dispatch(setCurrentAuctionDataList(_response.data.results));
          } else {
            throw new Error('Current Auction List is Empty');
          }
        } else {
          throw new Error('Current Auction Api Failure');
        }
      })
      .catch((_error) => {
        // console.log(_error);
        dispatch(setCurrentAuctionDataList([]));
      });
  };
};
// export const getUpcommingAuctionApiAction = (param) => {
//   return (dispatch) => {
//     /** Here there will be api calling for Getting WorkFlow list */
//     fetchClient
//       .post(constants.API.AUCTIONS.CURRENT_AUCTION, param)
//       .then((_response) => {
//         if (_response.data.success ==='yes') {
//           if (_response.data.results.length) {
//             //   const { totalCount } = _response.data.data[0];
//             dispatch(setUpcommingAuctionDataList(_response.data.results));
//           } else {
//             throw new Error('Upcomming Auction List is Empty');
//           }
//         } else {
//           throw new Error('Upcomming Auction Api Failure');
//         }
//       })
//       .catch((_error) => {
//         // console.log(_error);
//         dispatch(setUpcommingAuctionDataList([]));
//       });
//   };
// };
export const getNextAuctionApiAction = (param) => {
  return (dispatch) => {
    /** Here there will be api calling for Getting WorkFlow list */
    fetchClient
      .post(constants.API.AUCTIONS.CURRENT_AUCTION, param)
      .then((_response) => {
        if (_response.data.success ==='yes') {
          if (_response.data.results.length) {
            //   const { totalCount } = _response.data.data[0];
            dispatch(setNextAuctionDataList(_response.data.results));
          } else {
            throw new Error('Next Auction List is Empty');
          }
        } else {
          throw new Error('Next Auction Api Failure');
        }
      })
      .catch((_error) => {
        // console.log(_error);
        dispatch(setNextAuctionDataList([]));
      });
  };
};
