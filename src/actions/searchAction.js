import {
  SET_SEARCH_DATA,
  SET_SEARCH_DROP,
  SET_SEARCH_SELECTION,
} from '../type';
import fetchClient from '../util/axiosConfig';
import constants from '../util/constants';

export const setSearchDataList = (list) => ({
  type: SET_SEARCH_DATA,
  list,
});

export const setSearchDropList = (list) => ({
  type: SET_SEARCH_DROP,
  list,
});

export const setSelectionListAction = (list) => ({
  type: SET_SEARCH_SELECTION,
  list,
});

export const getSearchDropdownListApiAction = () => {
  return (dispatch) => {
    /** Here there will be api calling for Getting WorkFlow list */
    fetchClient
      .get(constants.API.SEARCH_AUCTION.DROPDOWN_LIST)
      .then((_response) => {
        if (_response.data.success) {
          dispatch(setSearchDropList(_response.data));
        } else {
          throw new Error('Search Api Failure');
        }
      })
      .catch((_error) => {
        // console.log(_error);
        dispatch(setSearchDropList([]));
      });
  };
};

export const getSearchDataListApiAction = (param) => {
  return (dispatch) => {
    /** Here there will be api calling for Getting WorkFlow list */
    fetchClient
      .get(constants.API.SEARCH_AUCTION.SEARCH, param)
      .then((_response) => {
        if (_response.data.success) {
          if (_response.data.data.length) {
            //   const { totalCount } = _response.data.data[0];
            dispatch(setSearchDataList(_response.data.data));
          } else {
            throw new Error('Search List is Empty');
          }
        } else {
          throw new Error('Search Api Failure');
        }
      })
      .catch((_error) => {
        // console.log(_error);
        dispatch(setSearchDataList([]));
      });
  };
};
                        