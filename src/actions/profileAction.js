import Axios from 'axios';

import {
  SET_PROFILE_DATA,
  SET_BIDS_ACTIVE_DATA,
  SET_BIDS_WON_DATA,
  SET_BIDS_LOST_DATA,
  SET_FAVOURITES_DATA,
  SET_LANGUAGE
} from '../type';

import {
  Bids_active,
  Bids_won,
  Bids_lost,
  Favourites,
} from '../util/DemoData/bidDemoData';

import fetchClient from '../util/axiosConfig';
import constants from '../util/constants';

export const setLanguage = (list) => ({
  type:SET_LANGUAGE,
  list,
});

export const setUserProfileData = (list) => ({
  type: SET_PROFILE_DATA,
  list,
});

export const setBidActiveData = (list) => ({
  type: SET_BIDS_ACTIVE_DATA,
  list,
});

export const setBidWonData = (list) => ({
  type: SET_BIDS_WON_DATA,
  list,
});
export const setBidLostData = (list) => ({
  type: SET_BIDS_LOST_DATA,
  list,
});
export const setFavouritesData = (list) => ({
  type: SET_FAVOURITES_DATA,
  list,
});

const token =  localStorage.getItem('auctionToken');
export const getLanguage = () => {
  return (dispatch) => {
    let language = localStorage.getItem('language') ? localStorage.getItem('language') :'English' 
    Axios
    .get(`https://forwardapi.auctionsoftware.com/mobileapi/langauge/${language}`,{},{headers:  {"Authorization":`Bearer ${token}`,"domain":"cyprus.auctionsoftware.com"}})
    .then((_response)=>{
     console.log('********************** RESP **************');
      //console.log(_response.data.returndata.English)
      // console.log(_response) 
      dispatch(setLanguage(_response.data.returndata[language]))
    }).catch((error)=>{
    })
  } 
};

export const getProfileDataApiAction = () => {
  return (dispatch) => {
    /** Here there will be api calling for Getting WorkFlow list */
    fetchClient
      .get(constants.API.PROFILE.GET_PROFILE)
      .then((_response) => {
        if (_response.data.success === 'yes') {
          // if (_response.data.results.length) {
          //   const { totalCount } = _response.data.data[0];
          dispatch(setUserProfileData(_response.data.data));
          // } else {
          //   throw new Error('Profile List is Empty');
          // }
        } else {
          throw new Error('Profile Api Failure');
        }
      })
      .catch((_error) => {
        // console.log(_error);
        dispatch(setUserProfileData([]));
      });
  };
};

export const getMyBidsDataApiAction = (Req, Type) => {
  return (dispatch) => {
    /** Here there will be api calling for Getting WorkFlow list */
    fetchClient
      .post(constants.API.PROFILE.GET_MYBIDS, Req)
      .then((_response) => {
        // if (_response.data.success === 'yes') {
          if (Type === 'active') {
            // dispatch(setBidActiveData(_response.data.results));
            dispatch(setBidActiveData(Bids_active.results));
          } else if (Type === 'won') {
            // dispatch(setBidWonData(_response.data.results));
            dispatch(setBidWonData(Bids_won.results));
          } else if (Type === 'lost') {
            // dispatch(setBidLostData(_response.data.results));
            dispatch(setBidLostData(Bids_lost.results));
          } else if (Type === 'fav') {
            // dispatch(setFavouritesData(_response.data.results));
            dispatch(setFavouritesData(Favourites.results));
          }
        // } else {
        //   throw new Error('Bids Api Failure');
        // }
      })
      .catch((_error) => {
        // console.log(_error);
        dispatch(setUserProfileData([]));
      });
  };
};
