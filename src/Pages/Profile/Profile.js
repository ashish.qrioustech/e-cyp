import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { NavLink, Switch, Route, Link } from 'react-router-dom';
import $ from 'jquery';
import UserProfile from './SubComponents/UserProfile';
import MyBits from './SubComponents/MyBits';
import MyFavourites from './SubComponents/MyFavourites';
import Payment from './SubComponents/Payment';
import Notifications from './SubComponents/Notifications';
import EditProfile from './SubComponents/EditProfile';

import { getProfileDataApiAction } from '../../actions/profileAction';

// const $ = window.$;
function Profile({ getProfileDataApi }) {
  useEffect(() => {
    // getProfileDataApiAction();
    // console.log(window.location);
    getProfileDataApi();
    $('a.active').parent('li').addClass('active-nav');
  }, []);
  const handleChageTab = (e) => {
    $('.dashbard_nav_items ul li').removeClass('active-nav'); //Remove any "active" class
    $(e.target).parent('li').addClass('active-nav');
  };
  return (
    <main>
      <div className="container">
        <div className="dashboad_row row">
          <div className="col-md-3 col-lg-3">
            <div className="dashboard_title">
              <h4>DASHBORD</h4>
            </div>
            <div className="dashboard_profile_img text-center">
              <img src="../images/profile-img.jpg" alt="profile" />
              <Link
                to="/profile/edit-image"
                onClick={handleChageTab}
                className="edit_btn_profile"
              >
                <img src="../images/edit-btn-profile.svg" alt="alt-img"  />
              </Link>
            </div>
            <div className="dashbard_nav_items">
              <ul>
                <li>
                  <Link
                    to="/profile/"
                    className={
                      window.location.pathname === '/profile/' ||
                      window.location.pathname === '/profile'
                        ? 'active'
                        : ''
                    }
                    onClick={handleChageTab}
                  >
                    Profile
                  </Link>
                </li>
                <li>
                  <NavLink to="/profile/my-bids" onClick={handleChageTab}>
                    My Bids
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/profile/favourites" onClick={handleChageTab}>
                    Favourites
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/profile/payments" onClick={handleChageTab}>
                    {' '}
                    Payments
                  </NavLink>
                </li>
                <li>
                  <NavLink to="/profile/notifications" onClick={handleChageTab}>
                    Notifications
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-md-9 col-lg-9">
            <Switch>
              <Route path="/profile/" exact component={UserProfile} />
              <Route path="/profile/my-bids" exact component={MyBits} />
              <Route
                path="/profile/favourites"
                exact
                component={MyFavourites}
              />
              <Route path="/profile/payments" exact component={Payment} />
              <Route
                path="/profile/notifications"
                exact
                component={Notifications}
              />
              <Route path="/profile/edit-image" exact component={EditProfile} />
            </Switch>
          </div>
        </div>
      </div>
    </main>
  );
}

Profile.defaultProps = {};
Profile.propTypes = {
  getProfileDataApi: PropTypes.func.isRequired,
  // currentAuctions: PropTypes.instanceOf(Array),
};
const mapStateToProps = () => ({
  // currentAuctions: getCurrentAuctionSelecter(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getProfileDataApi: getProfileDataApiAction,
    },
    dispatch
  );
export default connect(mapStateToProps, mapDispatchToProps)(Profile);
