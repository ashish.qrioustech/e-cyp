import React from 'react';

export default function Payment() {
  return (
    <div className="bids_detail_seller_dash">
      <ul className="nav nav-tabs" id="myTab" role="tablist">
        <li className="nav-item">
          <a
            className="nav-link active"
            id="home-tab"
            data-toggle="tab"
            href="#payments"
            role="tab"
            aria-controls="payments"
            aria-selected="true"
          >
            Payments
          </a>
        </li>
        <li className="nav-item">
          <a
            className="nav-link"
            id="my_card-tab"
            data-toggle="tab"
            href="#my_card"
            role="tab"
            aria-controls="my_card"
            aria-selected="false"
          >
            My Card
          </a>
        </li>
      </ul>
      <div className="tab-content" id="myTabContent">
        <div
          className="tab-pane fade show active"
          id="payments"
          role="tabpanel"
          aria-labelledby="payments-tab"
        >
          <div className="bid_table_view_Seller">
            <table>
              <tbody>
                <tr>
                  <th>#</th>
                  <th>Lot No</th>
                  <th>Property Name</th>
                  <th>Category</th>
                  <th>Bought on</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                <tr>
                  <td>01</td>
                  <td>
                    <span>1234567</span>
                  </td>
                  <td>Penthouse in Lima...</td>
                  <td>Residential</td>
                  <td>02/02/2020</td>
                  <td>€1000</td>
                  <td>Paid</td>
                  <td>
                    <button className="btn_view">View</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
        <div
          className="tab-pane fade"
          id="my_card"
          role="tabpanel"
          aria-labelledby="my_card-tab"
        >
          .....................
        </div>
      </div>
    </div>
  );
}
