import React from 'react';

export default function Notifications() {
  return (
    <div className="bids_detail_seller_dash">
      <div className="bid_table_view_Seller bid_table_favourite_pg notifications_pg">
        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et magn.
        </p>
        <table>
          <tbody>
            <tr>
              <th>#</th>
              <th>Notifications</th>
              <th>Email</th>
            </tr>
            <tr>
              <td>01</td>
              <td>Bid Submitted</td>
              <td>
                <label className="container-notify">
                  <input type="checkbox" defaultChecked="checked-notify" />
                  <span className="checkmark-notify" />
                </label>
              </td>
            </tr>
            <tr>
              <td>02</td>
              <td>Please make the payment for the product you won</td>
              <td>
                <label className="container-notify">
                  <input type="checkbox" defaultChecked="checked-notify" />
                  <span className="checkmark-notify" />
                </label>
              </td>
            </tr>
            <tr>
              <td>03</td>
              <td>You are outbid on product.</td>
              <td>
                <label className="container-notify">
                  <input type="checkbox" defaultChecked="checked-notify" />
                  <span className="checkmark-notify" />
                </label>
              </td>
            </tr>
            <tr>
              <td>04</td>
              <td>New product added</td>
              <td>
                <label className="container-notify">
                  <input type="checkbox" defaultChecked="checked-notify" />
                  <span className="checkmark-notify" />
                </label>
              </td>
            </tr>
          </tbody>
        </table>
        <div className="text-center">
          <button className="btn_save_noty">Save</button>
        </div>
      </div>
    </div>
  );
}
