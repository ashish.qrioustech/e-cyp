import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import { getuserProfileSelecter } from '../../../selector/selector';

function UserProfile({userProfile}) {

  // useEffect(() => {
  // // console.log(userProfile)
  // }, [userProfile])
  return (
    <div className="profile_form">
      <h2>Profile</h2>
      <form action className>
        <div className="form-group d-flex align-items-center">
          <label htmlFor="Email" style={{ marginBottom: 0 }}>
            Salutation:
          </label>
          <div className="Country_code">
            <select name="CountryCode" id="CountryCode">
              <option value="mr">Mr</option>
              <option value="mrs">Mrs</option>
              <option value="miss">Miss</option>
            </select>
          </div>
        </div>
        <div className="row">
          <div className="col-lg-6 col-md-6">
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">First Name</label>
              <input
                type="text"
                className="form-login"
                id="exampleInputEmail1"
                placeholder="Enter your first name"
                value={userProfile.first_name}
              />
            </div>
          </div>
          <div className="col-lg-6 col-md-6">
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Last Name</label>
              <input
                type="text"
                className="form-login"
                id="exampleInputEmail1"
                placeholder="Enter your last name"
                value={userProfile.last_name}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6 col-lg-6">
            <div className="form-group">
              <label htmlFor="exampleInputEmail1">Preferred Language</label>
              <select name="LanguageSelect" id="LanguageSelect">
                <option value="english">English</option>
                <option value="russian">Russian</option>
                <option value="greek">Greek</option>
              </select>
            </div>
          </div>
          <div className="col-md-6 col-lg-6">
            <label htmlFor="exampleInputEmail1">Phone</label>
            <div className="d-flex align-item-center phone_number_seller">
              <select value={userProfile.country}>
                <option value={+357}>Cyprus (+357)</option>
                <option value={+91}>+91</option>
              </select>
              <input
                type="email"
                className="form-login"
                id="exampleInputEmail1"
                aria-describedby="emailHelp"
                placeholder="Enter your phone number"
                value={userProfile.phone}
              />
            </div>
          </div>
        </div>
        <h3>Update email</h3>
        <div className="form-group" style={{ marginBottom: 32 }}>
          <div className="row d-flex align-items-end">
            <div className="col-md-6">
              <label htmlFor="exampleInputEmail1">Email</label>
              <input
                type="email"
                className="form-login"
                id="exampleInputEmail1"
                placeholder="Enter your email"
              />
            </div>
            <button className="seller_email_upload_btn ml-auto mr-3">Update</button>
          </div>
        </div>
        <div className="change_password">
          <h3>Change Password</h3>
          <label className="switch-ch_pass">
            <input type="checkbox" />
            <span className="slider-ch_pass round" />
          </label>
        </div>
        <div className="row">
          <div className="col-md-6 col-lg-6">
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Current Password</label>
              <input
                type="password"
                className="form-login"
                id="confirm_Password"
                name="confirm_Password"
                placeholder="Confirm new password"
              />
            </div>
          </div>
          <div className="col-md-6 col-lg-6"></div>
        </div>
        <div className="row">
          <div className="col-md-6 col-lg-6">
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">New Password</label>
              <input
                type="password"
                className="form-login"
                id="confirm_Password" 
                name="confirm_Password"
                placeholder="Enter new password"
              />
            </div>
          </div>
          <div className="col-md-6 col-lg-6">
            <div className="form-group">
              <label htmlFor="exampleInputPassword1">Confirm Password</label>
              <input
                type="password"
                className="form-login"
                id="confirm_Password"
                name="confirm_Password"
                placeholder="Confirm new password"
              />
            </div>
          </div>
        </div>
        <div className="seller_submit_btn">
          <button className="seller_submit_btn_bt">Save</button>
        </div>
        {/* <div class="form-check reg_pg_check">
          <label class="check_box_cn">I agree to the <a href="#">trems &amp; conditions</a>
              <input type="checkbox" checked="checked">
              <span class="checkmark"></span>
            </label>
      </div> */}
      </form>
    </div>
  );
}


UserProfile.defaultProps = {
  userProfile:{}
};
UserProfile.propTypes = {
  // getProfileDataApi: PropTypes.func.isRequired,
  userProfile: PropTypes.instanceOf(Object),
};
const mapStateToProps = (state) => ({
  userProfile: getuserProfileSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(UserProfile);