import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

// import { getuserProfileSelecter } from '../../../selector/selector';

function EditProfile({userProfile}) {
  return (
    <div className="profile_form">
      <h2>Change Profile Image</h2>
      <form action className>
        <img src="../images/profile-img.jpg" alt className="mb-5" />
        <div className="tab_file_Upload">
          <div className="resume_upload">
            <input type="file" id="real-file" hidden="hidden" />
            <button
              type="button"
              id="custom-button"
              style={{
                color: '#fff',
                background: '#e31836',
                border: 'none',
                borderRadius: 5,
                padding: '13px 19px 11px',
                marginRight: 19,
              }}
            >
              Choose Image
            </button>
            <span id="custom-text">(No files Chosen)</span>
          </div>
          {/* <button>
            Choose Documents
          </button>
          <span>(No files chosen)</span> */}
        </div>
        <div className="seller_submit_btn">
          <button className="seller_submit_btn_bt">Save</button>
        </div>
        {/* <div class="form-check reg_pg_check">
          <label class="check_box_cn">I agree to the <a href="#">trems &amp; conditions</a>
              <input type="checkbox" checked="checked">
              <span class="checkmark"></span>
            </label>
      </div> */}
      </form>
    </div>
  );
}

EditProfile.defaultProps = {
  // userProfile:{}
};
EditProfile.propTypes = {
  // getProfileDataApi: PropTypes.func.isRequired,
  // userProfile: PropTypes.instanceOf(Array),
};
const mapStateToProps = () => ({
  // userProfile: getuserProfileSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
