import React, { useEffect } from 'react';
import { Tabs, Tab } from 'react-bootstrap';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { getMyBidsDataApiAction } from '../../../actions/profileAction';
import { getCurrentAuctionApiAction } from '../../../actions/auctionAction';

import {
  getBitActiveSelecter,
  getBitWonSelecter,
  getBitLostSelecter,
  getUpcommingAuctionSelecter,
} from '../../../selector/selector';

function MyBits({
  getMyBidsDataApi,
  getUpcommingAuctionApi,
  BitLost,
  BitActive,
  BitWon,
  Upcomming,
}) {
  const Req = { order: 1, page: 1, sh_limit: 10 };
  useEffect(() => {
    getMyBidsDataApi({ ...Req, status: 'bidactive' }, 'active');
    getMyBidsDataApi({ ...Req, status: 'bidlost' }, 'lost');
    getMyBidsDataApi({ ...Req, status: 'bidwon' }, 'won');
    getUpcommingAuctionApi();
  }, []);
  return (
    <div className="bids_detail_seller_dash">
      <Tabs defaultActiveKey="Bids_Active" id="uncontrolled-tab-example">
        <Tab eventKey="Bids_Active" title="Bids Active">
          <div className="bid_table_view_Seller">
            <table>
              <tbody>
                <tr>
                  <th>#</th>
                  <th>Lot No</th>
                  <th>Property Name</th>
                  <th>Category</th>
                  <th>Bid Placed</th>
                  <th>Bid Deposit</th>
                  <th>Time Left</th>
                  <th>My Bid</th>
                  <th>Action</th>
                </tr>
                {BitActive.length ? (
                  BitActive.map((x, index) => {
                    return (
                      <tr key={x.id}>
                        <td>{index + 1}</td>
                        <td>
                          <span>{x.lotof}</span>
                        </td>
                        <td>{x.auctionlot_title}</td>
                        <td>{x.category_id}</td>
                        <td>02/02/2020</td>
                        <td>€{x.rprice}</td>
                        <td>04D:12H:24M</td>
                        <td>€1250</td>
                        <td>
                          <button className="btn_view">View</button>
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr>
                    <td>Data not found</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </Tab>
        <Tab eventKey="Bids_Won" title="Bids Won">
          <div className="bid_table_view_Seller">
            <table>
              <tbody>
                <tr>
                  <th>#</th>
                  <th>Lot No</th>
                  <th>Property Name</th>
                  <th>Category</th>
                  <th>Deposit</th>
                  <th>Bought on</th>
                  <th>Price</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                {BitWon.length ? (
                  BitWon.map((x, index) => {
                    return (
                      <tr key={x.id}>
                        <td>{index + 1}</td>
                        <td>
                          <span>{x.lotof}</span>
                        </td>
                        <td>{x.auctionlot_title}</td>
                        <td>{x.category_id}</td>
                        <td>€{x.rprice}</td>
                        <td>02/02/2020</td>
                        <td>€{x.wprice}</td>
                        <td>{x.market_status}</td>
                        <td>
                          <button className="btn_view">View</button>
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr>
                    <td>Data not found</td>
                  </tr>
                )}
                <tr>
                  <td>3</td>
                  <td>
                    <span>1234567</span>
                  </td>
                  <td>Penthouse in Lima...</td>
                  <td>Residential</td>
                  <td>€500</td>
                  <td>02/02/2020</td>
                  <td>€1250</td>
                  <td>Paid</td>
                  <td>
                    <button className="btn_Paynow">Paynow</button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </Tab>
        <Tab eventKey="Bid_Lots" title="Bid Lots">
          <div className="bid_table_view_Seller">
            <table>
              <tbody>
                <tr>
                  <th>#</th>
                  <th>Lot No</th>
                  <th>Property Name</th>
                  <th>Category</th>
                  <th>Bid Placed</th>
                  <th>My Bid</th>
                  <th>Sold for</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                {BitLost.length ? (
                  BitLost.map((x, index) => {
                    return (
                      <tr key={x.id}>
                        <td>{index + 1}</td>
                        <td>
                          <span>{x.lotof}</span>
                        </td>
                        <td>{x.auctionlot_title}</td>
                        <td>{x.category_id}</td>
                        <td>€{x.rprice}</td>
                        <td>02/02/2020</td>
                        <td>€{x.wprice}</td>
                        <td>{x.market_status}</td>
                        <td>
                          <button className="btn_view">View</button>
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr>
                    <td>Data not found</td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </Tab>
        <Tab eventKey="Upcoming" title="Upcoming">
          <div className="bid_table_view_Seller">
            <table>
              <tbody>
                <tr>
                  <th>#</th>
                  <th>Lot No</th>
                  <th>Property Name</th>
                  <th>Category</th>
                  <th>Starts on</th>
                  <th>Location</th>
                  <th>Time left</th>
                  <th>Action</th>
                  <th />
                </tr>
                {Upcomming.length ? (
                  Upcomming.map((x, index) => {
                    return (
                      <tr key={x.id}>
                        <td>{index + 1}</td>
                        <td>
                          <span>{x.lotof}</span>
                        </td>
                        <td>{x.title}</td>
                        <td>{x.category_id}</td>
                        <td>02/02/2020</td>
                        <td>€{x.rprice}</td>
                        <td>04D:12H:24M</td>

                        {/* <td>{x.market_status}</td> */}
                        <td>
                          <button className="btn_view">View</button>
                        </td>
                      </tr>
                    );
                  })
                ) : (
                  <tr>
                    <td>Data not found</td>
                  </tr>
                )}
                {/* <tr>
                  <td>01</td>
                  <td>
                    <span>1234567</span>
                  </td>
                  <td>Penthouse in Lima...</td>
                  <td>Residential</td>
                  <td>02/02/2020</td>
                  <td>Limassol</td>
                  <td>04D:12H:24M</td>
                  <td>
                    <button className="btn_view">View</button>
                  </td>
                  <td>
                    <a href="#">
                      <img src="images/delete-24px.svg" alt="alt-img"  />
                    </a>
                  </td>
                </tr> */}
              </tbody>
            </table>
          </div>
        </Tab>
      </Tabs>
    </div>
  );
}

MyBits.defaultProps = {
  BitActive: [],
  BitWon: [],
  BitLost: [],
  Upcomming:[],
};
MyBits.propTypes = {
  getMyBidsDataApi: PropTypes.func.isRequired,
  getUpcommingAuctionApi: PropTypes.func.isRequired,
  BitActive: PropTypes.instanceOf(Object),
  BitWon: PropTypes.instanceOf(Object),
  BitLost: PropTypes.instanceOf(Object),
  Upcomming:PropTypes.instanceOf(Object)
};
const mapStateToProps = (state) => ({
  BitActive: getBitActiveSelecter(state),
  BitWon: getBitWonSelecter(state),
  BitLost: getBitLostSelecter(state),
  Upcomming: getUpcommingAuctionSelecter(state),

});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getMyBidsDataApi: getMyBidsDataApiAction,
      getUpcommingAuctionApi: getCurrentAuctionApiAction,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MyBits);
