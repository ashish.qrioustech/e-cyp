import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { getMyBidsDataApiAction } from '../../../actions/profileAction';

import { getFavouritesSelecter } from '../../../selector/selector';

function MyFavourites({ getMyFavouriteDataApi, Favourites }) {
  const Req = { order: 1, page: 1, sh_limit: 10 };
  useEffect(() => {
    getMyFavouriteDataApi({ ...Req, status: 'watchlist' }, 'fav');
  }, []);
  return (
    <div className="bids_detail_seller_dash">
      <div className="bid_table_view_Seller bid_table_favourite_pg">
        <table>
          <tbody>
            <tr>
              <th>#</th>
              <th>Lot No</th>
              <th>Property Name</th>
              <th>Category</th>
              <th>Start Price</th>
              <th>Location</th>
              <th className="text-center">Action</th>
            </tr>
            {Favourites.length ? (
              Favourites.map((x, index) => {
                return (
                  <tr key={x.id}>
                    <td>{index + 1}</td>
                    <td>
                      <span>1234567</span>
                    </td>
                    <td>
                      <a href="#">{x.title}</a>
                    </td>
                    <td>{x.auctioncondition}</td>
                    <td>€{x.rprice}</td>
                    <td>Limassol</td>
                    <td className="text-center">
                      <a href="#">
                        <img src="images/delete-24px.svg" alt="alt-img"  />
                      </a>
                    </td>
                  </tr>
                );
              })
            ) : (
              <tr>
                <td>Data not found</td>
              </tr>
            )}

            {/* <tr>
              <td>01</td>
              <td>
                <span>1234567</span>
              </td>
              <td>
                <a href="#">Penthouse in Lima...</a>
              </td>
              <td>Residential</td>
              <td>€1250</td>
              <td>Limassol</td>
              <td className="text-center">
                <a href="#">
                  <img src="images/delete-24px.svg" alt="alt-img"  />
                </a>
              </td>
            </tr> */}
          </tbody>
        </table>
      </div>
    </div>
  );
}

MyFavourites.defaultProps = {
  Favourites: [],
};
MyFavourites.propTypes = {
  getMyFavouriteDataApi: PropTypes.func.isRequired,
  Favourites: PropTypes.instanceOf(Object),
};
const mapStateToProps = (state) => ({
  Favourites: getFavouritesSelecter(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      getMyFavouriteDataApi: getMyBidsDataApiAction,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(MyFavourites);
