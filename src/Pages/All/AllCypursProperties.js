import React from 'react';
import { Link } from 'react-router-dom';

export default function AllCypursProperties() {
  return (
    <main>
      <section className="contact_form_section">
        <div className="container">
          <div className="contact-title">
            <h2>Cyprus Properties</h2>
          </div>
          <div className="row all_cypurs_properties_city">
            <div className="col-lg-3 .col-md-3">
              <div className="find_item_card">
                <Link to="/all-cypurs-properties/city">
                  <img
                    src="images/Limassol-Promanade-2.jpg"
                    alt="Limassol-Promanade-2"
                    className="img-fluid"
                  />
                  <p>Limassol</p>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 .col-md-3">
              <div className="find_item_card">
                <Link to="/all-cypurs-properties/city">
                  <img
                    src="images/tourist-district-paphos.jpg"
                    alt="Limassol-Promanade-2"
                    className="img-fluid"
                  />
                  <p>Phaphos</p>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 .col-md-3">
              <div className="find_item_card">
                <Link to="/all-cypurs-properties/city">
                  <img
                    src="images/cyprus-nicosia-buyuk-han.jpg"
                    alt="Limassol-Promanade-2"
                    className="img-fluid"
                  />
                  <p>Nicosia</p>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 .col-md-3">
              <div className="find_item_card">
                <Link to="/all-cypurs-properties/city">
                  <img
                    src="images/Limassol-Promanade-2.jpg"
                    alt="Limassol-Promanade-2"
                    className="img-fluid"
                  />
                  <p>Larnaka</p>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 .col-md-3">
              <div className="find_item_card">
                <Link to="/all-cypurs-properties/city">
                  <img
                    src="images/Limassol-Promanade-2.jpg"
                    alt="Limassol-Promanade-2"
                    className="img-fluid"
                  />
                  <p>Famagusta</p>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 .col-md-3">
              <div className="find_item_card">
                <Link to="/all-cypurs-properties/city">
                  <img
                    src="images/tourist-district-paphos.jpg"
                    alt="Limassol-Promanade-2"
                    className="img-fluid"
                  />
                  <p>Kyrenia</p>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 .col-md-3">
              <div className="find_item_card">
                <Link to="/all-cypurs-properties/city">
                  <img
                    src="images/cyprus-nicosia-buyuk-han.jpg"
                    alt="Limassol-Promanade-2"
                    className="img-fluid"
                  />
                  <p>Ayia Napa</p>
                </Link>
              </div>
            </div>
            <div className="col-lg-3 .col-md-3">
              <div className="find_item_card">
                <Link to="/all-cypurs-properties/city">
                  <img
                    src="images/Limassol-Promanade-2.jpg"
                    alt="Limassol-Promanade-2"
                    className="img-fluid"
                  />
                  <p>Dali</p>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}
