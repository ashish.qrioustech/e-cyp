import React from 'react';
import SearchComponentWhite from '../../Components/SearchComponent/SearchComponentWhite';
import { Link } from 'react-router-dom';

export default function MakeAnOffer() {
  return (
    <main style={{ backgroundColor: '#f1f1f1' }}>
      <section
        className="second-page_bg second_page_1_bg"
        style={{ backgroundImage: 'url(images/make-offer.jpg)' }}
      >
        <h2>MAKE AN OFFER</h2>
      </section>
      {/* section search start */}
      <SearchComponentWhite/>
      {/* <section className="Second-Page_Discover">
        <div className="container search_container second_page_discover_info">
          <h3>Discover your perfect property in Cyprus</h3>
          <div className="search_dp_section">
            <div className="row">
              <div className="col-md-3">
                <label>DISTRICT</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Any
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Any
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label> AREA / VILLAGE</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Choose Area / Village
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>TYPE</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Any
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>SUB TYPE</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Choose Sub-Type
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>CATEGORY</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Choose Category
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>PRICE</label>
                <div className="slider-range_Price_section">
                  <span>Price €0 - €10,000,000+</span>
                </div>
                <div className="slider-range__Price">
                  <div id="slider-range" />
                </div>
              </div>
              <div className="col-md-3">
                <label>LOT NUMBER</label>
                <input
                  type="text"
                  name="LOTNo"
                  id="LOTNo"
                  style={{ width: '100%' }}
                  placeholder="Enter Lot Number"
                />
              </div>
              <div className="col-md-3">
                <label>&nbsp;</label>
                <br />
                <button className="searc_sc_btn">
                  <img src="images/search.svg" alt="alt-img"  />
                  Search
                </button>
              </div>
            </div>
          </div>
          <div
            className="rest_show_search_section"
            style={{ paddingBottom: 15 }}
          >
            <a href="#">Reset</a>
            <a id="advance-search">Advance Search</a>
          </div>
          <div className="search_dp_section advance_Search_Section">
            <div className="row">
              <div className="col-md-3">
                <label>NEIGHBOURHOOD</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Choose Neighbourhood
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>LAND</label>
                <div className="slider-range_Price_section">
                  <span>
                    m<small>2</small> 0 - 59000
                  </span>
                </div>
                <div className="slider-range__Price">
                  <div id="land" />
                </div>
              </div>
              <div className="col-md-3">
                <label>BEDROOMS</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Any
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>NEW / USED</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Choose Sub-Type
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>REMOVE LISTINGS</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Choose Remove listings
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>COVERED AREA</label>
                <div className="slider-range_Price_section">
                  <span>
                    m<small>2</small> 0 - 59000
                  </span>
                </div>
                <div className="slider-range__Price">
                  <div id="coverd-area" />
                </div>
              </div>
              <div className="col-md-3">
                <label>BEDROOMS</label>
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Any
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
              <div className="col-md-3">
                <label>NEW / USED</label>
                <br />
                <div className="dropdown">
                  <button
                    className="btn btn-Search_drop_D dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Choose Sub-Type
                  </button>
                  <div
                    className="drop_custome-show dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a className="dropdown-item" href="#">
                      Action
                    </a>
                    <a className="dropdown-item" href="#">
                      Another action
                    </a>
                    <a className="dropdown-item" href="#">
                      Something else here
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      */}
      {/* section search end */}
      <section className="gird-properties">
        <div className="container text-center">
          <h4>Make An Offer</h4>
        </div>
        <div className="container">
          <div className="short-list">
            <a href="#">
              <img src="images/grid.svg" alt="alt-img"  />
            </a>
            <a href="#">
              <img src="images/list-view.svg" alt="alt-img"  />
            </a>
            <div className="dropdown">
              <button
                className="btn btn-short-dp dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Short by
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <a className="dropdown-item" href="#">
                  Popular
                </a>
                <a className="dropdown-item" href="#">
                  Low - High
                </a>
                <a className="dropdown-item" href="#">
                  High - Low
                </a>
              </div>
            </div>
          </div>
          <div className="grid_list_properties">
            <div className="row">
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1(2).jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>Detached House</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-3.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1(2).jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>Detached House</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-3.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1(2).jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>Detached House</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-3.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="pagination">
        <div className="container">
          <div className="pagination_second-pg">
            <a href="#" className="active">
              01
            </a>
            <a href="#">02</a>
            <a href="#">03</a>
            <a href="#">04</a>
            <a href="#" className="multi-pagination">
              <img src="images/three-dot.svg" alt="alt-img"  />
            </a>
            <a href="#">99</a>
          </div>
        </div>
      </section>
    </main>
  );
}
