import React from 'react';
import { Tabs, Tab } from 'react-bootstrap';

// import OwlCarousel from 'react-owl-carousel2';

export default function HowItWork() {
  // const options = {
  //   loop: true,
  //   margin: 48,
  //   nav: false,
  //   navClass: ['d-none', 'd-none'],

  //   autoplay: true,
  //   autoplaySpeed: 500,
  //   dots: false,
  //   responsive: {
  //     0: {
  //       items: 1,
  //     },
  //     600: {
  //       items: 2,
  //     },
  //     1000: {
  //       items: 3,
  //     },
  //   },
  // };
  return (
    <main>
      <section className="contact_form_section how-it-work-title">
        <div className="container">
          <div className="contact-title">
            <h2>How to bid</h2>
          </div>
          <div className="tab_how_it_work">
            <Tabs
              defaultActiveKey="find-property"
              id="uncontrolled-tab-example"
            >
              <Tab  eventKey="find-property" title="Find your property">
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-7">
                      <p>
                        Peruse our most recent private and business properties
                        across Spain, UK, Ireland, South Africa and Cyprus.
                      </p>
                    </div>
                    <div className="col-md-5">
                      <img
                        src="images/how-it-works.svg"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </Tab>

              <Tab  eventKey="talk" title="Talk to us">
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        We're here to help - we have a full group of land
                        specialists in each market who are close by to respond
                        to any inquiries you have about the properties.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/talk-with-us.jpg"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </Tab>

              <Tab  eventKey="get_details" title="Get the details">
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        The legitimate documentation for each property is
                        accessible on our site - so you can settle on an
                        educated choice.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/document.png"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </Tab>

              <Tab  eventKey="view_property" title="View your property">
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        Go along with us at one of our open viewings to
                        investigate and meet our group.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/view-property.png"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </Tab>

              <Tab  eventKey="reg_bid" title="Register for bidding">
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        Register to offer on your picked properties – we'll
                        require a few insights concerning you and an offering
                        store.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img src="images/bid.png" alt className="img-fluid" />
                    </div>
                  </div>
                </div>
              </Tab>

              <Tab  eventKey="sale_day" title="Sale day">
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        When offering opens, every single enlisted gathering can
                        begin setting offers. It's totally straightforward - the
                        entirety of the offers will show up continuously, on the
                        important property page.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/sale-property-icon.png"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </Tab>

              <Tab  eventKey="contaract_sign" title="Contract signed">
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        Once bidding has closed, we immediately sign contracts
                        on behalf of both buyer and seller.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/contract-sign.png"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </Tab>
            </Tabs>
          </div>

          {/* <div className="tab_how_it_work">
            <ul
              className="nav nav-tabs responsive-tabs"
              id="myTab"
              role="tablist"
            >
              <li className="nav-item">
                <a
                  className="nav-link active"
                  id="find-property-tab"
                  data-toggle="tab"
                  href="#find-property"
                  role="tab"
                  aria-controls="find-property"
                  aria-selected="true"
                >
                  Find your property
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="talk-tab"
                  data-toggle="tab"
                  href="#talk"
                  role="tab"
                  aria-controls="talk"
                  aria-selected="false"
                >
                  Talk to us
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="get_details-tab"
                  data-toggle="tab"
                  href="#get_details"
                  role="tab"
                  aria-controls="get_details"
                  aria-selected="false"
                >
                  Get the details
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="view_property-tab"
                  data-toggle="tab"
                  href="#view_property"
                  role="tab"
                  aria-controls="view_property"
                  aria-selected="false"
                >
                  View your property
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="reg_bid-tab"
                  data-toggle="tab"
                  href="#reg_bid"
                  role="tab"
                  aria-controls="reg_bid"
                  aria-selected="false"
                >
                  Register for bidding
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="sale_day-tab"
                  data-toggle="tab"
                  href="#sale_day"
                  role="tab"
                  aria-controls="sale_day"
                  aria-selected="false"
                >
                  Sale day
                </a>
              </li>
              <li className="nav-item">
                <a
                  className="nav-link"
                  id="contaract_sign-tab"
                  data-toggle="tab"
                  href="#contaract_sign"
                  role="tab"
                  aria-controls="contaract_sign"
                  aria-selected="false"
                >
                  Contract signed
                </a>
              </li>
            </ul>
            <div className="tab-content" id="myTabContent">
              <div
                className="tab-pane fade show active"
                id="find-property"
                role="tabpanel"
                aria-labelledby="find-property-tab"
              >
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-7">
                      <p>
                        Peruse our most recent private and business properties
                        across Spain, UK, Ireland, South Africa and Cyprus.
                      </p>
                    </div>
                    <div className="col-md-5">
                      <img
                        src="images/how-it-works.svg"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="tab-pane fade"
                id="talk"
                role="tabpanel"
                aria-labelledby="talk-tab"
              >
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        We're here to help - we have a full group of land
                        specialists in each market who are close by to respond
                        to any inquiries you have about the properties.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/talk-with-us.jpg"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="tab-pane fade"
                id="get_details"
                role="tabpanel"
                aria-labelledby="-tab"
              >
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        The legitimate documentation for each property is
                        accessible on our site - so you can settle on an
                        educated choice.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/document.png"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="tab-pane fade"
                id="view_property"
                role="tabpanel"
                aria-labelledby="-tab"
              >
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        Go along with us at one of our open viewings to
                        investigate and meet our group.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/view-property.png"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="tab-pane fade"
                id="reg_bid"
                role="tabpanel"
                aria-labelledby="reg_bid-tab"
              >
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        Register to offer on your picked properties – we'll
                        require a few insights concerning you and an offering
                        store.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img src="images/bid.png" alt className="img-fluid" />
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="tab-pane fade"
                id="sale_day"
                role="tabpanel"
                aria-labelledby="sale_day-tab"
              >
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        When offering opens, every single enlisted gathering can
                        begin setting offers. It's totally straightforward - the
                        entirety of the offers will show up continuously, on the
                        important property page.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/sale-property-icon.png"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
              <div
                className="tab-pane fade"
                id="contaract_sign"
                role="tabpanel"
                aria-labelledby="contaract_sign-tab"
              >
                <div className="container tab_content_details">
                  <div className="row d-flex align-items-center">
                    <div className="col-md-8">
                      <p>
                        Once bidding has closed, we immediately sign contracts
                        on behalf of both buyer and seller.
                      </p>
                    </div>
                    <div className="col-md-4">
                      <img
                        src="images/contract-sign.png"
                        alt
                        className="img-fluid"
                      />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        */}
        </div>
      </section>
      <section className="mb-5">
        <div className="container">
          <h2 className="text-center">FAQs</h2>
          <div id="accordion" className="accordion">
            <div className="card mb-0">
              <div
                className="card-header collapsed"
                data-toggle="collapse"
                href="#collapseOne"
                aria-expanded="false"
              >
                <a className="card-title">HOW DO I ACTIVATE MY ACCOUNT?</a>
              </div>
              <div
                id="collapseOne"
                className="card-body collapse"
                data-parent="#accordion"
                style={{}}
              >
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book.
                </p>
              </div>
              <div
                className="card-header collapsed"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapseTwo"
                aria-expanded="false"
              >
                <a className="card-title">
                  HOW CAN I USE MY REMAINING ACCOUNT CREDITS?
                </a>
              </div>
              <div
                id="collapseTwo"
                className="card-body collapse"
                data-parent="#accordion"
                style={{}}
              >
                <p>
                  Lorem Ipsum is simply dummy text of the printing and
                  typesetting industry. Lorem Ipsum has been the industry's
                  standard dummy text ever since the 1500s, when an unknown
                  printer took a galley of type and scrambled it to make a type
                  specimen book.
                </p>
              </div>
              <div
                className="card-header collapsed"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapseThree"
                aria-expanded="false"
              >
                <a className="card-title">
                  HOW MANY FREE SAMPLES CAN I REDEEM?
                </a>
              </div>
              <div
                id="collapseThree"
                className="collapse"
                data-parent="#accordion"
              >
                <div className="card-body">
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book.
                  </p>
                </div>
              </div>
              <div
                className="card-header collapsed"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapseFour"
                aria-expanded="false"
              >
                <a className="card-title">Dummy Content won't install...</a>
              </div>
              <div
                id="collapseFour"
                className="collapse"
                data-parent="#accordion"
              >
                <div className="card-body">
                  <p>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry. Lorem Ipsum has been the industry's
                    standard dummy text ever since the 1500s, when an unknown
                    printer took a galley of type and scrambled it to make a
                    type specimen book.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* <section class="find_out_more">
  <div class="container">
      <h2>
          Find out more
      </h2>
      <div class="slider_find_out_more">
          <div class="owl-carousel owl-theme find_slider" id="property_slider">
              <div class="find_item_card">
                  <a href="city.html">
                      <img src="images/Limassol-Promanade-2.jpg" alt="Limassol-Promanade-2" class="img-fluid">
                      <p>Limassol</p>
                  </a>
              </div>
              <div class="find_item_card">
                  <a href="city.html">
                      <img src="images/tourist-district-paphos.jpg" alt="Limassol-Promanade-2" class="img-fluid">
                      <p>phaphos</p>
                  </a>
              </div>
              <div class="find_item_card">
                  <a href="city.html">
                      <img src="images/cyprus-nicosia-buyuk-han.jpg" alt="Limassol-Promanade-2" class="img-fluid">
                      <p>Nicosia</p>
                  </a>
              </div>
              <div class="find_item_card">
                  <a href="city.html">
                      <img src="images/cyprus-nicosia-buyuk-han.jpg" alt="Limassol-Promanade-2" class="img-fluid">
                      <p>Nicosia</p>
                  </a>
              </div>
              <div class="find_item_card">
                  <a href="city.html">
                      <img src="images/cyprus-nicosia-buyuk-han.jpg" alt="Limassol-Promanade-2" class="img-fluid">
                      <p>Nicosia</p>
                  </a>
              </div>
          </div>
      </div>
  </div>
    </section> */}
    </main>
  );
}
