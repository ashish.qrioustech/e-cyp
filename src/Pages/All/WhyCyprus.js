import React from 'react';
import { Link } from 'react-router-dom';

export default function WhyCyprus() {
  return (
    <main>
      <section className="second-page_bg second_page_1_bg">
        <h2 style={{ textTransform: 'uppercase' }}>Why Cyprus?</h2>
      </section>
      <section className="Why-cyprus-content mt-5">
        <div className="container" style={{ textAlign: 'justify' }}>
          <p>
            Cyprus, an individual from the European Union since 2004, is
            otherwise called the excellent island of Aphrodite, giving an
            entrancing blend of the dark blue waters of the ocean and its
            mountains. It is the third biggest Mediterranean island having a
            region of 9.251 sq. km. It is arranged at the junction of three
            mainlands Europe, Asia and Africa and near the bustling courses
            connecting Western Europe with the Arab World and the Far East. It
            is arranged inside four hours flying time from London, three from
            Moscow and two from Dubai. The island's time region is 7 hours in
            front of New York and 7 hours behind Tokyo. The island has a lovely
            and sound atmosphere with around 340 days of daylight a year. The
            capital of Cyprus is Nicosia and the principle mechanical and
            business focus is Limassol.
          </p>
        </div>
      </section>
      <section
        className="visit_cypurs_why pt-5 mt-5 pb-5"
        style={{ backgroundColor: '#f1f1f1' }}
      >
        <div className="container">
          <div className="buyers_guide_row_pg row" style={{ marginTop: 0 }}>
            <div className="col-md-12 col-lg-6 col-sm-12">
              <a href>
                <div className="buyers_Guide-details">
                  <img src="images/house-1353389_640.jpg" alt="alt-img"  />
                  <div className="buyers_details_title_discription">
                    <div className="buyers_details_title">
                      <h6>Cyprus Property Taxation</h6>
                    </div>
                    <div className="buyers_details_desicription">
                      {/* <span>
                            23rd March 2020
                        </span> */}
                      <p>
                        Steady Property Transfer Fees Real Estate Transfer
                        charge expenses are vital so as to move FREEHOLD
                        proprietorship to the name of the buyer. This should be
                        possible when the important Government Authority has
                        given the title deed and the buy has been finished...
                      </p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div className="col-md-12 col-lg-6 col-sm-12">
              <a href>
                <div className="buyers_Guide-details">
                  <img src="images/deeds.jpg" alt="alt-img"  />
                  <div className="buyers_details_title_discription">
                    <div className="buyers_details_title">
                      <h6>Gaining Title Deeds in Cyprus</h6>
                    </div>
                    <div className="buyers_details_desicription">
                      {/* <span>
                          23rd March 2020
                      </span> */}
                      <p>
                        Title deeds are otherwise called "Endorsements of
                        Registration of Immovable Property". These are masterful
                        archives which are critical as confirmation for land or
                        property proprietorship in Cyprus. At the point when
                        Cyprus got admitted to EU it needed to ...
                      </p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
          <div className="buyers_guide_row_pg row">
            <div className="col-md-12 col-lg-6 col-sm-12">
              <a href>
                <div className="buyers_Guide-details">
                  <img src="images/architecture-1836070_640.jpg" alt="alt-img"  />
                  <div className="buyers_details_title_discription">
                    <div className="buyers_details_title">
                      <h6>Purchasing property in Cyprus</h6>
                    </div>
                    <div className="buyers_details_desicription">
                      {/* <span>
                            23rd March 2020
                        </span> */}
                      <p>
                        The delightful Island of Cyprus is widely acclaimed for
                        its common magnificence, incredible neighborhood
                        cooking, and accommodation. Now and again just coming to
                        Cyprus for a couple of days of the year isn't sufficient
                        and there are many individuals who might want to fantasy
                        ...
                      </p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
            <div className="col-md-12 col-lg-6 col-sm-12">
              <a href>
                <div className="buyers_Guide-details">
                  <img src="images/money-2724235_640.jpg" alt="alt-img"  />
                  <div className="buyers_details_title_discription">
                    <div className="buyers_details_title">
                      <h6>Cyprus Property Tax Incentive broadened</h6>
                    </div>
                    <div className="buyers_details_desicription">
                      {/* <span>
                              23rd March 2020
                          </span> */}
                      <p>
                        CHANGES to Cyprus' Property Transfer Fees, which
                        happened in December 2011 for a time of a half year and
                        in this manner stretched out for a further time of a
                        half year, may now been reached out until the
                        31stDecember 2013. The motivators just apply to the
                        principal offer of a property ...
                      </p>
                    </div>
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>
      <section className="Most_visited_place_bg">
        <div className="container">
          <div className="row d-flex align-items-center">
            <div className="col-md-8">
              <h1>Places Most Visit Cyprus</h1>
            </div>
            <div className="col-md-4 text-center">
              <Link to="places-most-visit">
                <button>Places Visits</button>
              </Link>
            </div>
          </div>
        </div>
      </section>
      {/* <section class="visit_cypurs_why pt-5 mt-5 pb-5" style="background-color: #f1f1f1;">
      <div class="container">
          <div class="contact-title mb-5">
              <h2>Places Most Visit Cyprus</h2>
          </div>
          <div class="place_most_visit_list">
              <div class="list_view_property_deti_info">
                  <div class="list_view_property_img" style="background: url(images/nature.jpg); background-size: cover;">

                  </div>
                  <div class="list_view_property_full_info">
                      <div class="list_view_property-title_info" style="width: 100%; padding:20px 0;">
                          <div class="list_view_property-title">
                              <h4>NATURE</h4>
                          </div>
                          <div class="list_view_property-info place_visit_why_cyprus">
                              <div class="description_list_view" style="text-align: justify;">
                                  <p>Favored with the excellence of nature's best palette, the landscape of Cyprus unfurls across sparkling coasts, moving mountains, fragrant woodlands and rough headlands. From the warm shores of the territory to the
                                      pristine and cool desert spring of the Troodos mountain run, nature sweethearts, craftsmen, picture takers and pioneers will all savor the experience of meeting modest animals, and finding uncommon plants that
                                      peep out in the midst of cascades, inlets, forest, winding path and confined sandy sea shores. As the island is on the movement way between Europe, Asia and Africa, Cyprus is a birdwatchers dream, with herds
                                      of flamingos frequenting the salt lakes, and numerous other huge species going through or settling. Furthermore, somewhere down in the woodlands, the national creature - the Mouflon - meanders uninhibitedly,
                                      with getting a brief look at this hesitant, wild sheep a genuine reward for local people and guests alike.</p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="place_most_visit_list">
              <div class="list_view_property_deti_info">
                  <div class="list_view_property_img" style="background: url(images/sea-sun.jpg); background-size: cover;">

                  </div>
                  <div class="list_view_property_full_info">
                      <div class="list_view_property-title_info" style="width: 100%; padding:20px 0;">
                          <div class="list_view_property-title">
                              <h4>SUN & SEA</h4>
                          </div>
                          <div class="list_view_property-info place_visit_why_cyprus">
                              <div class="description_list_view" style="text-align: justify;">
                                  <p> Scarcely any sentiments can contrast with that of sinking your toes into warm sand… of the sun kissing your skin, and your faculties taking in the new, pungent breeze and the unlimited perspectives on sparkling
                                      blue waters. </p>

                                  <p> This unspoiled scene is one that can be appreciated generally of the year on the island of Cyprus, and one that offers a horde of encounters, from the all out unwinding of sunbathing with drink close by, to the
                                      rush and challenge of attempting another water sport..</p>

                                  <p> From segregated narrows, to exuberant hotels and sandy sea shores to rough bays, the island's sweeping coastline is home to a wide range of style sea shores; every remarkable in its appearance and the offices it
                                      offers. </p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="place_most_visit_list">
              <div class="list_view_property_deti_info">
                  <div class="list_view_property_img" style="background: url(images/sports.jpg) center; background-size: cover;">

                  </div>
                  <div class="list_view_property_full_info">
                      <div class="list_view_property-title_info" style="width: 100%; padding:20px 0;">
                          <div class="list_view_property-title">
                              <h4>SPORTS & TRAINING</h4>
                          </div>
                          <div class="list_view_property-info place_visit_why_cyprus">
                              <div class="description_list_view" style="text-align: justify;">
                                  <p>
                                      Preparing hard doesn't mean preparing in cruel conditions, and there is no preferable inspiration over rehearsing and getting ready for the game you love on an island where the charming atmosphere joins with wonderful Mediterranean environmental factors
                                      - and top notch enhancements - to motivate accomplishment for the coming season. A large group of sportspersons and groups from everywhere throughout the world, including European football crews, Olympic hopefuls,
                                      prepared medallists and perseverance competitors, all give themselves an upper hand by selecting to prepare towards their objectives in Cyprus, because of a triumphant equation of services.One of the most alluring
                                      explanations behind picking the island is in fact its nearly ensured day by day daylight, gentle winter temperatures and insignificant precipitation, and this is additionally upgraded and supplemented by a large
                                      group of different advantages.
                                  </p>

                                  </p>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="place_most_visit_list">
              <div class="list_view_property_deti_info">
                  <div class="list_view_property_img" style="background: url(images/spa.jpg); background-size: cover;">

                  </div>
                  <div class="list_view_property_full_info">
                      <div class="list_view_property-title_info" style="width: 100%; padding:20px 0;">
                          <div class="list_view_property-title">
                              <h4>HEALTH & WELLNESS</h4>
                          </div>
                          <div class="list_view_property-info place_visit_why_cyprus">
                              <div class="description_list_view" style="text-align: justify;">
                                  <p>
                                      Joining the charm of a daylight island with top notch human services offices and appealing charges, Cyprus is quick turning into a supported choice for clinical the travel industry. What's more, with the special reward of recovering in loosening up environmental
                                      factors that are helpful for recuperating, it is no big surprise that an expanding number of patients are picking the island for their medicines. Close by ordinary clinical medicines - that happen in the solace
                                      of present day emergency clinics and facilities - the island is additionally home to extravagance spa resorts and rustic retreats, where solace and nature amicably entwine with wellbeing. Spoiling, comprehensive
                                      treatments, magnificence medicines and an overall feeling of prosperity are completely summoned, alongside the sentiment of getting away and concentrating exclusively on oneself… without the burdens or interruptions
                                      of every day life.
                                  </p>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="place_most_visit_list">
              <div class="list_view_property_deti_info">
                  <div class="list_view_property_img" style="background: url(images/food.jpg); background-size: cover;">

                  </div>
                  <div class="list_view_property_full_info">
                      <div class="list_view_property-title_info" style="width: 100%; padding:20px 0;">
                          <div class="list_view_property-title">
                              <h4>FOOD & DRINK</h4>
                          </div>
                          <div class="list_view_property-info place_visit_why_cyprus">
                              <div class="description_list_view" style="text-align: justify;">
                                  <p>
                                      The custom of sharing great, new nearby cooking is a significant piece of the island's way of life, and is inherently connected with each get-together, from family social events and exceptional events to strict celebrations… each set apart with its own
                                      unmistakable treats and plans. From healthy meat dishes and claim to fame cheeses to exceptional treats of carob and grape, the Cypriot food is a colorful mix of Greek and Middle Eastern societies, sprinkled
                                      with leftovers of old civilisations, for example, indigenous Roman root vegetables or old Phoenician luxuries. What's more, its a well known fact that the 'Mediterranean eating routine' is viewed as of the most
                                      beneficial, on account of its wealth of heart-sound olive oil, beats, lean meat, neighborhood spices and newly developed foods grown from the ground. Add to this the ideal atmosphere - that gives the new produce
                                      its extraordinary flavor - and a festival everywhere, complete with unique treats, and you will locate a major gastronomic experience anticipates on this little island!
                                  </p>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="place_most_visit_list">
              <div class="list_view_property_deti_info">
                  <div class="list_view_property_img" style="background: url(images/wedding.jpg); background-size: cover;">

                  </div>
                  <div class="list_view_property_full_info">
                      <div class="list_view_property-title_info" style="width: 100%; padding:20px 0;">
                          <div class="list_view_property-title">
                              <h4>WEDDINGS & HONEYMOONS</h4>
                          </div>
                          <div class="list_view_property-info place_visit_why_cyprus">
                              <div class="description_list_view" style="text-align: justify;">
                                  <p>
                                      Cyprus is one of Europe's driving goals for weddings, gifts and special first nights, and is popular for its brilliant atmosphere, delightful view and bunch of alternatives for scenes, services and festivities. From sundrenched pledges and holy functions
                                      to love bird experiences, Aphrodite's island is love embodied.
                                  </p>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="place_most_visit_list">
              <div class="list_view_property_deti_info">
                  <div class="list_view_property_img" style="background: url(images/CULTURE.jpg); background-size: cover;">

                  </div>
                  <div class="list_view_property_full_info">
                      <div class="list_view_property-title_info" style="width: 100%; padding:20px 0;">
                          <div class="list_view_property-title">
                              <h4>CULTURE & RELIGION</h4>
                          </div>
                          <div class="list_view_property-info place_visit_why_cyprus">
                              <div class="description_list_view" style="text-align: justify;">
                                  <p>
                                      Cyprus is a little island with a long history and a rich culture that traverses 10.000 years, making it probably the most seasoned civilisation in the Mediterranean - as prove by the many captivating social sights, exhibition halls, landmarks and displays.
                                      Arranged at the intersection of three mainlands - Europe, Asia and Africa - the island's extraordinary geographic position has had a significant influence in its fierce past since relic. Its Prehistoric Age
                                      occupants were joined 3,500 years prior by the Mycenaean Greeks, who presented and built up their civilisation, hence for all time ingraining the island's Greek roots. Numerous different societies followed from
                                      that point, including Phoenicians, Assyrians, Egyptians, Romans, Franks, Venetians, Ottomans and British, who all abandoned obvious leftovers of their entry, and have in this manner made a mosaic of various
                                      societies and periods.
                                  </p>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="place_most_visit_list">
              <div class="list_view_property_deti_info">
                  <div class="list_view_property_img" style="background: url(images/western-1662449_640.jpg); background-size: cover;">

                  </div>
                  <div class="list_view_property_full_info">
                      <div class="list_view_property-title_info" style="width: 100%; padding:20px 0;">
                          <div class="list_view_property-title">
                              <h4>THEMATIC ROUTES</h4>
                          </div>
                          <div class="list_view_property-info place_visit_why_cyprus">
                              <div class="description_list_view" style="text-align: justify;">
                                  <p>
                                      Blessed with the beauty of nature’s best palette, the scenery of Cyprus unfolds across glittering coasts, rolling mountains, fragrant forests and rugged headlands.
                                  </p>

                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </section> */}
      <section className="mt-5">
        <div className="container">
          <div className="contact-title mb-5">
            <h2>Cities</h2>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div  className="card-visit-place">
                <Link  to="/why-cyprus/paphos">
                  <img src="images/Paphos.jpg" alt className="img-fluid" />
                  <h4>Paphos</h4>
                </Link>
              </div>
            </div>
            <div className="col-md-4">
              <div  className="card-visit-place">
                <Link  to="/why-cyprus/protaras">
                  <img src="images/Protaras.jpg" alt className="img-fluid" />
                  <h4>Protaras</h4>
                </Link>
              </div>
            </div>
            <div className="col-md-4">
              <div  className="card-visit-place">
                <Link  to="/why-cyprus/ayia-napa">
                  <img src="images/Ayia-Napa.jpg" alt className="img-fluid" />
                  <h4>Ayia Napa</h4>
                </Link>
              </div>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-md-4">
              <div  className="card-visit-place">
                <Link  to="/why-cyprus/nicosia">
                  <img src="images/Nicosia.jpg" alt className="img-fluid" />
                  <h4>Nicosia</h4>
                </Link>
              </div>
            </div>
            <div className="col-md-4">
              <div  className="card-visit-place">
                <Link  to="/why-cyprus/larnaka">
                  <img src="images/Larnaka.jpg" alt className="img-fluid" />
                  <h4>Larnaka</h4>
                </Link>
              </div>
            </div>
            <div className="col-md-4">
              <div  className="card-visit-place">
                <Link  to="/why-cyprus/limassol">
                  <img src="images/Limassol.jpg" alt className="img-fluid" />
                  <h4>Limassol</h4>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
    
    </main>
  );
}
