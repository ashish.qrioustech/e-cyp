import React, { useEffect, useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel2';
// import { Fade } from 'react-bootstrap';
// import DropdownComponent from '../../Components/Dropdown/DropdownComponent';
import Axios from 'axios';
import SearchComponent from '../../Components/SearchComponent/SearchComponent';
// import SearchComponentWhite from '../../Components/SearchComponent/SearchComponentWhite';

import { Modal } from 'react-bootstrap';

export default function Home() {
  const history = useHistory();
  const [dropData] = useState([
    { id: 1, value: 'Any' },
    { id: 2, value: 'Choose Area / Village' },
    { id: 3, value: 'Choose Sub-Type' },
    { id: 4, value: 'Choose Category' },
    { id: 5, value: 'Choose Neighbourhood' },
    { id: 6, value: 'Choose Remove listings' },
  ]);
  const [open, setOpen] = useState(false);
  const options = {
    arrows: false,
    autoplay: true,
    loop: true,

    fade: true,
    items: 1,
    dots: false,
    animateIn: 'fadeIn',
    animateOut: 'fadeOut',
    autoplaySpeed: true,
    navClass: ['d-none', 'd-none'],
    nav: false,
  };
  useEffect(() => {
    // document.getElementById('myModal_no_page_load').modal('show');
    // setInterval(() => {
    //   console.log('This will run every second!');
    // }, 1000);
    setTimeout(() => {
      setOpen(true);
    }, 1000);
  }, []);
  const Req = {
    // category: ['residential'],
    fromprice: '0',
    toprice: '50000000',
    // city: ['losangeles'],
  };
  const handleSearchProperty = () => {
    Axios.post(process.env.REACT_APP_API + 'mobilesearch', {
      ...Req,
    })
      .then((res) => {
        if (res.data.success === 'yes') {
          history.push({ pathname: '/search-result', data: res.data.results });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <main>
      <section className="main_pg_banner_slider">
        <OwlCarousel options={options}>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-1.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-2.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-3.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-4.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-5.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-6.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-7.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-8.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-9.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
          <div className="columns">
            <div
              className="image"
              style={{ backgroundImage: 'url("images/banner-10.jpg")' }}
            />
            <div className="text-banner-slider-main">
              <h1> Welcome to e-cyprusauction.com </h1>
              <h3>Best solution for all property auctions</h3>
            </div>
          </div>
        </OwlCarousel>
      </section>
      {/* section slider end */}
      {/* section search start */}
      <SearchComponent />

      {/* <SearchComponentWhite/> */}

      {/* section search end */}
      {/* Mansnory Gallery */}
      <section className="masnory_Gallery_section">
        <div className="masnory_Gallery_title">
          <h2>Heading_Main</h2>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt lorem ipsum dolor sit amet, consectetur
            adipiscing
          </p>
        </div>
        <div className="container">
     
          <div className="mess-gallery-wd-custome">
            <div className="row">
              <div className="col-md-8">
                <Link to="/auctions">
                  <div className="mess-gallery-link">
                    <div className="mess-gallery-section-auction">
                      <div className="mess-Gallery-section-title">
                        <h3>
                          <Link to="/auctions">Auctions</Link>
                        </h3>
                        <span>
                          {' '}
                          <Link to="/auctions"> Featured Auction </Link> |{' '}
                          <Link to="/auctions">Current Auction </Link> |{' '}
                          <Link to="/auctions">Next Auction </Link>
                        </span>
                      </div>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-4">
                <Link to="/make-an-offer" className="mess-gallery-link">
                  <div className="mess-gallery-section-make-offer">
                    <div className="mess-Gallery-section-title">
                      <h3>Make us an offer</h3>
                      <span>120properties</span>
                    </div>
                  </div>
                </Link>
                <Link to="/buyers-guide" className="mess-gallery-link">
                  <div className="mess-gallery-section-how-to-bid">
                    <div className="mess-Gallery-section-title">
                      <h3>Learn How to Bid & Legalities</h3>
                      <span>120properties</span>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-6">
                <Link to="/new-listing" className="mess-gallery-link">
                  <div className="mess-gallery-section-New-listing">
                    <div className="mess-Gallery-section-title">
                      <h3>New Listings</h3>
                      <span>120properties</span>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-6">
                <Link to="/all-cypurs-properties" className="mess-gallery-link">
                  <div className="mess-gallery-section-all-proprety">
                    <div className="mess-Gallery-section-title">
                      <h3>All our Cyprus properties</h3>
                      <span>&nbsp;</span>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-4">
                <Link to="/seller-guide" className="mess-gallery-link">
                  <div className="mess-gallery-section-thing-salling">
                    <div className="mess-Gallery-section-title">
                      <h3>Thinking of Selling</h3>
                      {/* <span>by buying properties</span> */}
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-8">
                <Link to="/cyprus-residency-grid" className="mess-gallery-link">
                  <div className="mess-gallery-section-rasidency">
                    <div className="mess-Gallery-section-title">
                      <h3>Cyprus Residency</h3>
                      <span>by buying properties</span>
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-4">
                <Link to="/our-services" className="mess-gallery-link">
                  <div className="mess-gallery-section-our-services">
                    <div className="mess-Gallery-section-title">
                      <h3>Our Services</h3>
                      {/* <span>Fetured Action</span> */}
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-4">
                <Link
                  to="/investment-commercia-properties"
                  className="mess-gallery-link"
                >
                  <div className="mess-gallery-section-invest-com-property">
                    <div className="mess-Gallery-section-title">
                      <h3>Investment &amp; Commercial properties</h3>
                      {/* <span>Fetured Action</span> */}
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-4">
                <Link to="/why-cyprus" className="mess-gallery-link">
                  <div className="mess-gallery-section-cypus">
                    <div className="mess-Gallery-section-title">
                      <h3>Why Cyprus?</h3>
                      {/* <span>Fetured Action</span> */}
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-8">
                <Link to="/join-our-team" className="mess-gallery-link">
                  <div className="mess-gallery-section-join-our_team">
                    <div className="mess-Gallery-section-title">
                      <h3>Join our Successfull Team</h3>
                      {/* <span>Fetured Action</span> */}
                    </div>
                  </div>
                </Link>
              </div>
              <div className="col-md-4">
                <Link to="/news_blog" className="mess-gallery-link">
                  <div className="mess-gallery-section-Blog">
                    <div className="mess-Gallery-section-title">
                      <h3>Our Property News</h3>
                      {/* <span>Fetured Action</span> */}
                    </div>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      
      <Modal
        id="myModal_no_page_load"
        show={open}
        onHide={() => setOpen(false)}
      >
        <Modal.Header closeButton>
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-hidden="true"
          >
            ×
          </button>
          <h4>Welcome to e-Cyprusauctions.com</h4>
          <p>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry
          </p>
        </Modal.Header>
        <Modal.Body>
          {' '}
          <form>
            <div className="form-group">
              <label htmlFor="Name">
                Name <span>*</span>{' '}
              </label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter your first name and last name"
              />
            </div>
            <div className="form-group">
              <label htmlFor="Email">
                Email <span>*</span>{' '}
              </label>
              <input
                type="email"
                className="form-control"
                placeholder="Enter your email address"
              />
            </div>
            <div className="form-group">
              <label htmlFor="Email">Phone number </label>
              <div className="Country_code">
                <select name="CountryCode" id="CountryCode">
                  <option value={+357}>Cyprus (+357)</option>
                  <option value={+91}>India (+91)</option>
                </select>
                <input
                  type="tel"
                  className="form-control"
                  placeholder="Enter your phone number"
                />
              </div>
            </div>
            <div className="text-center">
              <button
                type="submit"
                className="btn btn-modal-submit"
                data-dismiss="modal"
                aria-hidden="true"
                onClick={(e) => {
                  e.preventDefault();
                  setOpen(false);
                }}
              >
                Submit
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>
      {/* <Modal
        id="myModal_no_page_load"
        show={open}
        onHide={() => setOpen(false)}
      >
        <div className="modal-dialog">
          <div className="modal-content">
            <div className="modal-header">
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-hidden="true"
              >
                ×
              </button>
              <h4>Welcome to e-Cyprusauctions.com</h4>
              <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry
              </p>
            </div>
            <div className="modal-body">
              <form>
                <div className="form-group">
                  <label htmlFor="Name">
                    Name <span>*</span>{' '}
                  </label>
                  <input
                    type="text"
                    className="form-control"
                    placeholder="Enter your first name and last name"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="Email">
                    Email <span>*</span>{' '}
                  </label>
                  <input
                    type="email"
                    className="form-control"
                    placeholder="Enter your email address"
                  />
                </div>
                <div className="form-group">
                  <label htmlFor="Email">Phone number </label>
                  <div className="Country_code">
                    <select name="CountryCode" id="CountryCode">
                      <option value={+357}>Cyprus (+357)</option>
                      <option value={+91}>India (+91)</option>
                    </select>
                    <input
                      type="tel"
                      className="form-control"
                      placeholder="Enter your phone number"
                    />
                  </div>
                </div>
                <div className="text-center">
                  <button
                    type="submit"
                    className="btn btn-modal-submit"
                    data-dismiss="modal"
                    aria-hidden="true"
                  >
                    Submit
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </Modal>
      
       */}
      {/* </div> */}
    </main>
  );
}
