import React from 'react'

export default function PropertiesShortList() {
    return (
       <main style={{backgroundColor: '#f1f1f1'}}>
  <section className="second-page_bg second_page_1_bg">
    <h2 style={{textTransform: 'uppercase'}}>
      Properties Shortlist
    </h2>
  </section>
  {/* section search start */}
  <section className="Second-Page_Discover">
    <div className="container search_container second_page_discover_info">
      <h3>
        Discover your perfect property in Cyprus
      </h3>
      <div className="search_dp_section">
        <div className="row">
          <div className="col-md-3">
            <label>DISTRICT</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Any
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Any</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label> AREA / VILLAGE</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose Area / Village
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>TYPE</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Any
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>SUB TYPE</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose Sub-Type
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>CATEGORY</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose Category
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>PRICE</label>
            <div className="slider-range_Price_section">
              <span>Price €0 - €10,000,000+</span>
            </div>
            <div className="slider-range__Price">
              <div id="slider-range" />
            </div>
          </div>
          <div className="col-md-3">
            <label>LOT NUMBER</label>
            <input type="text" name="LOTNo" id="LOTNo" style={{width: '100%'}} placeholder="Enter Lot Number" />
          </div>
          <div className="col-md-3">
            <label>&nbsp;</label><br />
            <button className="searc_sc_btn">
              <img src="images/search.svg" alt="alt-img"  />
              Search</button>
          </div>
        </div>
      </div>
      <div className="rest_show_search_section" style={{paddingBottom: 15}}>
        <a href="#">Reset</a>
        <a id="advance-search">Advance Search</a>
      </div>
      <div className="search_dp_section advance_Search_Section">
        <div className="row">
          <div className="col-md-3">
            <label>NEIGHBOURHOOD</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose Neighbourhood
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>LAND</label>
            <div className="slider-range_Price_section">
              <span>m<small>2</small> 0 - 59000</span>
            </div>
            <div className="slider-range__Price">
              <div id="land" />
            </div>
          </div>
          <div className="col-md-3">
            <label>BEDROOMS</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Any
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>NEW / USED</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose Sub-Type
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>REMOVE LISTINGS</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose Remove listings
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>COVERED AREA</label>
            <div className="slider-range_Price_section">
              <span>m<small>2</small> 0 - 59000</span>
            </div>
            <div className="slider-range__Price">
              <div id="coverd-area" />
            </div>
          </div>
          <div className="col-md-3">
            <label>BEDROOMS</label>
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Any
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
          <div className="col-md-3">
            <label>NEW / USED</label><br />
            <div className="dropdown">
              <button className="btn btn-Search_drop_D dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Choose Sub-Type
              </button>
              <div className="drop_custome-show dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a className="dropdown-item" href="#">Action</a>
                <a className="dropdown-item" href="#">Another action</a>
                <a className="dropdown-item" href="#">Something else here</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  {/* section search end */}
  <section className="gird-properties">
    <div className="container text-center">
      <h4>Properties Shortlist</h4>
    </div>
    <div className="container">
      <div className="list_view_toggle">
        <div className="list-view_toggle_btn">
          <p>Show favorite listings</p>
          <label className="switch">
            <input type="checkbox" />
            <span className="slider-toggle round" />
          </label>
        </div>
        <div className="create_search_list_view_pg">
          <p>Create a saved search</p>
          <img src="images/edit-2.svg" alt="edit-2" />
        </div>
        <div className="short-list">
          <a href="properties-shortlist-grid.html">
            <img src="images/grid-normal.svg" alt="alt-img"  />
          </a>
          <a href="properties-shortlist-list.html">
            <img src="images/list-active.svg" alt="alt-img"  />
          </a>
          <div className="dropdown">
            <button className="btn btn-short-dp dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              Short by
            </button>
            <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a className="dropdown-item" href="#">Popular</a>
              <a className="dropdown-item" href="#">Low - High</a>
              <a className="dropdown-item" href="#">High - Low</a>
            </div>
          </div>
        </div>
      </div>
      <div className="list_view_property_deti_info">
        <div className="list_view_property_img" style={{background: 'url(images/275734-2-bed-house-for-sale-in-koilani-limassol-cyprus_full.jpg)', backgroundSize: 'cover'}}>
          <div className="favorite_heart_icon_list">
            <img src="images/heart-uncheck.svg" alt onclick="this.src='images/active-hart.svg'" />
          </div>
        </div>
        <div className="list_view_property_full_info">
          <div className="list_view_property-title_info">
            <div className="list_view_property-title">
              <h4>Detached House in Koilani</h4>
              <span>Starts Monday, 02/02/2020 at 01:00:00 pm</span>
            </div>
            <div className="list_view_property-info">
              <div className="d-flex justify-content-between">
                <div className="lot_no_list">
                  <span>
                    Lot No. :
                  </span>
                  <strong>
                    12346578
                  </strong>
                </div>
                <div className="lot_no_list">
                  <span>
                    Category :
                  </span>
                  <strong>
                    Residential
                  </strong>
                </div>
                <div className="lot_no_list">
                  <span>
                    Sub-category :
                  </span>
                  <strong>
                    House
                  </strong>
                </div>
              </div>
              <div className="description_list_view">
                <p><strong>Description</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim diam quis enim lobortis scelerisque. Lobortis mattis aliquam faucibus purus in. Feugiat in fermentum posuere urna nec tincidunt praesent semper. Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Dis parturient montes nascetur ridiculus mus mauris. Scelerisque mauris pellentesque pulvinar pellentesque. Donec pretium vulputate sapien nec sagittis aliquam malesuada. </p>
              </div>
            </div>
          </div>
          <div className="start_price_list_view">
            <div className="start_price">
              <span>Start price :</span>
              <span>$ 373K</span>
            </div>
            <button className="view_details_btn" onclick="window.location.href='properties_item.html'">View Details</button>
          </div>
        </div>
      </div>
      <div className="list_view_property_deti_info">
        <div className="list_view_property_img" style={{background: 'url(images/296976-3-bed-house-for-sale-in-souni-zanakia-limassol-cyprus_full.jpg)', backgroundSize: 'cover'}}>
          <div className="favorite_heart_icon_list">
            <button>
              <img src="images/heart-uncheck.svg" alt onclick="this.src='images/active-hart.svg'" />
            </button>
          </div>
        </div>
        <div className="list_view_property_full_info">
          <div className="list_view_property-title_info">
            <div className="list_view_property-title">
              <h4>Detached House in Koilani</h4>
              <span>Starts Monday, 02/02/2020 at 01:00:00 pm</span>
            </div>
            <div className="list_view_property-info">
              <div className="d-flex justify-content-between">
                <div className="lot_no_list">
                  <span>
                    Lot No. :
                  </span>
                  <strong>
                    12346578
                  </strong>
                </div>
                <div className="lot_no_list">
                  <span>
                    Category :
                  </span>
                  <strong>
                    Residential
                  </strong>
                </div>
                <div className="lot_no_list">
                  <span>
                    Sub-category :
                  </span>
                  <strong>
                    House
                  </strong>
                </div>
              </div>
              <div className="description_list_view">
                <p><strong>Description</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim diam quis enim lobortis scelerisque. Lobortis mattis aliquam faucibus purus in. Feugiat in fermentum posuere urna nec tincidunt praesent semper. Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Dis parturient montes nascetur ridiculus mus mauris. Scelerisque mauris pellentesque pulvinar pellentesque. Donec pretium vulputate sapien nec sagittis aliquam malesuada. </p>
              </div>
            </div>
          </div>
          <div className="start_price_list_view">
            <div className="start_price">
              <span>Start price :</span>
              <span>$ 373K</span>
            </div>
            <div className="start_bid_timer text-center">
              <img src="images/timer-32x32.svg" alt="timer-32x32" />
              <p>2M:13D:5M:32S</p>
            </div>
            <button className="view_details_btn" onclick="window.location.href='properties_item.html'">View Details</button>
          </div>
        </div>
      </div>
      <div className="list_view_property_deti_info">
        <div className="list_view_property_img" style={{background: 'url(images/list-imag-3.jpg)', backgroundSize: 'cover'}}>
          <div className="favorite_heart_icon_list">
            <button>
              <img src="images/heart-uncheck.svg" alt onclick="this.src='images/active-hart.svg'" />
            </button>
          </div>
        </div>
        <div className="list_view_property_full_info">
          <div className="list_view_property-title_info">
            <div className="list_view_property-title">
              <h4>Detached House in Koilani</h4>
              <span>Starts Monday, 02/02/2020 at 01:00:00 pm</span>
            </div>
            <div className="list_view_property-info">
              <div className="d-flex justify-content-between">
                <div className="lot_no_list">
                  <span>
                    Lot No. :
                  </span>
                  <strong>
                    12346578
                  </strong>
                </div>
                <div className="lot_no_list">
                  <span>
                    Category :
                  </span>
                  <strong>
                    Residential
                  </strong>
                </div>
                <div className="lot_no_list">
                  <span>
                    Sub-category :
                  </span>
                  <strong>
                    House
                  </strong>
                </div>
              </div>
              <div className="description_list_view">
                <p><strong>Description</strong></p>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim diam quis enim lobortis scelerisque. Lobortis mattis aliquam faucibus purus in. Feugiat in fermentum posuere urna nec tincidunt praesent semper. Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Dis parturient montes nascetur ridiculus mus mauris. Scelerisque mauris pellentesque pulvinar pellentesque. Donec pretium vulputate sapien nec sagittis aliquam malesuada. </p>
              </div>
            </div>
          </div>
          <div className="start_price_list_view">
            <div className="start_price">
              <span>Start price :</span>
              <span>$ 373K</span>
            </div>
            <div className="start_bid_timer text-center">
              <img src="images/timer-32x32.svg" alt="timer-32x32" />
              <p>2M:13D:5M:32S</p>
            </div>
            <button className="view_details_btn" onclick="window.location.href='properties_item.html'">View Details</button>
          </div>
        </div>
      </div>
    </div>
  </section>
  <section className="pagination">
    <div className="container">
      <div className="pagination_second-pg">
        <a href="#" className="active">
          01
        </a>
        <a href="#">
          02
        </a>
        <a href="#">
          03
        </a>
        <a href="#">
          04
        </a>
        <a href="#" className="multi-pagination">
          <img src="images/three-dot.svg" alt="alt-img"  />
        </a>
        <a href="#">
          99
        </a>
      </div>
    </div>
  </section>
</main>

    )
}
