import React, { useState, useRef } from 'react';
import { Tab, Nav, Modal } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ReactToPrint from 'react-to-print';
import { getGlobalLangSelecter } from '../../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import Guide from '../../images/guide.svg';
import Outline from '../../images/Outline.svg';
import PrintProperty from '../../images/print-properties_main.svg';
import RealEstate from '../../images/real-estate.svg';

// import Printer, { print } from 'react-pdf-print';

function AfterBidProperties(props) {
  const componentRef = useRef();
  const [viewProperty, setViewProperty] = useState(false);
  return (
    <main>
      <section>
        <div className="container">
          <div className="single_properties_title">
            <h2>{props.Language.det_hou}</h2>
          </div>
        </div>
        <div className="container properties_single_pg">
          {/*Carousel Wrapper*/}
          <div
            id="carousel-thumb"
            className="carousel slide carousel-fade carousel-thumbnails"
            data-ride="carousel"
          >
            {/*Slides*/}
            <div className="carousel-inner" role="listbox">
              <div className="carousel-item active">
                <img
                  className="d-block w-100"
                  src="../images/slider-1.jpg"
                  alt="First slide"
                />
                <div className="carousel_heart">
                  <img
                    src="../images/heart-uncheck.svg"
                    alt
                    onclick="this.src='images/active-hart.svg'"
                  />
                </div>
              </div>
              <div className="carousel-item">
                <img
                  className="d-block w-100"
                  src="../images/slider-1-1-new.jpg"
                  alt="Second slide"
                />
                <div className="carousel_heart">
                  <img
                    src="../images/heart-uncheck.svg"
                    alt
                    onclick="this.src='images/active-hart.svg'"
                  />
                </div>
              </div>
            </div>

            {/*/.Controls*/}
            <ol
              className="carousel-indicators owl-carousel owl-theme thum-slider"
              id="thum_slider"
            >
              <li data-target="#carousel-thumb" data-slide-to={0}>
                <img className="d-block w-100" src="../images/slider-1-5.jpg" />
              </li>
              <li
                data-target="#carousel-thumb"
                data-slide-to={1}
                className="active"
              >
                {' '}
                <img className="d-block w-100" src="../images/slider-1-1.jpg" />
              </li>
              <li data-target="#carousel-thumb" data-slide-to={2}>
                <img className="d-block w-100" src="../images/slider-1-2.jpg" />
              </li>
              <li data-target="#carousel-thumb" data-slide-to={3}>
                <img className="d-block w-100" src="../images/slider-1-3.jpg" />
              </li>
              <li data-target="#carousel-thumb" data-slide-to={4}>
                <img className="d-block w-100" src="../images/slider-1-4.jpg" />
              </li>
              <li data-target="#carousel-thumb" data-slide-to={5}>
                <img className="d-block w-100" src="../images/slider-1-6.jpg" />
              </li>
              <li data-target="#carousel-thumb" data-slide-to={6}>
                <img className="d-block w-100" src="../images/slider-1-7.jpg" />
              </li>
            </ol>
          </div>
          {/*/.Carousel Wrapper*/}
        </div>
      </section>
      <section>
        <div className="container">
          <div className="Properties_second_Section_page">
            <div className="Properties_Bid_details_One">
              <p>
                {props.Language.lo_no}.: <strong>12345648</strong>
              </p>
              <p>
                {props.Language.categ} : <strong>{props.Language.resid}</strong>
              </p>
              <p>
                {props.Language.sub_cate} :{' '}
                <strong>{props.Language.hou_s}</strong>
              </p>
              <p>
                {props.Language.bid_depo} : <strong>€1250</strong>
              </p>
            </div>
            <div className="bid_price_enter">
              <div className="start_price_bid_properties">
                <p>{props.Language.st_price} :</p>
                <div className>
                  <label htmlFor>€85,000</label>
                </div>
                <div className="after_bid_properties_total_bid">
                  <span>Total Bids [10 Bids]</span>
                </div>
                {/* <div class="text-center">
                  <button class="submit_offer_start_btn" data-toggle="modal" data-target="#myModal_no_page_load_first">
                      Submit offer
                  </button>
                  </div> */}
              </div>
            </div>
            <div className="bid_Deposit_properties_single">
              <div className="start_price_bid_properties">
                <p>
                  {props.Language.bid_depo} : *{props.Language.fully_ref}
                </p>
                <div className>
                  <label htmlFor>€5,000</label>
                </div>
                <div className="place_bid_after_page_bid_btn">
                  <p>{props.Language.pl_bid}:</p>
                  <div className="place_bid_fild_bid">
                    <input
                      type="text"
                      name="fideBid"
                      id="fideBid"
                      placeholder="Enter €105,000 or more amount"
                    />
                    <button>{props.Language.pl_bid}</button>
                  </div>
                </div>
                {/* <div class="text-center">
                  <button class="submit_offer_start_btn">
                      Registration Opens on 02/04 01:00PM
                  </button>
                  </div> */}
              </div>
            </div>
            <div className="bid_timer_start_properties_single">
              <div className="bid_timer_start_properties_single_title">
                <img
                  src="../images/clock_properties.svg"
                  alt="clock_properties"
                />
                <span>{props.Language.bid_s}</span>
              </div>
              <div className="bid_timer_timer" id="countdown">
                <div>
                  <p id="day">{/* 22 */}</p>
                  <span>{props.Language.da_ys}</span>
                </div>
                <div>
                  <p id="hour">{/* 23 */}</p>
                  <span>{props.Language.hr_s}</span>
                </div>
                <div>
                  <p id="min">{/* 24 */}</p>
                  <span>{props.Language.min_t}</span>
                </div>
                <div>
                  <p id="sec">{/* 26 */}</p>
                  <span>{props.Language.sec_on}</span>
                </div>
              </div>
            </div>
          </div>
          <div className="Start_lable_Property_page">
            <p>{props.Language.st_th}, 02/04/2020 at 01:00:00 pm</p>
          </div>
        </div>
      </section>
      <section className="container">
        <div className="social_icon_share starting_social_icon">
          <span>{props.Language.sh_a} :</span>
          <a href="#" style={{ marginRight: 27 }}>
            <img src="../images/linkedin-in-brands.png" alt />
          </a>
          <a href="#" style={{ marginRight: 27 }}>
            <img src="../images/youtube_properties.svg" alt />
          </a>
          <a href="#" style={{ marginRight: 27 }}>
            <img src="../images/facebook-f-brands.png" alt />
          </a>
          <a href="#" style={{ marginRight: 27 }}>
            <img src="../images/instagram-sketched.png" alt />
          </a>
          <a href="#">
            <img src="../images/twitter-brands.png" alt />
          </a>
        </div>
      </section>
      <section className="bid_history">
        <div className="container">
          <div className="table_heading">
            <div className="table_history">
              <table>
                <tbody>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.bid_amt} : €85,000</span>
                    </td>
                  </tr>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.bid_amt} : €85,000</span>
                    </td>
                  </tr>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.bid_amt} : €85,000</span>
                    </td>
                  </tr>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.bid_amt} : €85,000</span>
                    </td>
                  </tr>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.bid_amt} : €85,000</span>
                    </td>
                  </tr>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.bid_amt} : €85,000</span>
                    </td>
                  </tr>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.bid_amt} : €85,000</span>
                    </td>
                  </tr>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.bid_amt} : €85,000</span>
                    </td>
                  </tr>
                  <tr>
                    <td>{props.Language.ref_nu} : 12345678</td>
                    <td>
                      {props.Language.loc} : {props.Language.tex_as}
                    </td>
                    <td>02/02/2020 01:57:45 PM</td>
                    <td>
                      <span>{props.Language.loc} : €85,000</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <label>{props.Language.bid_his}</label>
          </div>
        </div>
      </section>
      <section className="four_big_btn_section">
        <div className="container">
          <div className="row">
            <div className="col-lg-3 col-md-3">
              <Link to="/buyers-guide">
                <button className="btn_properti_View">
                  <img src={Guide} alt />
                  <span>{props.Language.b_y}</span>
                </button>
              </Link>
            </div>
            <div className="col-lg-3 col-md-3">
              <Link to="/faq">
                <button className="btn_properti_View mr_58">
                  <img src={Outline} alt />
                  <span>{props.Language.fa_q}</span>
                </button>
              </Link>
            </div>
            <div className="col-lg-3 col-md-3">
              {/* <Link to="/view-property"> */}
              <button
                className="btn_properti_View"
                onClick={() => setViewProperty(true)}
              >
                <img src={RealEstate} alt />
                <span>{props.Language.vie_pr}</span>
              </button>
              {/* </Link> */}
            </div>
            <div className="col-lg-3 col-md-3">
              <ReactToPrint
                trigger={() => (
                  <button className="btn_properti_View">
                    <img src={PrintProperty} alt />
                    <span>Print Brochure</span>
                  </button>
                )}
                content={() => componentRef.current}
              />
            </div>
          </div>
        </div>
      </section>
      {/* View Properties */}
      <Modal
        id="view_properties"
        className="Properties_single_properties"
        show={viewProperty}
        onHide={() => setViewProperty(false)}
      >
        <Modal.Header closeButton>
          <button
            type="button"
            className="close"
            data-dismiss="modal"
            aria-hidden="true"
          >
            ×
          </button>
          <h4>View Property</h4>
        </Modal.Header>
        <Modal.Body>
          <form>
            <div className="form-group">
              <label htmlFor="Name">Name</label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter your first name and last name"
                defaultValue="Bruce Robin"
              />
            </div>
            <div className="form-group">
              <label htmlFor="Email">Email</label>
              <input
                type="email"
                className="form-control"
                placeholder="Enter your email address"
                defaultValue="bruce1234@mail.com"
              />
            </div>
            <div className="form-group">
              <label htmlFor="Email">Phone Number </label>
              <input
                type="text"
                className="form-control"
                placeholder="Enter your number"
                defaultValue="+357 1234 5678"
              />
            </div>
            <div className="form-group">
              <p>Date &amp; Time of planning to view property</p>
              <div className="time_date_view_properties">
                <input type="date" className="form-control" />
                <input type="time" className="form-control" />
              </div>
            </div>
            <div className="next_cancle_popup_btn_section">
              <button
                className="btn btn-modal-submit"
                data-dismiss="modal"
                aria-hidden="true"
              >
                Cancel
              </button>
              <button
                type="submit"
                className="btn btn-modal-next"
                onclick
                id="view-propertise-btn"
              >
                Submit
              </button>
            </div>
          </form>
        </Modal.Body>
      </Modal>

      {/* View Properties */}

      <section className="tab_view_properties_details">
        <div className="container">
          <div className="tab_Propertie_singlesss">
            <Tab.Container
              id="left-tabs-example"
              defaultActiveKey="Description"
            >
              <div className="row">
                <div className="col-sm-3 px-0 tab_nav_propertiesss">
                  <Nav
                    className="nav flex-column nav-pills"
                    variant="pills"
                    className="flex-column"
                  >
                    <Nav.Link eventKey="Description">
                      {props.Language.deeees}
                    </Nav.Link>
                    <Nav.Link eventKey="Brochure">
                      {props.Language.br_c}
                    </Nav.Link>
                    <Nav.Link eventKey="Legal-Documents">
                      {props.Language.lg_doc}
                    </Nav.Link>

                    <Nav.Link eventKey="Map">{props.Language.m_p}</Nav.Link>
                  </Nav>
                </div>
                <div className="col-sm-9 px-0 tab_inner_Section_details_most">
                  <Tab.Content>
                    <Tab.Pane eventKey="Description">
                      <div className="tab_inner_Section_details_most" ref={componentRef}>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore magna aliqua. Dignissim diam quis enim lobortis
                          scelerisque. Lobortis mattis aliquam faucibus purus
                          in. Feugiat in fermentum posuere urna nec tincidunt
                          praesent semper. Ut enim blandit volutpat maecenas
                          volutpat blandit aliquam etiam erat. Dis parturient
                          montes nascetur ridiculus mus mauris.
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore magna aliqua. Dignissim diam quis enim lobortis
                          scelerisque. Lobortis mattis aliquam faucibus purus
                          in. Feugiat in fermentum posuere urna nec tincidunt
                          praesent semper. Ut enim blandit volutpat maecenas
                          volutpat blandit aliquam etiam erat. Dis parturient
                          montes nascetur ridiculus mus mauris.
                        </p>
                        <p>
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do eiusmod tempor incididunt ut labore et
                          dolore magna aliqua. Dignissim diam quis enim lobortis
                          scelerisque. Lobortis mattis aliquam faucibus purus
                          in. Feugiat in fermentum posuere urna nec tincidunt
                          praesent semper. Ut enim blandit volutpat maecenas
                          volutpat blandit aliquam etiam erat. Dis parturient
                          montes nascetur ridiculus mus mauris.
                        </p>
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="Brochure">
                      <div className="tab_inner_Section_details_most">
                        <div className="tab_brouchure">
                          <ul>
                            <li>
                              <a
                                href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                                target="_blank"
                              >
                                Brochure_one.pdf
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="Legal-Documents">
                      <div className="tab_inner_Section_details_most">
                        <div className="tab_brouchure">
                          <ul>
                            <li>
                              <a
                                href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                                target="_blank"
                              >
                                Brochure_one.pdf
                              </a>
                            </li>
                            <li>
                              <a
                                href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                                target="_blank"
                              >
                                Brochure_one.pdf
                              </a>
                            </li>
                            <li>
                              <a
                                href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                                target="_blank"
                              >
                                Brochure_one.pdf
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="Map">
                      <div className="tab_inner_Section_details_most">
                        {' '}
                        {/* <div className="tab_brouchure">  */}
                        <iframe
                          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27050.039502475036!2d-1.5744919973476545!3d53.18362487776462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x25a3b1142c791a9%3A0xc4f8a0433288257a!2sUnited%20Kingdom!5e0!3m2!1sen!2sin!4v1593769316045!5m2!1sen!2sin"
                          width="100%"
                          height={450}
                          frameBorder={0}
                          style={{ border: 0 }}
                          allowFullScreen
                          aria-hidden="false"
                          tabIndex={0}
                        />
                      </div>
                    </Tab.Pane>
                  </Tab.Content>
                </div>
              </div>
            </Tab.Container>
          </div>
        </div>
      </section>
      <section className="similar_properties_single">
        <div className="container">
          <h2>Similar Properties</h2>
        </div>
        <div className="container">
          <div className="row">
            <div className="col-md-3 col-lg-3">
              <div className="property_item">
                <Link to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                    {/* <div class="similar_heart_fav">
            <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='images/active-hart.svg'">
          </div> */}
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </Link>
                <div className="similar_heart_fav">
                  <img
                    src="../images/heart-uncheck.svg"
                    alt
                    onclick="this.src='images/active-hart.svg'"
                  />
                </div>
              </div>
            </div>
            <div className="col-md-3 col-lg-3">
              <div className="property_item">
                <Link to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                    {/* <div class="similar_heart_fav">
            <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='images/active-hart.svg'">
          </div> */}
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </Link>
                <div className="similar_heart_fav">
                  <img
                    src="../images/heart-uncheck.svg"
                    alt
                    onclick="this.src='images/active-hart.svg'"
                  />
                </div>
              </div>
            </div>
            <div className="col-md-3 col-lg-3">
              <div className="property_item">
                <Link to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                    {/* <div class="similar_heart_fav">
            <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='images/active-hart.svg'">
          </div> */}
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </Link>
                <div className="similar_heart_fav">
                  <img
                    src="../images/heart-uncheck.svg"
                    alt
                    onclick="this.src='images/active-hart.svg'"
                  />
                </div>
              </div>
            </div>
            <div className="col-md-3 col-lg-3">
              <div className="property_item">
                <Link to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                    {/* <div class="similar_heart_fav">
            <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='images/active-hart.svg'">
          </div> */}
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </Link>
                <div className="similar_heart_fav">
                  <img
                    src="../images/heart-uncheck.svg"
                    alt
                    onclick="this.src='images/active-hart.svg'"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}
const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AfterBidProperties);
