import React from 'react';
import { getGlobalLangSelecter } from '../../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

function JoinOurTeam(props) {
  return (
    <main>
      <section className="contact_form_section">
        <div className="container">
          <div className="contact-title">
  <h2>{props.Language.jo_ou_su}</h2>
          </div>
          <div className="row contact_form_contact_info">
            <div className="col-md-6">
              <img
                src="images/workplace-1245776_1920.jpg"
                alt="contact-main-banner"
                className="img-fluid"
              />
            </div>
            <div className="col-md-6">
              <form className="contact-form_form">
                <input type="text" placeholder="Name" />
                <input type="text" placeholder="Email" />
                <input type="text" placeholder="Phone" />
                <div className="resume_upload">
                  <input type="file" id="real-file" hidden="hidden" />
                  <button type="button" id="custom-button">
                    {props.Language.up_res}
                  </button>
                  <span id="custom-text">{props.Language.no_fi_ch}.</span>
                </div>
                <div className="text-center">
  <button>{props.Language.subm}</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(JoinOurTeam);