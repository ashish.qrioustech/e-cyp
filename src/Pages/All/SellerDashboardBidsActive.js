import React from 'react';

export default function SellerDashboardBidsActive() {
  return (
    <main>
      <div className="container">
        <div className="dashboad_row row">
          <div className="col-md-3 col-lg-3">
            <div className="dashboard_title">
              <h4>DASHBORD</h4>
            </div>
            <div className="dashboard_profile_img text-center">
              <img src="images/profile-img.jpg" alt="profile" />
              <div className="edit_btn_profile">
                <img src="images/edit-btn-profile.svg" alt="alt-img"  />
              </div>
            </div>
            <div className="dashbard_nav_items">
              <ul>
                <li>
                  <a href="#">Profile</a>
                </li>
                <li className="active-nav">
                  <a href="#">My Bids</a>
                </li>
                <li>
                  <a href="#">Favourites</a>
                </li>
                <li>
                  <a href="#">Payments</a>
                </li>
                <li>
                  <a href="#">Notifications</a>
                </li>
              </ul>
            </div>
          </div>
          <div className="col-md-9 col-lg-9">
            <div className="bids_detail_seller_dash">
              <ul className="nav nav-tabs" id="myTab" role="tablist">
                <li className="nav-item">
                  <a
                    className="nav-link active"
                    id="home-tab"
                    data-toggle="tab"
                    href="#Bids-Active"
                    role="tab"
                    aria-controls="Bids-Active"
                    aria-selected="true"
                  >
                    Bids Active
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    id="bids_won-tab"
                    data-toggle="tab"
                    href="#bids_won"
                    role="tab"
                    aria-controls="bids_won"
                    aria-selected="false"
                  >
                    Bids Won
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    id="bid_lots-tab"
                    data-toggle="tab"
                    href="#bid_lots"
                    role="tab"
                    aria-controls="bid_lots"
                    aria-selected="false"
                  >
                    Bid Lots
                  </a>
                </li>
                <li className="nav-item">
                  <a
                    className="nav-link"
                    id="bid_lots-tab"
                    data-toggle="tab"
                    href="#upcoming"
                    role="tab"
                    aria-controls="bid_lots"
                    aria-selected="false"
                  >
                    Upcoming
                  </a>
                </li>
              </ul>
              <div className="tab-content" id="myTabContent">
                <div
                  className="tab-pane fade show active"
                  id="Bids-Active"
                  role="tabpanel"
                  aria-labelledby="Bids-Active-tab"
                >
                  <div className="bid_table_view_Seller">
                    <table>
                      <tbody>
                        <tr>
                          <th>#</th>
                          <th>Lot No</th>
                          <th>Property Name</th>
                          <th>Category</th>
                          <th>Bid Placed</th>
                          <th>Bid Deposit</th>
                          <th>Time Left</th>
                          <th>My Bid</th>
                          <th>Action</th>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>€1000</td>
                          <td>04D:12H:24M</td>
                          <td>€1250</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>€1000</td>
                          <td>04D:12H:24M</td>
                          <td>€1250</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>€1000</td>
                          <td>04D:12H:24M</td>
                          <td>€1250</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>€1000</td>
                          <td>04D:12H:24M</td>
                          <td>€1250</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>€1000</td>
                          <td>04D:12H:24M</td>
                          <td>€1250</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>€1000</td>
                          <td>04D:12H:24M</td>
                          <td>€1250</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>€1000</td>
                          <td>04D:12H:24M</td>
                          <td>€1250</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="bids_won"
                  role="tabpanel"
                  aria-labelledby="bids_won-tab"
                >
                  <div className="bid_table_view_Seller">
                    <table>
                      <tbody>
                        <tr>
                          <th>#</th>
                          <th>Lot No</th>
                          <th>Property Name</th>
                          <th>Category</th>
                          <th>Deposit</th>
                          <th>Bought on</th>
                          <th>Price</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>€500</td>
                          <td>02/02/2020</td>
                          <td>€1250</td>
                          <td>Paid</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>€500</td>
                          <td>02/02/2020</td>
                          <td>€1250</td>
                          <td>Paid</td>
                          <td>
                            <button className="btn_Paynow">Paynow</button>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="bid_lots"
                  role="tabpanel"
                  aria-labelledby="bid_lots-tab"
                >
                  <div className="bid_table_view_Seller">
                    <table>
                      <tbody>
                        <tr>
                          <th>#</th>
                          <th>Lot No</th>
                          <th>Property Name</th>
                          <th>Category</th>
                          <th>Bid Placed</th>
                          <th>My Bid</th>
                          <th>Sold for</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>€1000</td>
                          <td>€1250</td>
                          <td>Sold</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <div
                  className="tab-pane fade"
                  id="upcoming"
                  role="tabpanel"
                  aria-labelledby="upcoming_lots-tab"
                >
                  <div className="bid_table_view_Seller">
                    <table>
                      <tbody>
                        <tr>
                          <th>#</th>
                          <th>Lot No</th>
                          <th>Property Name</th>
                          <th>Category</th>
                          <th>Starts on</th>
                          <th>Location</th>
                          <th>Time left</th>
                          <th>Action</th>
                          <th />
                        </tr>
                        <tr>
                          <td>01</td>
                          <td>
                            <span>1234567</span>
                          </td>
                          <td>Penthouse in Lima...</td>
                          <td>Residential</td>
                          <td>02/02/2020</td>
                          <td>Limassol</td>
                          <td>04D:12H:24M</td>
                          <td>
                            <button className="btn_view">View</button>
                          </td>
                          <td>
                            <a href="#">
                              <img src="images/delete-24px.svg" alt="alt-img"  />
                            </a>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
}
