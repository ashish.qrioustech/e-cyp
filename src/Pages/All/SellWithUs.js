import React from 'react';
import { getGlobalLangSelecter } from '../../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

function SellWithUs(props) {
  return (
    <main>
      <section className="contact_form_section shell_bg">
        <div className="container">
          <div className="contact-title">
  <h2>{props.Language.sell_wi}</h2>
          </div>
          <div className="row contact_form_contact_info shell-us_form_info">
            <div className="col-md-6">
              <img
                src="images/sell-with-us.svg"
                alt="contact-main-banner"
                className="img-fluid"
              />
            </div>
            <div className="col-md-6">
              <form className="contact-form_form">
                <input type="text" placeholder="Name" />
                <input type="email" placeholder="Email" />
                <div className="phone_sell_us">
                  <select name="phone_Sell" id="phone_Sell">
                    <option value={+91}>Cyprus (+357)</option>
                    <option value={+91}>India (+91)</option>
                    <option value={+91}>India (+91)</option>
                  </select>
                  <input
                    type="tel"
                    name="shell_us_phone"
                    id="shell_us_phone"
                    placeholder="Phone"
                  />
                </div>
                <div className="row">
                  <div className="col-lg-6">
                    <div className="custom_drop_shell">
                      <select name="phone_Sell" id="phone_Sell">
  <option value={+91}>{props.Language.prop_cat}</option>
                        <option value={+91} />
                        <option value={+91} />
                      </select>
                    </div>
                  </div>
                  <div className="col-lg-6">
                    <div className="custom_drop_shell">
                      <select name="phone_Sell" id="phone_Sell">
  <option value={+91}>{props.Language.prop_sub}</option>
                        <option value={+91} />
                        <option value={+91} />
                      </select>
                    </div>
                  </div>
                </div>
                <div className="custom_drop_shell">
                  <select name="phone_Sell" id="phone_Sell">
  <option value={+91}>{props.Language.sel_prop}</option>
                    <option value={+91} />
                    <option value={+91} />
                  </select>
                </div>
                <div className="custom_drop_shell">
                  <select name="phone_Sell" id="phone_Sell">
  <option value={+91}>{props.Language.sel_dis}</option>
                    <option value={+91} />
                    <option value={+91} />
                  </select>
                </div>
                <div className="shell_radio-button">
  <p>{props.Language.is_this}?</p>
                  <div className="shell_radio-button_shell">
                    <label className="container_radio_btn">
                      {props.Language.ye}
                      <input
                        type="radio"
                        defaultChecked="checked"
                        name="radio"
                      />
                      <span className="checkmark" />
                    </label>
                    <label className="container_radio_btn">
                      {props.Language.no_no}
                      <input type="radio" name="radio" />
                      <span className="checkmark" />
                    </label>
                  </div>
                </div>
                <div className="text-center">
  <button>{props.Language.subm}</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SellWithUs);