import React from 'react';
import { Accordion, Card, Button } from 'react-bootstrap';
import { useEffect, useState } from 'react';
import { Link, useParams } from 'react-router-dom';
import Axios from 'axios';
import { getGlobalLangSelecter } from '../../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

function FAQ(props) {
  const [propertyDetails, setPropertyDetails] = useState({});
  let { id } = useParams();
  const [faqList, setFaqList] = useState([]);

  console.log('id ----', id);
  useEffect(() => {
    let params = {
      product_id: id,
      limit: 0,
      page: 0,
    };
    let auctionToken = localStorage.getItem('auctionToken');
    let header = {};
    header['authorization'] = 'Bearer ' + auctionToken;
    Axios.post(process.env.REACT_APP_API + 'get_faq_of_product', params, header)
      .then((res) => {
        if (res.data.success) {
          console.log('res------------------', res);
          setFaqList(res.data.data);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <main>
      <section className="contact_form_section">
        <div className="container">
          <div className="contact-title">
            <h2>{props.Language.fa_q}</h2>
          </div>
          {/* defaultActiveKey="0" */}
          <Accordion>
            {faqList.length?faqList.map((item, index) => {
              return (
                <Card>
                  <Card.Header>
                    <Accordion.Toggle
                      className="text-white text-left"
                      style={{ width: '98%' }}
                      as={Button}
                      variant="text"
                      eventKey={index}
                    >
                      {item.title}
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey={index}>
                    <Card.Body>
                      <p>{item.description}</p>
                    </Card.Body>
                  </Accordion.Collapse>
                </Card>
              );
            }):<h1>{props.Language.fa_q} EMPTY </h1>}
          </Accordion>
          {/* <div id="accordion" className="accordion">
            <div className="card mb-0">
              <div
                className="card-header collapsed"
                data-toggle="collapse"
                href="#collapseOne"
              >
                <a className="card-title">
                  How can I install/upgrade Dummy Content?
                </a>
              </div>
              <div
                id="collapseOne"
                className="card-body collapse"
                data-parent="#accordion"
              >
                <p>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life
                  accusamus terry richardson ad squid. 3 wolf moon officia aute,
                  non cupidatat skateboard dolor brunch. Food truck quinoa
                  nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                  aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                  beer labore wes anderson cred nesciunt sapiente ea proident.
                  Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                  beer farm-to-table, raw denim aesthetic synth nesciunt you
                  probably haven't heard of them accusamus labore sustainable
                  VHS.
                </p>
              </div>
              <div
                className="card-header collapsed"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapseTwo"
              >
                <a className="card-title">Dummy Content won't install...</a>
              </div>
              <div
                id="collapseTwo"
                className="card-body collapse"
                data-parent="#accordion"
              >
                <p>
                  Anim pariatur cliche reprehenderit, enim eiusmod high life
                  accusamus terry richardson ad squid. 3 wolf moon officia aute,
                  non cupidatat skateboard dolor brunch. Food truck quinoa
                  nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                  aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft
                  beer labore wes anderson cred nesciunt sapiente ea proident.
                  Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                  beer farm-to-table, raw denim aesthetic synth nesciunt you
                  probably haven't heard of them accusamus labore sustainable
                  VHS.
                </p>
              </div>
              <div
                className="card-header collapsed"
                data-toggle="collapse"
                data-parent="#accordion"
                href="#collapseThree"
              >
                <a className="card-title">How can I uninstall Dummy Content?</a>
              </div>
              <div
                id="collapseThree"
                className="collapse"
                data-parent="#accordion"
              >
                <div className="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life
                  accusamus terry richardson ad squid. 3 wolf moon officia aute,
                  non cupidatat skateboard dolor brunch. Food truck quinoa
                  nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt
                  aliqua put a bird on it squid single-origin coffee nulla
                  assumenda shoreditch et. samus labore sustainable VHS.
                </div>
              </div>
            </div>
          </div> */}
        </div>
      </section>
    </main>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(FAQ);
