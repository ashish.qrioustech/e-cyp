import React from 'react';
import { getGlobalLangSelecter } from '../../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

function OurServices(props) {
  return (
    <main>
      <section
        className="second-page_bg second_page_1_bg"
        style={{ backgroundImage: 'url(images/services-bg.jpg)' }}
      >
        <h2 style={{ textTransform: 'uppercase' }}>{props.Language.our_Serv}</h2>
      </section>
      <section className="mt-5">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div href className="card-visit-place">
                <a href="#">
                  <img src="images/valuation.jpg" alt className="img-fluid" />
  <h4>{props.Language.	
valua}</h4>
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div href className="card-visit-place">
                <a href="#">
                  <img
                    src="images/property-managment.jpg"
                    alt
                    className="img-fluid"
                  />
                  <h4>{props.Language.prop_man}</h4>
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div href className="card-visit-place">
                <a href="#">
                  <img
                    src="images/common-expenses-management.jpg"
                    alt
                    className="img-fluid"
                  />
                  <h4>{props.Language.comm_expen}</h4>
                </a>
              </div>
            </div>
          </div>
          <div className="row mt-3">
            <div className="col-md-4">
              <div href className="card-visit-place">
                <a className href="#">
                  <img
                    src="images/parmenet-racidency.jpg"
                    alt
                    className="img-fluid"
                  />
                  <h4>{props.Language.perm_resi}</h4>
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div href className="card-visit-place">
                <a href="#">
                  <img
                    src="images/company-formation.jpg"
                    alt
                    className="img-fluid"
                  />
                  <h4>{props.Language.com_forma}</h4>
                </a>
              </div>
            </div>
            <div className="col-md-4">
              <div href className="card-visit-place">
                <a href="#">
                  <img
                    src="images/bank-account-opening.jpg"
                    alt
                    className="img-fluid"
                  />
                  <h4>{props.Language.bank_acc}</h4>
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OurServices);