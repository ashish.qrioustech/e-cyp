import React, { useEffect } from 'react';
import fetchClient from '../../util/axiosConfig';

function BuyersGuide() {
  const Params = {
    limit: 5,
    page: 2,
    product_id: 5235,
  };
  useEffect(() => {
    fetchClient
      .post('get_buyersguide_of_product', Params)
      .then((res) => {
        console.log(res);
      })
      .catch((error) => {
        console.log(error);
      });
  }, []);
  return (
    <main>
      <section className="contact_form_section">
        <div className="container">
          <div className="contact-title mb-5">
            <h2>Buyer's Guide</h2>
          </div>
          <div className="content-buyers-guide-new mb-4">
            <h4>Discover your property</h4>
            <p>
              The entirety of our present properties are recorded here. On the
              off chance that you'd prefer to be advised when new properties in
              Cyprus are accessible, we'd be glad to send you refreshes.
            </p>
            <p>
              Every property is overseen by one of our expert assessors. They
              will be accessible to address any of your inquiries in the
              approach offering.
            </p>
            <p>
              Every property has a hold cost – the most minimal value that the
              dealer is eager to acknowledge for the property. It's a
              significant figure, as no property can sell for not as much as
              this. We'll generally give a sign of the save to give you a
              thought of where offering will begin. You will see this recorded
              as 'Hold' for every property.
            </p>
            <p>
              Open viewings for every property are additionally recorded so
              you'll have the option to visit the property and organize a study
              whenever required.
            </p>
          </div>
          <div className="content-buyers-guide-new mb-4">
            <h4>Due Diligence</h4>
            <p>
              The e-Cyprusacutions stage is completely straightforward. All
              pertinent legitimate documentation identifying with a property is
              given by the merchant's specialist and accessible on our site
              alongside the other data identifying with the property. You'll
              require a record to see these authoritative archives; it just
              pauses for a moment to make one.
            </p>
            <p>
              Looking at the authoritative records is a significant piece of the
              procedure on the grounds that our computerized deals are genuine –
              when a property is sold on the e-Cyprusacutions stage, a
              legitimate agreement has been shaped and the fruitful bidder is
              lawfully obliged to finish the buy.
            </p>
          </div>
          <div className="content-buyers-guide-new mb-4">
            <h4>Registration</h4>
            <p>
              In case you're keen on a property and need to offer, you'll have
              to enroll with us. A full enrollment manage is accessible here.
              This is a straight-forward procedure and includes giving evidence
              of recognizable proof, a selfie photograph and confirmation of
              address, just as giving an offering store. You will likewise need
              to acknowledge our T and C's so as to be affirmed to offer. The
              store sum will rely upon the hold for the property – for the most
              part the higher the save cost of a benefit, the bigger the
              offering store required.
            </p>
            <p>
              The security gave by the enrollment procedure is significant,
              giving an establishment of trust to the two purchasers and
              dealers. Property deals don't need to be mind boggling and
              confounding; our bidders realize that all gatherings have finished
              a similar enrollment process, pre-submitted a store and if
              effective, will be will undoubtedly finish the deal.
            </p>
          </div>
          <div className="content-buyers-guide-new mb-4">
            <h4>Bidding</h4>
            <p>
              Putting offers at our sales is simple. As properties can't be sold
              for not exactly the save value, we'll set a fitting beginning
              offer. This sum will be made open the day preceding the sale and
              it's the least offered that can be set on the property. For
              instance, you won't have the option to put an initial offer of
              €1,000 on a property saved at €4m.
            </p>
            <p>
              When offering opens, every enlisted bidder are allowed to put
              offers on their picked properties. In spite of the fact that
              offering on all properties will open simultaneously, each parcel
              will have an alternate shutting time - try to check the opening
              and shutting times on your picked properties so you're all set on
              the day.
            </p>
            <p>
              Offering happens on the applicable property page – a similar page
              you've been visiting to get data about the property and to enlist
              for offering.
            </p>
            <p>
              Our default offer sum is €1,000. This implies €1,000 is the least
              offered you can put, and that you'll just have the option to offer
              in products of €1,000 eg. €2,000, €3,000, €4,000 and so forth.
            </p>
            <p>
              The deal procedure is completely straightforward: all offers are
              logged and shown on the property page continuously, and we'll tell
              you on screen when you're the most elevated bidder so you'll know
              precisely what's going on consistently. There's no compelling
              reason to stress over security however – bidders are just
              recognized by a number.
            </p>
          </div>
          <div className="content-buyers-guide-new mb-4">
            <h4>Instant vs Max Bids</h4>
            <p>
              On the off chance that you would prefer not to put offers
              physically, you can set a Maximum Bid. For this situation, the PC
              will enter offers for you – however just to keep up your situation
              as the triumphant bidder and just up to the most extreme sum that
              you've indicated. Your Maximum Bid can't be seen freely, so just
              you will realize how high you're willing to go.
            </p>
            <p>
              Each parcel has a different shutting time which will be recorded
              on the property page. Try not to stress, it is anything but an
              instance of 'quickest finger first' as the clock runs down. In the
              event that an offer is put inside 60 seconds of the end time, an
              extra 60 seconds will be added to the clock. We call this an
              Extension Period, and it gives everybody a reasonable and
              equivalent opportunity to put an extra counter-offer on the off
              chance that they wish to do as such. This will keep on occurring
              until an entire 60 seconds goes without any offers being set.
            </p>
          </div>
          <div className="content-buyers-guide-new mb-4">
            <h4>Closing Time</h4>
            <p>
              When offering has shut, we promptly sign agreements for both
              purchaser and dealer. On the off chance that you've been effective
              – congrats! – you should mastermind installment of a 10% store
              inside ten working days. An extent of this 10% has just been given
              by the offering store you submitted during enrollment, so you just
              need to pay the parity. Try not to stress; we'll call you on the
              day to examine any questions.
            </p>
            <p>
              On the off chance that you've been out-offered – there's
              consistently next time!
            </p>
            <p>
              That is it. An advanced stage sponsored by advertise driving
              property skill, permitting you to offer from anyplace on the
              planet, on any gadget – safely, productively and certainly.
            </p>
          </div>
        </div>
      </section>
      {/* <section class="buyer_guide_pagination">
  <a href="#">
      <img src="images/expand_more-24px.svg" alt="">
  </a>
  <a href="#" class="active">
  01
    </a>
  <a href="#">
  02
    </a>
  <a href="#">
  03
    </a>
  <a href="#">
      <img src="images/expand_more-24px-1.svg" alt="">
  </a>
    </section> */}
    </main>

    //   <main>
    //     <section className="contact_form_section">
    //       <div className="container">
    //         <div className="contact-title">
    //           <h2>Buyer's Guide</h2>
    //         </div>
    //         <div className="buyers_guide_row_pg row">
    //           <div className="col-md-12 col-lg-6 col-sm-12">
    //             <a href="buyers-guide–detail-page.html">
    //               <div className="buyers_Guide-details">
    //                 <img src="images/pexels-photo-101808.jpg" alt="alt-img"  />
    //                 <div className="buyers_details_title_discription">
    //                   <div className="buyers_details_title">
    //                     <h6>Property Key Holding</h6>
    //                   </div>
    //                   <div className="buyers_details_desicription">
    //                     <span>23rd March 2020</span>
    //                     <p>
    //                       We can take responsibility for the property key and
    //                       allow access to authorized persons and guest. We will
    //                       hold a spare key in case of key loss. We can also
    //                       arrange that a locksmith changes the locks if and when
    //                       nece...
    //                     </p>
    //                   </div>
    //                 </div>
    //               </div>
    //             </a>
    //           </div>
    //           <div className="col-md-12 col-lg-6 col-sm-12">
    //             <a href="buyers-guide–detail-page.html">
    //               <div className="buyers_Guide-details">
    //                 <img src="images/green-trees-2360684.jpg" alt="alt-img"  />
    //                 <div className="buyers_details_title_discription">
    //                   <div className="buyers_details_title">
    //                     <h6>Property Gardnening and Landscaping</h6>
    //                   </div>
    //                   <div className="buyers_details_desicription">
    //                     <span>23rd March 2020</span>
    //                     <p>
    //                       We can take responsibility for the property key and
    //                       allow access to authorized persons and guest. We will
    //                       hold a spare key in case of key loss. We can also
    //                       arrange that a locksmith changes the locks if and when
    //                       nece...
    //                     </p>
    //                   </div>
    //                 </div>
    //               </div>
    //             </a>
    //           </div>
    //         </div>
    //         <div className="buyers_guide_row_pg row">
    //           <div className="col-md-12 col-lg-6 col-sm-12">
    //             <a href="buyers-guide–detail-page.html">
    //               <div className="buyers_Guide-details">
    //                 <img src="images/chairs-covered-in-snow-1866690.jpg" alt="alt-img"  />
    //                 <div className="buyers_details_title_discription">
    //                   <div className="buyers_details_title">
    //                     <h6>Property Gardnening and Landscaping</h6>
    //                   </div>
    //                   <div className="buyers_details_desicription">
    //                     <span>23rd March 2020</span>
    //                     <p>
    //                       We can take responsibility for the property key and
    //                       allow access to authorized persons and guest. We will
    //                       hold a spare key in case of key loss. We can also
    //                       arrange that a locksmith changes the locks if and when
    //                       nece...
    //                     </p>
    //                   </div>
    //                 </div>
    //               </div>
    //             </a>
    //           </div>
    //           <div className="col-md-12 col-lg-6 col-sm-12">
    //             <a href="buyers-guide–detail-page.html">
    //               <div className="buyers_Guide-details">
    //                 <img
    //                   src="images/blur-cartography-close-up-concept-408503.jpg"
    //                   alt
    //                 />
    //                 <div className="buyers_details_title_discription">
    //                   <div className="buyers_details_title">
    //                     <h6>Property Key Holding</h6>
    //                   </div>
    //                   <div className="buyers_details_desicription">
    //                     <span>23rd March 2020</span>
    //                     <p>
    //                       We can take responsibility for the property key and
    //                       allow access to authorized persons and guest. We will
    //                       hold a spare key in case of key loss. We can also
    //                       arrange that a locksmith changes the locks if and when
    //                       nece...
    //                     </p>
    //                   </div>
    //                 </div>
    //               </div>
    //             </a>
    //           </div>
    //         </div>
    //       </div>
    //     </section>
    //     <section className="buyer_guide_pagination">
    //       <a href="#">
    //         <img src="images/expand_more-24px.svg" alt="alt-img"  />
    //       </a>
    //       <a href="#" className="active">
    //         01
    //       </a>
    //       <a href="#">02</a>
    //       <a href="#">03</a>
    //       <a href="#">
    //         <img src="images/expand_more-24px-1.svg" alt="alt-img"  />
    //       </a>
    //     </section>
    //   </main>
    //
  );
}

export default BuyersGuide;
