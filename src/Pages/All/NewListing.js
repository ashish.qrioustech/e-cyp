import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import DropdownComponent from '../../Components/Dropdown/DropdownComponent';
import Axios from 'axios';
import { Fade } from 'react-bootstrap';
import { useHistory } from 'react-router';

import { getCurrentAuctionApiAction } from '../../actions/auctionAction';

import { getCurrentAuctionSelecter } from '../../selector/selector';
import { Link } from 'react-router-dom';
import SearchComponentWhite from '../../Components/SearchComponent/SearchComponentWhite';
import { getGlobalLangSelecter } from '../../selector/selector';

function NewListing({ callCurrentAuctionApi, currentAuctions,Language }) {
  const history = useHistory();
  const [gridView, setGridView] = useState(false);

  // const [Data, setData] = useState([]);
  const [dropData] = useState([
    { id: 1, value: 'Any' },
    { id: 2, value: 'Choose Area / Village' },
    { id: 3, value: 'Choose Sub-Type' },
    { id: 4, value: 'Choose Category' },
    { id: 5, value: 'Choose Neighbourhood' },
    { id: 6, value: 'Choose Remove listings' },
  ]);
  const [open, setOpen] = useState(false);
  const Req = {
    sh_limit: 10,
    page: 1,
    orderby: 1,

    category: '',
  };
  useEffect(() => {
    callCurrentAuctionApi(Req);
  }, []);

  const handleSearchProperty = () => {
    Axios.post(process.env.REACT_APP_API + 'mobilesearch', {
      ...Req,
    })
      .then((res) => {
        if (res.data.success === 'yes') {
          history.push({ pathname: '/search-result', data: res.data.results });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <main style={{ backgroundColor: '#f1f1f1' }}>
      <section
        className="second-page_bg second_page_1_bg"
        style={{ backgroundImage: 'url(images/new-listing.jpg)' }}
      >
        <h2>{Language.new_lis_cap}</h2>
      </section>
      {/* section search start */}

      <SearchComponentWhite />
      {/* section search end */}
      <section className="gird-properties">
        <div className="container text-center">
  <h4>{Language.new_listin_sm}</h4>
        </div>
        <div className="container">
          <div className="short-list">
            <a className="mt-3">
              <img
                style={{ cursor: 'pointer' }}
                src={gridView ? 'images/grid.svg' : 'images/grid-normal.svg'}
                alt="img"
                onClick={() => {
                  setGridView(true);
                }}
              />
            </a>
            <a className="mt-3">
              <img
                style={{ cursor: 'pointer' }}
                src={
                  gridView ? 'images/list-view.svg' : 'images/list-active.svg'
                }
                alt="img"
                onClick={() => {
                  setGridView(false);
                }}
              />
            </a>
            {/* <div className="dropdown">
              <button
                className="btn btn-short-dp dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Short by
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <a className="dropdown-item" href="#">
                  Popular
                </a>
                <a className="dropdown-item" href="#">
                  Low - High
                </a>
                <a className="dropdown-item" href="#">
                  High - Low
                </a>
              </div>
            </div>
          */}
          </div>

          <div className="grid_list_properties">
            <div className="row">
              {currentAuctions.length ? (
                currentAuctions.map((x) => {
                  return gridView ? (
                    <div className="col-md-3" key={x.id}>
                      <Link
                        to={'/propertyview/' + x.id}
                        className="card"
                        href="properties_item.html"
                      >
                        <div className="property_item_img">
                          <img
                            src={x.avatarorg}
                            alt={x.avatarorg}
                            className="img-fluid"
                            style={{ height: '250px' }}
                          />
                          <div className="property-rate_in_img">
                            <span>{x.starttext}</span>
                            <h6>€{x.pricestart}.00</h6>
                          </div>
                        </div>
                        <div className="card_details_properties">
                          <h4>{x.title}</h4>
                          <p>{x.description}</p>
                          <span>{x.auctiontype}</span>
                          <h5>{Language.off_price} : €{x.pricestart}</h5>
                        </div>
                      </Link>
                    </div>
                  ) : (
                    <div
                      className="list_view_property_deti_info w-100"
                      key={x.id}
                    >
                      <div
                        className="list_view_property_img"
                        style={{
                          backgroundImage: `url(${x.avatarorg})`,
                          backgroundSize: 'cover',
                        }}
                      >
                        <div className="favorite_heart_icon_list">
                          <img
                            src="images/heart-uncheck.svg"
                            alt="hart"
                            // onclick="this.src='images/active-hart.svg'"
                          />
                        </div>
                      </div>
                      <div className="list_view_property_full_info">
                        <div className="list_view_property-title_info">
                          <div className="list_view_property-title">
                            <h4>{x.title}</h4>
                            <span>
                              {Language.st_mo}, 02/02/2020 at 01:00:00 pm
                            </span>
                          </div>
                          <div className="list_view_property-info">
                            <div className="d-flex justify-content-between">
                              <div className="lot_no_list">
                                <span>{Language.lo_no}. :</span>
                                <strong>{x.content_head1}</strong>
                              </div>
                              <div className="lot_no_list">
                                <span>{Language.categ} :</span>
                      <strong>{Language.resid}</strong>
                              </div>
                              <div className="lot_no_list">
                                <span>{Language.sub_cate} :</span>
                                <strong>{x.auctiontype}</strong>
                              </div>
                            </div>
                            <div className="description_list_view">
                              <p>
                      <strong>{Language.deeees}</strong>
                              </p>
                              <p>{x.description}</p>
                            </div>
                          </div>
                        </div>
                        <div className="start_price_list_view">
                          <div className="start_price">
                            <span>{x.starttext} :</span>
                            <span>$ {x.pricestart}</span>
                          </div>
                          <button
                            className="view_details_btn"
                            // onclick="window.location.href='properties_item.html'"
                          >
                            {Language.vie_det}
                          </button>
                        </div>
                      </div>
                    </div>
                  );
                })
              ) : (
                <div className="col-12 text-center">
                  <h1>{Language.no_dat}</h1>
                </div>
              )}
            </div>
          </div>
        </div>
      </section>
      <section className="pagination">
        <div className="container">
          <div className="pagination_second-pg">
            <a href="#" className="active">
              01
            </a>
            {/* <a href="#">02</a>
            <a href="#">03</a>
            <a href="#">04</a> */}
            {/* <a href="#" className="multi-pagination">
              <img src="images/three-dot.svg" alt="img" />
            </a> */}
            {/* <a href="#">99</a> */}
          </div>
        </div>
      </section>
    </main>
  );
}

// NewListing.defaultProps = {
//   location: [],
// };
// NewListing.propTypes = {
//   location: PropTypes.object,
// };

NewListing.defaultProps = {
  currentAuctions: [],
};
NewListing.propTypes = {
  callCurrentAuctionApi: PropTypes.func.isRequired,
  currentAuctions: PropTypes.instanceOf(Array),
};
const mapStateToProps = (state) => ({
  currentAuctions: getCurrentAuctionSelecter(state),
  Language: getGlobalLangSelecter(state)
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      callCurrentAuctionApi: getCurrentAuctionApiAction,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(NewListing);
