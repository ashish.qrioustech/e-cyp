import React, { useEffect } from 'react';

export default function CitiesDetails(props) {
  useEffect(() => {
    window.scrollTo(0,0);
  }, []);
  return (
    <main>
      <section
        className="second-page_bg second_page_1_bg"
        style={{ backgroundImage: 'url(images/Paphos.jpg) !important' }}
      >
        <h2 style={{ textTransform: 'uppercase' }}>{props.match.params.id}</h2>
      </section>
      <section className="Why-cyprus-content mt-5">
        <div className="container" style={{ textAlign: 'justify' }}>
          <p>
            The antiquated capital of Cyprus lies in the southwest bank of
            Cyprus, around 50 km west from Limassol with populace around 50.000.
            With its wonderful fishing marina, harbor and medieval fortress
            joins the authentic destinations, beautiful open country and
            cosmopolitan occasion resort. In view of city's chronicled esteem
            UNESCO added the entire city to World Cultural Heritage List. Paphos
            International Airport is second biggest air terminal in Cyprus and
            keeps up around 2 mln clients a year.
          </p>
          <p>
            Paphos city is the capital of Paphos region and the fourth biggest
            town of Cyprus. The upper piece of town has every single
            administrative help, schools, medical clinics, and huge malls.
          </p>
          <p>
            Kato Pafos, the lower some portion of Pafos is the primary
            vacationer region. It centers around the beautiful fishing harbor
            and medieval post. Eateries, fish tavernas, cafeterias, keepsake
            shops, wonderful inns with significant archeological locales around
            them make this piece of Pafos one of the most smoking vacationer
            goals in Cyprus, however in Mediterranean too.
          </p>
          <p>
            In Paphos we can locate a wide determination of convenience from
            lavish inns with all courtesies to low spending lofts and appreciate
            amazing nearby and universal food in numerous eateries.
          </p>
          <p>
            The most significant spot to see in Paphos and entire Cyprus also is
            Byzantine Mosaics dated from second through 5thcentury which are
            situated close to the harbor. These complicated floor mosaics in
            Roman estates portraying for the most part scenes from Greek
            folklore are still in awesome condition and should be seen. Close to
            the mosaics we can appreciate cut out of strong stone underground
            Tombs of The Kings ornamented with Doric columns. Its offers
            numerous other chronicled places worth to find, for example, an
            amazing column to which St Paul was tied and whipped, Byzantine
            Museum and Archeological Museum where we can locate a wonderful
            assortment of Cypriot artifacts from Paphos territory and Folk
            Museum.
          </p>
          <p>
            Remember to visit one of the most significant locales in Cyprus –
            The Rock's of Aphrodite, unbelievable spot where the goddess of
            adoration, Aphrodite rose from the waves which is only a couple of
            miles from Paphos city. There additionally the popular Aphrodite
            Hills Resort, multi grant winning retreat which Forbes Magazine
            authorized to the world's most alluring five hotels list. The Resort
            incorporates 5* de luxury Intercontinental Htl, occasions estates,
            lofts, apartments, wellness offices, rivalry tennis court and 18
            opening standard title fairway where you can play golf entire year.
            Around there we can locate another 2 fairways – Tsada and Secret
            Valley.
          </p>
          <p>
            Paphos has a Subtropical Mediterranean atmosphere with sweltering
            and dry summers and cool blustery winters. Summer season starts in
            May and completes in October or some of the time toward the
            beginning of November and climate during this period is radiant
            consistently and downpour is phenomenal.
          </p>
          <p />
        </div>
      </section>
    </main>
  );
}
