import React, { useState } from 'react';
import Axios from 'axios';
import Swal from 'sweetalert2';
import { getGlobalLangSelecter } from '../../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';





function ContactUs(props) {

 const [fullname,setfullname] = useState('');
 const [email,setemail] = useState('');
 const [phone,setphone] = useState('');
 const [contactmessage,setcontactmessage] = useState('');
 const [subject,setsubject] = useState('null');

 // Keys: name phone_number messages from_email subject
 
const token = localStorage.getItem('auctionToken');
const formData = new FormData();
formData.set('name',fullname);
formData.set('from_email',email);
formData.set('phone_number',phone);
formData.set('messages',contactmessage);
formData.set('subject',subject);




const submit = (e)=>{
  e.preventDefault();
 Axios.post('https://forwardapi.auctionsoftware.com/mobileapi/send_contactus',formData,{headers:{"Authorization":`Bearer ${token}`,"domain":"cyprus.auctionsoftware.com"}})
 .then((res)=>{
   if(res.data.success === true){
    Swal.fire({
      title: 'Data sent successfully',
      icon: 'success',
      timer:1500,
      showConfirmButton: false,
      position:'center'
    })
   }
 console.log(res);
 }).catch((error)=>{
 console.log(error);
 })
};

  return (
    <main>
      <section className="contact_form_section">
        <div className="container">
          <div className="contact-title">
  <h2>{props.Language.contact_us}</h2>
          </div>
          <div className="row contact_form_contact_info">
            <div className="col-md-6">
              <img
                src="images/contact-bg.jpg"
                alt="contact-main-banner"
                className="img-fluid"
              />
            </div>
            <div className="col-md-6">
              <form className="contact-form_form">
                <input type="text" placeholder="Name" value={fullname} name='fullname' onChange={(e)=> setfullname(e.target.value)}/>
                <div className="row">
                  <div className="col-lg-6">
                    <input type="text" placeholder="Email" value={email} name='email' onChange={(e)=> setemail(e.target.value)}/>
                  </div>
                  <div className="col-lg-6">
                    <input type="text" placeholder="Phone" value={phone} name='phone' onChange={(e)=>setphone(e.target.value)}/>
                  </div>
                </div>
                <textarea
                  name="contact_Messages"
                  id="contact_Messages"
                  rows={5}
                  placeholder="Meassage"
                  defaultValue={''}
                  value={contactmessage}
                  onChange={(e)=>setcontactmessage(e.target.value)}
                />
                <div className="text-center">
  <button onClick={submit}>{props.Language.subm}</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section className="contact_pg_address_sec">
        <div className="container">
          <div className="contact_map_row">
            <div className="contact_map">
              <iframe
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2487.1818785398755!2d-2.4971094846945503!3d51.43645362370972!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48718587fac6a643%3A0xb096081097723069!2sBlue%20Sky%20Property!5e0!3m2!1sen!2sin!4v1593515826409!5m2!1sen!2sin"
                width="100%"
                height={340}
                frameBorder={0}
                style={{ border: 0 }}
                allowFullScreen
                aria-hidden="false"
                tabIndex={0}
              />
            </div>
            <div className="contact_address">
              <h3>ecyprusauctions.com</h3>
              <p>
                106, Griva Digeni, Nea Polis,
                <br /> Madonna Building,
                <br /> Offices 1-7
                <br /> 3101 Limassol Cyprus
              </p>
              <div className="office-hours">
                <p>
                  {' '}
                  <strong>{props.Language.off_hrs} :</strong>
                </p>
                <p>Daily : 09:00-13:00 and 14:30-18:00</p>
                <p>Saturday : 09:00-13:00</p>
                <p>Sunday : Closed</p>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ContactUs);