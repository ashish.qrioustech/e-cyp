import React from 'react';
import { getGlobalLangSelecter } from '../../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

function AboutUs(props) {
  return (
    <main>
      <section className="about_main-banner">
  <h2>{props.Language.com_pro}</h2>
  <p>{props.Language.regis_caps}</p>
        <div className="about_banner-after">
          <img src="images/search-bottom.svg" alt style={{ width: '100%' }} />
        </div>
      </section>
      <section className="about_Details">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <p className="mb-3">
                {props.Language.	
e_cyp_big}
                {props.Language.	
association_s}
                {props.Language.and_in_ex}.
              </p>
              <p className="mb-3">
                {props.Language.e_office_adminis}
                {props.Language.and_normal}
                {props.Language.reserve_th}.
              </p>
              <p className="mb-3">
                <b>{props.Language.home_agen}:</b> {props.Language.smart_est}
                {props.Language.master_be}
                {props.Language.rentals_n}.
                {props.Language.its_custome}
                {props.Language.landowners_e}
                {props.Language.pr}.
              </p>
            </div>
            <div className="col-md-6 trems-condition">
              <p className="mb-3">{props.Language.types_pr}:</p>
              <ul>
                <li>Residential properties - Houses, Apartments, Buildings</li>
                <li>Holiday Homes - Villas and Bungalows</li>
                <li>Shops and Offices</li>
                <li>Land and Plots</li>
                <li>Warehouses</li>
                <li>Projects for investors with rental income</li>
              </ul>
            </div>
            <div className="col-md-6 trems-condition">
              <p className="mb-3">e-Cyprusauctions also offers:</p>
              <ul>
                <li>
                  property and common expenses management services through
                  e-Cyprusauctions Smart Property Management.
                </li>
                <li>
                  property valuations through Polyviou &amp; Mouskides
                  Valuations.
                </li>
                <li>
                  a wide range of property investments for both local and
                  foreign investors.
                </li>
                <li>
                  investment products in real estate through Mouflon Real Estate
                  Fund Ltd, supervised by CySEC.
                </li>
                <li>assistance for buying property in the UK.</li>
              </ul>
            </div>
          </div>
        </div>
        {/* <div class="about_bottom_after">
  <img src="images/about_bottom_shap.svg" alt="about_bottom_shap" style="width: 100%;">
    </div> */}
      </section>
      <section className="about_Details">
        <div className="container">
          <h2 className>Corporate Responsibility</h2>
          <p>
            As individuals and as a company, we recognize that corporate
            responsibility is integral to being a successful business and an
            essential ingredient in maintaining a respected, trusted brand. This
            belief is central to our approach - doing good business depends on
            it.
          </p>
          <div className="corporate-list-resposibility">
            <h5>Customers</h5>
            <p>
              Giving the best expectations of customer care implies acting with
              polished skill and uprightness consistently. That is the means by
              which our administrations surpass customer desires in each area
              and area.
            </p>
          </div>
          <div className="corporate-list-resposibility">
            <h5>Individuals </h5>
            <p>
              We pull in, enlist and hold the best individuals in property.
              Directly across Cyprus, and in territories of property as
              different as property the executives and bequest office.
            </p>
          </div>
          <div className="corporate-list-resposibility">
            <h5>Condition</h5>
            <p>
              e-Cyprusauctions is focused on diminishing our corporate effect on
              the earth, and to raising the attention to our kin to help
              accomplish this.
            </p>
          </div>
          <div className="corporate-list-resposibility">
            <h5>Network</h5>
            <p>
              Our notoriety lays on our commitment to all the networks we work
              in. An inherent piece of this is supporting causes and other
              network related exercises in Cyprus.
            </p>
          </div>
          <div className="corporate-list-resposibility">
            <h5>Business Ethics</h5>
            <p>
              Our duty to acting genuinely and to the greatest advantage of our
              customers is crucial to all that we do, and a big motivator for
              FOX as a specialist co-op, manager and corporate resident.
            </p>
          </div>
        </div>
      </section>
      {/* <section class="about_video_bg">
  <div class="about_content_video_link">
      <h2>Let's know about us</h2>

      <img src="images/play.svg" alt="play" data-toggle="modal" data-target="#homeVideo" onclick="playVid()">

  </div>
  <video height="auto" controls="controls" preload="none" onclick="this.play()">
<source type="video/mp4" src="images/video.mp4">
     </video>
  <div class="about_bottom_after">
      <img src="images/about_bottom_shap.svg" alt="about_bottom_shap" style="width: 100%;">
  </div>
  <div class="about_bottom_before">
      <img src="images/search-bottom.svg" alt="about_bottom_shap" style="width: 100%;">
  </div>
    </section> */}
      {/* <section class="our_path_about_page">
  <div class="container">
      <h3>Our Path</h3>
      <div class="tab_how_it_work our_path_tab">
          <ul class="nav nav-tabs responsive-tabs" id="myTab" role="tablist">
              <li class="nav-item">
                  <a class="nav-link active" id="find-property-tab" data-toggle="tab" href="#find-property" role="tab" aria-controls="find-property" aria-selected="true">20XX</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="talk-tab" data-toggle="tab" href="#talk" role="tab" aria-controls="talk" aria-selected="false">20XX</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="get_details-tab" data-toggle="tab" href="#get_details" role="tab" aria-controls="get_details" aria-selected="false">20XX</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="view_property-tab" data-toggle="tab" href="#view_property" role="tab" aria-controls="view_property" aria-selected="false">20XX</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="reg_bid-tab" data-toggle="tab" href="#reg_bid" role="tab" aria-controls="reg_bid" aria-selected="false">20XX</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="sale_day-tab" data-toggle="tab" href="#sale_day" role="tab" aria-controls="sale_day" aria-selected="false">20XX</a>
              </li>
              <li class="nav-item">
                  <a class="nav-link" id="contaract_sign-tab" data-toggle="tab" href="#contract_sign" role="tab" aria-controls="contaract_sign" aria-selected="false">20XX</a>
              </li>
          </ul>
          <div class="tab-content" id="myTabContent">
              <div class="tab-pane fade show active" id="find-property" role="tabpanel" aria-labelledby="find-property-tab">
                  <div class="container tab_content_details">
                      <div class="row d-flex align-items-center">
                          <div class="col-md-7">
                              <p>
                                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                              </p>
                          </div>
                          <div class="col-md-5">
                              <img src="images/alexa-williams-RaYjMmmaSCA-unsplash.jpg" alt="" class="img-fluid">
                          </div>
                      </div>
                  </div>
              </div>
              <div class="tab-pane fade" id="talk" role="tabpanel" aria-labelledby="talk-tab">...</div>
              <div class="tab-pane fade" id="get_details" role="tabpanel" aria-labelledby="-tab">...</div>
              <div class="tab-pane fade" id="view_property" role="tabpanel" aria-labelledby="-tab">...</div>
              <div class="tab-pane fade" id="reg_bid" role="tabpanel" aria-labelledby="reg_bid-tab">...</div>
              <div class="tab-pane fade" id="sale_day" role="tabpanel" aria-labelledby="sale_day-tab">...</div>
              <div class="tab-pane fade" id="contract_sign" role="tabpanel" aria-labelledby="-tab">...</div>
          </div>
      </div>
  </div>
    </section> */}
    </main>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(AboutUs);