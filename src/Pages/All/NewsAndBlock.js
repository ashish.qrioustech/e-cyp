import React from 'react';

export default function NewsAndBlock() {
  return (
    <main>
      <section className="contact_form_section ">
        <div className="container">
          <div className="news-blog-main-title">
            <h2>NEWS &amp; BLOGS</h2>
          </div>
          <div className="blog_news_row">
            <div className="news_blog_with_sidebar ">
              <div className="side_bar_items_news">
                <div className="news_blog_sidebar">
                  <div className="title_sidebar">
                    <h4>Search News &amp; Blogs</h4>
                  </div>
                  <div className="sidebar_inner_body">
                    <div className="sidebar_blog_search">
                      <form action>
                        <input
                          type="text"
                          name="sideInputSearch"
                          id="sideInputSearch"
                          placeholder="Search"
                        />
                        <button>Search</button>
                      </form>
                    </div>
                  </div>
                </div>
                {/* Headline */}
                <div className="news_blog_sidebar">
                  <div className="title_sidebar">
                    <h4>Headlines</h4>
                  </div>
                  <div className="sidebar_inner_body">
                    <div className="Side_bar_Headline_link">
                      <p>
                        {' '}
                        <a href="#">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do
                        </a>
                      </p>
                      <p>
                        {' '}
                        <a href="#">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do
                        </a>
                      </p>
                      <p>
                        {' '}
                        <a href="#">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do
                        </a>
                      </p>
                      <p>
                        {' '}
                        <a href="#">
                          Lorem ipsum dolor sit amet, consectetur adipiscing
                          elit, sed do
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
                {/* News by Region */}
                <div className="news_blog_sidebar">
                  <div className="title_sidebar">
                    <h4>News by Region</h4>
                  </div>
                  <div className="sidebar_inner_body">
                    <div className="Side_bar_Headline_link news_by_region">
                      <p>
                        {' '}
                        <a href="#">All</a>
                      </p>
                      <p>
                        {' '}
                        <a href="#">Phaphos</a>
                      </p>
                      <p>
                        {' '}
                        <a href="#">limassol</a>
                      </p>
                      <p>
                        {' '}
                        <a href="#">Nicosa</a>
                      </p>
                      <p>
                        {' '}
                        <a href="#">City-X</a>
                      </p>
                      <p>
                        {' '}
                        <a href="#">City-Y</a>
                      </p>
                    </div>
                  </div>
                </div>
                {/* calander */}
                <div className="news_blog_sidebar">
                  <div className="title_sidebar">
                    <h4>Calander</h4>
                  </div>
                  <div className="sidebar_inner_body table-calander">
                    <table className="table-calander">
                      <thead>
                        <tr>
                          <th colSpan={7}>
                            <span className="btn-group">
                              <a className="btn">
                                <img src="images/left-arrow.svg" alt="alt-img"  />
                              </a>
                              <a className="btn active text-center">May 2020</a>
                              <a className="btn">
                                <img src="images/right-arrow.svg" alt="alt-img"  />
                              </a>
                            </span>
                          </th>
                        </tr>
                        <tr className="day_section_table">
                          <th>Su</th>
                          <th>Mo</th>
                          <th>Tu</th>
                          <th>We</th>
                          <th>Th</th>
                          <th>Fr</th>
                          <th>Sa</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td className="muted">29</td>
                          <td className="muted">30</td>
                          <td className="muted">31</td>
                          <td>1</td>
                          <td>2</td>
                          <td>3</td>
                          <td>4</td>
                        </tr>
                        <tr>
                          <td>5</td>
                          <td>6</td>
                          <td>7</td>
                          <td>8</td>
                          <td>9</td>
                          <td>10</td>
                          <td>11</td>
                        </tr>
                        <tr>
                          <td>12</td>
                          <td>13</td>
                          <td>14</td>
                          <td>15</td>
                          <td>16</td>
                          <td>17</td>
                          <td>18</td>
                        </tr>
                        <tr>
                          <td>19</td>
                          <td className="btn-primary">20</td>
                          <td>21</td>
                          <td>22</td>
                          <td>23</td>
                          <td>24</td>
                          <td>25</td>
                        </tr>
                        <tr>
                          <td>26</td>
                          <td>27</td>
                          <td>28</td>
                          <td>29</td>
                          <td className="muted">1</td>
                          <td className="muted">2</td>
                          <td className="muted">3</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                {/* calander */}
                {/* blog & news by */}
                <div className="news_blog_sidebar">
                  <div className="title_sidebar">
                    <h4>Blog &amp; News by</h4>
                  </div>
                  <div className="sidebar_inner_body">
                    <div className="news_blog_by_owner">
                      <div className="blog-owner-img">
                        <img
                          src="images/selective-focus-photograph-of-man-wearing-gray-suit-jacket-1138903.png"
                          alt
                        />
                      </div>
                      <div className="bloger_name_img">
                        <h6>Nathan James</h6>
                        <p>agent@cyprus.com</p>
                        <p>+123 456 78</p>
                      </div>
                    </div>
                    <div className="news_blog_by_owner">
                      <div className="blog-owner-img">
                        <img
                          src="images/selective-focus-photograph-of-man-wearing-gray-suit-jacket-1138903.png"
                          alt
                        />
                      </div>
                      <div className="bloger_name_img">
                        <h6>Nathan James</h6>
                        <p>agent@cyprus.com</p>
                        <p>+123 456 78</p>
                      </div>
                    </div>
                    <div className="news_blog_by_owner">
                      <div className="blog-owner-img">
                        <img
                          src="images/selective-focus-photograph-of-man-wearing-gray-suit-jacket-1138903.png"
                          alt
                        />
                      </div>
                      <div className="bloger_name_img">
                        <h6>Nathan James</h6>
                        <p>agent@cyprus.com</p>
                        <p>+123 456 78</p>
                      </div>
                    </div>
                    <div className="news_blog_by_owner">
                      <div className="blog-owner-img">
                        <img
                          src="images/selective-focus-photograph-of-man-wearing-gray-suit-jacket-1138903.png"
                          alt
                        />
                      </div>
                      <div className="bloger_name_img">
                        <h6>Nathan James</h6>
                        <p>agent@cyprus.com</p>
                        <p>+123 456 78</p>
                      </div>
                    </div>
                  </div>
                </div>
                {/* topic */}
                <div className="news_blog_sidebar">
                  <div className="title_sidebar">
                    <h4>Topics</h4>
                  </div>
                  <div className="sidebar_inner_body">
                    <div className="topics-links">
                      <p>
                        <a href="#">Commercials</a>
                      </p>
                      <p>
                        <a href="#">Residential</a>
                      </p>
                      <p>
                        <a href="#">Land</a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="Main_content_blog">
              <div className="main-blog-info">
                <img src="images/1 (4).jpg" alt={1} className="img-fluid" />
                <div className="blog_details_info_blog_pg">
                  <h4>FOX One Stop Solution</h4>
                  <p>
                    Home Buyers Expo by FOX 1ST HOME BUYERS EXPO BY FOX The 1st
                    Expo for properties Buying and selling was took place on
                    February 23rd, 2019 in Nicosia, at the Mall of Cyprus,
                    organized by FOX Smart Estate Agency with the participation
                    of Hellenic Bank. In the current property market state,
                    investing in a house for your needs or for renting out, has
                    become extremely advantageous. On this basis, we…
                  </p>
                  <div className="bloger_name-date-btn">
                    <div className="bloger_name-date">
                      <div className="bloger_img">
                        <img src="images/bloger-details-bloger-img.jpg" alt="alt-img"  />
                      </div>
                      <div className="bloger-name">
                        {/* <p>Sam Michale</p> */}
                        <span>28th July 2020</span>
                      </div>
                    </div>
                    <div className="read_more_blog_btn">
                      <button>Read more</button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="main-blog-info">
                <img src="images/1 (4).jpg" alt={1} className="img-fluid" />
                <div className="blog_details_info_blog_pg">
                  <h4>Μπορούμε να βρούμε ότι και αν ψάχνετε</h4>
                  <p>
                    {' '}
                    Η FOX είναι ένα από τα μεγαλύτερα Κτηματομεσιτικά Γραφεία
                    στην Κύπρο με 7 καταστήματα…
                  </p>
                  <div className="bloger_name-date-btn">
                    <div className="bloger_name-date">
                      <div className="bloger_img">
                        <img src="images/bloger-details-bloger-img.jpg" alt="alt-img"  />
                      </div>
                      <div className="bloger-name">
                        {/* <p>Sam Michale</p> */}
                        <span>28th July 2020</span>
                      </div>
                    </div>
                    <div className="read_more_blog_btn">
                      <button>Read more</button>
                    </div>
                  </div>
                </div>
              </div>
              <div className="main-blog-info">
                <img src="images/1 (4).jpg" alt={1} className="img-fluid" />
                <div className="blog_details_info_blog_pg">
                  <h4>RENTS, CORONA VIRUS AND GOVERNMENT DECISIONS</h4>
                  <p>
                    {' '}
                    Following many discussions, the House of Representatives,
                    recently passed with many amendments, a government bill
                    regarding rents. It is expected that tenants of residential
                    as well as commercial properties are now facing financial
                    problems caused by the pandemic, and this may continue in
                    the future. Part of these tenants have always been
                    consistent with their rent payments but the lockdown…
                  </p>
                  <div className="bloger_name-date-btn">
                    <div className="bloger_name-date">
                      <div className="bloger_img">
                        <img src="images/bloger-details-bloger-img.jpg" alt="alt-img"  />
                      </div>
                      <div className="bloger-name">
                        {/* <p>Sam Michale</p> */}
                        <span>15th June 2020</span>
                      </div>
                    </div>
                    <div className="read_more_blog_btn">
                      <button>Read more</button>
                    </div>
                  </div>
                </div>
              </div>
              <section className="buyer_guide_pagination">
                <a href="#">
                  <img src="images/expand_more-24px.svg" alt="alt-img"  />
                </a>
                <a href="#" className="active">
                  01
                </a>
                <a href="#">02</a>
                <a href="#">03</a>
                <a href="#">
                  <img src="images/expand_more-24px-1.svg" alt="alt-img"  />
                </a>
              </section>
            </div>
          </div>
        </div>
      </section>
      {/* <section class="news_blog_second_section">
      <div class="container">
          <div class="row">
              <div class="col-md-6 col-lg-6">
                  <a href="join-our-team.html" class="feature_link">
                      <div class="feature_second_pg_details">
                          <div class="feature_second_pg_img">
                              <img src="images/workplace-1245776_1920.jpg" alt="" class="img-fluid">
                          </div>
                          <div class="feature_second_pg_title">
                              <h3>Join our Team</h3>
                              <img src="images/arrow-secod-pg.svg" alt="">
                          </div>
                      </div>
                  </a>
              </div>
              <div class="col-md-6 col-lg-6">
                  <a href="#" class="feature_link">
                      <div class="feature_second_pg_details">
                          <div class="feature_second_pg_img">
                              <img src="images/643-cyprus-citizenship-and-passport-program_orig.jpg" alt="" class="img-fluid">
                          </div>
                          <div class="feature_second_pg_title">
                              <h3>Cyprus passport <br/> program
                              </h3>
                              <img src="images/arrow-secod-pg.svg" alt="">
                          </div>
                      </div>
                  </a>
              </div>
              <div class="col-md-6 col-lg-6">
                  <a href="#" class="feature_link">
                      <div class="feature_second_pg_details">
                          <div class="feature_second_pg_img">
                              <img src="images/old-house-4751245_1920.jpg" alt="" class="img-fluid">
                          </div>
                          <div class="feature_second_pg_title">
                              <h3>Headline / Title</h3>
                              <img src="images/arrow-secod-pg.svg" alt="">
                          </div>
                      </div>
                  </a>
              </div>
              <div class="col-md-6 col-lg-6">
                  <a href="#" class="feature_link">
                      <div class="feature_second_pg_details">
                          <div class="feature_second_pg_img">
                              <img src="images/autumn-mood-3830714_1920.jpg" alt="" class="img-fluid">
                          </div>
                          <div class="feature_second_pg_title">
                              <h3>Headline / Title</h3>
                              <img src="images/arrow-secod-pg.svg" alt="">
                          </div>
                      </div>
                  </a>
              </div>
          </div>
      </div>
  </section> */}
    </main>
  );
}
