import React, { useEffect, useState } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

// import OwlCarousel from 'react-owl-carousel2';
// import 'react-owl-carousel2/style.css';
import OwlCarousel from 'react-owl-carousel2';
import Axios from 'axios';
// import { currentAuctions } from '../../util/demoData';
import { getCurrentAuctionApiAction } from '../../actions/auctionAction';

import {
  getCurrentAuctionSelecter,
  getUpcommingAuctionSelecter,
} from '../../selector/selector';
import { Link } from 'react-router-dom';

function Auctions({
  callCurrentAuctionApi,
  currentAuctions,
  upcommingAuction,
}) {
  const [featured, setfeatured] = useState([]);
  // const [current, setCurrent] = useState([]);
  // const [upcomming, setUpcommingAuction] = useState([]);

  const Req = {
    sh_limit: 10,
    page: 1,
    orderby: 1,
    category: '',
  };

  useEffect(() => {
    callCurrentAuctionApi(Req);
    Axios.post(process.env.REACT_APP_API + 'mobilesearch', {
      ...Req,
      contenthead2: 1,
    })
      .then((res) => {
        if (res.data.success === 'yes') {
          setfeatured(res.data.results);
        }
      })
      .catch((error) => {
        console.log(error);
      });

    // Axios.post(process.env.REACT_APP_API + 'mobilesearch', { ...Req })
    //   .then((res) => {
    //     if (res.data.success === 'yes') {
    //       setCurrent(res.data.results);

    //       Axios.post(process.env.REACT_APP_API + 'mobilesearch', {
    //         ...Req,
    //         storedate:
    //           res.data.upcomingauctionlist &&
    //           res.data.upcomingauctionlist.length
    //             ? res.data.upcomingauctionlist.map((x) => x.up_date)
    //             : [],
    //       })
    //         .then((res) => {
    //           if (res.data.success === 'yes') {
    //             setUpcommingAuction(res.data.results);
    //           }
    //         })
    //         .catch((error) => {
    //           console.log(error);
    //         });
    //     }
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   });
  }, []);

  const options = {
    autoplay: true,
    loop: true,
    margin: 30,
    navClass: ['d-none', 'd-none'],
    nav: false,
    navBtn: false,
    dots: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 4,
      },
    },
  };
  // const events = {
  //   onDragged: function (event) {},
  //   onChanged: function (event) {},
  // };
  return (
    <main>
      <section
        className="second-page_bg"
        style={{ backgroundImage: 'url(images/auction-banner.jpg)' }}
      >
        <h2>Auctions</h2>
      </section>

      <section className="aunctions_pg">
        <div className="container">
          <div className="properties_auction_item_slider" id="featured_auction">
            <h2>Featured Auction</h2>

            {featured.length ? (
              <OwlCarousel options={options}>
                {featured.map((acution) => {
                  return (
                    <div className="property_item" key={acution.id}>
                      <Link
                        to={'/propertyview/' + acution.id}
                        className="properties-items_link"
                      >
                        <div className="property_item_img">
                          <img
                            src={acution.avatarorg}
                            alt={acution.avatarorg}
                            className="img-fluid auction-img"
                          />
                          <div className="property-rate_in_img">
                            <span>{acution.starttext}</span>
                            <h6>€{acution.pricestart}.00</h6>
                          </div>
                        </div>
                        <div className="property_item_details">
                          <h3>{acution.title}</h3>
                          <span>{acution.manufacturer}</span>
                          <p>{acution.description}</p>
                          <div className="property_item_timing">
                            <img
                              src="../images/timer.svg"
                              alt="timer"
                              className
                            />
                            <span>2M:13D:5M:32S</span>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                })}
              </OwlCarousel>
            ) : (
              <div className="property_item text-center">
                <h1 className="text-danger">No Record found</h1>
              </div>
            )}
          </div>
          <div className="properties_auction_item_slider" id="Current_auction">
            <h2>Current Auction</h2>
            {currentAuctions.length ? (
              <OwlCarousel options={options}>
                {currentAuctions.map((acution) => {
                  return (
                    <div className="property_item" key={acution.id}>
                      <Link
                        to={'/propertyview/' + acution.id}
                        className="properties-items_link"
                      >
                        <div className="property_item_img">
                          <img
                            src={acution.avatarorg}
                            alt={acution.avatarorg}
                            className="img-fluid auction-img"
                          />
                          <div className="property-rate_in_img">
                            <span>{acution.starttext}</span>
                            <h6>€{acution.pricestart}.00</h6>
                          </div>
                        </div>
                        <div className="property_item_details">
                          <h3>{acution.title}</h3>
                          <span>{acution.manufacturer}</span>
                          <p>{acution.description}</p>
                          <div className="property_item_timing">
                            <img
                              src="../images/timer.svg"
                              alt="timer"
                              className
                            />
                            <span>2M:13D:5M:32S</span>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                })}
              </OwlCarousel>
            ) : (
              <div className="property_item text-center">
                <h1 className="text-danger">No Record found</h1>
              </div>
            )}
          </div>
          <div className="properties_auction_item_slider" id="New_auction">
            <h2>Next Auction</h2>
            {upcommingAuction.length ? (
              <OwlCarousel options={options}>
                {upcommingAuction.map((acution) => {
                  return (
                    <div className="property_item" key={acution.id}>
                      <Link
                        to={'/propertyview/' + acution.id}
                        className="properties-items_link"
                      >
                        <div className="property_item_img">
                          <img
                            src={acution.avatarorg}
                            alt={acution.avatarorg}
                            className="img-fluid auction-img"
                          />
                          <div className="property-rate_in_img">
                            <span>{acution.starttext}</span>
                            <h6>€{acution.pricestart}.00</h6>
                          </div>
                        </div>
                        <div className="property_item_details">
                          <h3>{acution.title}</h3>
                          <span>{acution.manufacturer}</span>
                          <p>{acution.description}</p>
                          <div className="property_item_timing">
                            <img
                              src="../images/timer.svg"
                              alt="timer"
                              className
                            />
                            <span>2M:13D:5M:32S</span>
                          </div>
                        </div>
                      </Link>
                    </div>
                  );
                })}
              </OwlCarousel>
            ) : (
              <div className="property_item text-center">
                <h1 className="text-danger">No Record found</h1>
              </div>
            )}
          </div>
        </div>
      </section>
    </main>
  );
}

Auctions.defaultProps = {
  currentAuctions: [],
  upcommingAuction: [],
};
Auctions.propTypes = {
  callCurrentAuctionApi: PropTypes.func.isRequired,
  currentAuctions: PropTypes.instanceOf(Array),
  upcommingAuction: PropTypes.instanceOf(Array),
};
const mapStateToProps = (state) => ({
  currentAuctions: getCurrentAuctionSelecter(state),
  upcommingAuction: getUpcommingAuctionSelecter(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      callCurrentAuctionApi: getCurrentAuctionApiAction,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(Auctions);
