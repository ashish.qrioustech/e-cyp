import React from 'react';

export default function PlacesMostVisit() {
  return (
    <main>
      <section className="contact_form_section">
        <div className="container">
          <div className="contact-title mb-4">
            <h2>Places Most Visit Cyprus</h2>
          </div>
          <div className="place_most_visit_list">
            <div className="list_view_property_deti_info">
              <div
                className="list_view_property_img"
                style={{
                  background: 'url(images/nature.jpg)',
                  backgroundSize: 'cover',
                }}
              ></div>
              <div className="list_view_property_full_info">
                <div
                  className="list_view_property-title_info"
                  style={{ width: '100%', padding: '20px 0' }}
                >
                  <div className="list_view_property-title">
                    <h4>NATURE</h4>
                  </div>
                  <div className="list_view_property-info place_visit_why_cyprus">
                    <div
                      className="description_list_view"
                      style={{ textAlign: 'justify' }}
                    >
                      <p>
                        Favored with the excellence of nature's best palette,
                        the landscape of Cyprus unfurls across sparkling coasts,
                        moving mountains, fragrant woodlands and rough
                        headlands. From the warm shores of the territory to the
                        pristine and cool desert spring of the Troodos mountain
                        run, nature sweethearts, craftsmen, picture takers and
                        pioneers will all savor the experience of meeting modest
                        animals, and finding uncommon plants that peep out in
                        the midst of cascades, inlets, forest, winding path and
                        confined sandy sea shores. As the island is on the
                        movement way between Europe, Asia and Africa, Cyprus is
                        a birdwatchers dream, with herds of flamingos
                        frequenting the salt lakes, and numerous other huge
                        species going through or settling. Furthermore,
                        somewhere down in the woodlands, the national creature -
                        the Mouflon - meanders uninhibitedly, with getting a
                        brief look at this hesitant, wild sheep a genuine reward
                        for local people and guests alike.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="place_most_visit_list">
            <div className="list_view_property_deti_info">
              <div
                className="list_view_property_img"
                style={{
                  background: 'url(images/sea-sun.jpg)',
                  backgroundSize: 'cover',
                }}
              ></div>
              <div className="list_view_property_full_info">
                <div
                  className="list_view_property-title_info"
                  style={{ width: '100%', padding: '20px 0' }}
                >
                  <div className="list_view_property-title">
                    <h4>SUN &amp; SEA</h4>
                  </div>
                  <div className="list_view_property-info place_visit_why_cyprus">
                    <div
                      className="description_list_view"
                      style={{ textAlign: 'justify' }}
                    >
                      <p>
                        {' '}
                        Scarcely any sentiments can contrast with that of
                        sinking your toes into warm sand… of the sun kissing
                        your skin, and your faculties taking in the new, pungent
                        breeze and the unlimited perspectives on sparkling blue
                        waters.{' '}
                      </p>
                      <p>
                        {' '}
                        This unspoiled scene is one that can be appreciated
                        generally of the year on the island of Cyprus, and one
                        that offers a horde of encounters, from the all out
                        unwinding of sunbathing with drink close by, to the rush
                        and challenge of attempting another water sport..
                      </p>
                      <p>
                        {' '}
                        From segregated narrows, to exuberant hotels and sandy
                        sea shores to rough bays, the island's sweeping
                        coastline is home to a wide range of style sea shores;
                        every remarkable in its appearance and the offices it
                        offers.{' '}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="place_most_visit_list">
            <div className="list_view_property_deti_info">
              <div
                className="list_view_property_img"
                style={{
                  background: 'url(images/sports.jpg) center',
                  backgroundSize: 'cover',
                }}
              ></div>
              <div className="list_view_property_full_info">
                <div
                  className="list_view_property-title_info"
                  style={{ width: '100%', padding: '20px 0' }}
                >
                  <div className="list_view_property-title">
                    <h4>SPORTS &amp; TRAINING</h4>
                  </div>
                  <div className="list_view_property-info place_visit_why_cyprus">
                    <div
                      className="description_list_view"
                      style={{ textAlign: 'justify' }}
                    >
                      <p>
                        Preparing hard doesn't mean preparing in cruel
                        conditions, and there is no preferable inspiration over
                        rehearsing and getting ready for the game you love on an
                        island where the charming atmosphere joins with
                        wonderful Mediterranean environmental factors - and top
                        notch enhancements - to motivate accomplishment for the
                        coming season. A large group of sportspersons and groups
                        from everywhere throughout the world, including European
                        football crews, Olympic hopefuls, prepared medallists
                        and perseverance competitors, all give themselves an
                        upper hand by selecting to prepare towards their
                        objectives in Cyprus, because of a triumphant equation
                        of services.One of the most alluring explanations behind
                        picking the island is in fact its nearly ensured day by
                        day daylight, gentle winter temperatures and
                        insignificant precipitation, and this is additionally
                        upgraded and supplemented by a large group of different
                        advantages.
                      </p>
                      <p />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="place_most_visit_list">
            <div className="list_view_property_deti_info">
              <div
                className="list_view_property_img"
                style={{
                  background: 'url(images/spa.jpg)',
                  backgroundSize: 'cover',
                }}
              ></div>
              <div className="list_view_property_full_info">
                <div
                  className="list_view_property-title_info"
                  style={{ width: '100%', padding: '20px 0' }}
                >
                  <div className="list_view_property-title">
                    <h4>HEALTH &amp; WELLNESS</h4>
                  </div>
                  <div className="list_view_property-info place_visit_why_cyprus">
                    <div
                      className="description_list_view"
                      style={{ textAlign: 'justify' }}
                    >
                      <p>
                        Joining the charm of a daylight island with top notch
                        human services offices and appealing charges, Cyprus is
                        quick turning into a supported choice for clinical the
                        travel industry. What's more, with the special reward of
                        recovering in loosening up environmental factors that
                        are helpful for recuperating, it is no big surprise that
                        an expanding number of patients are picking the island
                        for their medicines. Close by ordinary clinical
                        medicines - that happen in the solace of present day
                        emergency clinics and facilities - the island is
                        additionally home to extravagance spa resorts and rustic
                        retreats, where solace and nature amicably entwine with
                        wellbeing. Spoiling, comprehensive treatments,
                        magnificence medicines and an overall feeling of
                        prosperity are completely summoned, alongside the
                        sentiment of getting away and concentrating exclusively
                        on oneself… without the burdens or interruptions of
                        every day life.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="place_most_visit_list">
            <div className="list_view_property_deti_info">
              <div
                className="list_view_property_img"
                style={{
                  background: 'url(images/food.jpg)',
                  backgroundSize: 'cover',
                }}
              ></div>
              <div className="list_view_property_full_info">
                <div
                  className="list_view_property-title_info"
                  style={{ width: '100%', padding: '20px 0' }}
                >
                  <div className="list_view_property-title">
                    <h4>FOOD &amp; DRINK</h4>
                  </div>
                  <div className="list_view_property-info place_visit_why_cyprus">
                    <div
                      className="description_list_view"
                      style={{ textAlign: 'justify' }}
                    >
                      <p>
                        The custom of sharing great, new nearby cooking is a
                        significant piece of the island's way of life, and is
                        inherently connected with each get-together, from family
                        social events and exceptional events to strict
                        celebrations… each set apart with its own unmistakable
                        treats and plans. From healthy meat dishes and claim to
                        fame cheeses to exceptional treats of carob and grape,
                        the Cypriot food is a colorful mix of Greek and Middle
                        Eastern societies, sprinkled with leftovers of old
                        civilisations, for example, indigenous Roman root
                        vegetables or old Phoenician luxuries. What's more, its
                        a well known fact that the 'Mediterranean eating
                        routine' is viewed as of the most beneficial, on account
                        of its wealth of heart-sound olive oil, beats, lean
                        meat, neighborhood spices and newly developed foods
                        grown from the ground. Add to this the ideal atmosphere
                        - that gives the new produce its extraordinary flavor -
                        and a festival everywhere, complete with unique treats,
                        and you will locate a major gastronomic experience
                        anticipates on this little island!
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="place_most_visit_list">
            <div className="list_view_property_deti_info">
              <div
                className="list_view_property_img"
                style={{
                  background: 'url(images/wedding.jpg)',
                  backgroundSize: 'cover',
                }}
              ></div>
              <div className="list_view_property_full_info">
                <div
                  className="list_view_property-title_info"
                  style={{ width: '100%', padding: '20px 0' }}
                >
                  <div className="list_view_property-title">
                    <h4>WEDDINGS &amp; HONEYMOONS</h4>
                  </div>
                  <div className="list_view_property-info place_visit_why_cyprus">
                    <div
                      className="description_list_view"
                      style={{ textAlign: 'justify' }}
                    >
                      <p>
                        Cyprus is one of Europe's driving goals for weddings,
                        gifts and special first nights, and is popular for its
                        brilliant atmosphere, delightful view and bunch of
                        alternatives for scenes, services and festivities. From
                        sundrenched pledges and holy functions to love bird
                        experiences, Aphrodite's island is love embodied.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="place_most_visit_list">
            <div className="list_view_property_deti_info">
              <div
                className="list_view_property_img"
                style={{
                  background: 'url(images/CULTURE.jpg)',
                  backgroundSize: 'cover',
                }}
              ></div>
              <div className="list_view_property_full_info">
                <div
                  className="list_view_property-title_info"
                  style={{ width: '100%', padding: '20px 0' }}
                >
                  <div className="list_view_property-title">
                    <h4>CULTURE &amp; RELIGION</h4>
                  </div>
                  <div className="list_view_property-info place_visit_why_cyprus">
                    <div
                      className="description_list_view"
                      style={{ textAlign: 'justify' }}
                    >
                      <p>
                        Cyprus is a little island with a long history and a rich
                        culture that traverses 10.000 years, making it probably
                        the most seasoned civilisation in the Mediterranean - as
                        prove by the many captivating social sights, exhibition
                        halls, landmarks and displays. Arranged at the
                        intersection of three mainlands - Europe, Asia and
                        Africa - the island's extraordinary geographic position
                        has had a significant influence in its fierce past since
                        relic. Its Prehistoric Age occupants were joined 3,500
                        years prior by the Mycenaean Greeks, who presented and
                        built up their civilisation, hence for all time
                        ingraining the island's Greek roots. Numerous different
                        societies followed from that point, including
                        Phoenicians, Assyrians, Egyptians, Romans, Franks,
                        Venetians, Ottomans and British, who all abandoned
                        obvious leftovers of their entry, and have in this
                        manner made a mosaic of various societies and periods.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="place_most_visit_list">
            <div className="list_view_property_deti_info">
              <div
                className="list_view_property_img"
                style={{
                  background: 'url(images/western-1662449_640.jpg)',
                  backgroundSize: 'cover',
                }}
              ></div>
              <div className="list_view_property_full_info">
                <div
                  className="list_view_property-title_info"
                  style={{ width: '100%', padding: '20px 0' }}
                >
                  <div className="list_view_property-title">
                    <h4>THEMATIC ROUTES</h4>
                  </div>
                  <div className="list_view_property-info place_visit_why_cyprus">
                    <div
                      className="description_list_view"
                      style={{ textAlign: 'justify' }}
                    >
                      <p>
                        Blessed with the beauty of nature’s best palette, the
                        scenery of Cyprus unfolds across glittering coasts,
                        rolling mountains, fragrant forests and rugged
                        headlands.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}
