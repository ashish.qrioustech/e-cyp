import React from 'react'
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import Axios from 'axios';
import moment from "moment";
import { subscribeToTimer, bidUpdate } from "../../util/Services/index";
import { getTimeInterval} from "../../util/Common";

export default function PropertiesItems() {
   let {id} = useParams();
  const [propertyDetails, setPropertyDetails] = useState({});
  const [attachmentsList, setattachmentsList] = useState([]);
  const [serverTimeSocket, setServerTimeSocket] = useState("");
  const [bidHisstoryList, setBidHisstoryList] = useState([]);
  const [bidDetails, setBidDetails] = useState([]);
  const [amount, setAmount] = useState("");
  const [timer, setTimer] = useState({});
    
    useEffect(() => {
        let params = {
          product_id: id
        }
        let auctionToken= localStorage.getItem('auctionToken');
        let header = {};
        header["authorization"] = "Bearer " + auctionToken;
        Axios.post(process.env.REACT_APP_API + 'getprodetails', params, header)
          .then((res) => {
            if (res.data.success === 'yes') {
              setPropertyDetails(res.data.results);
              //console.log("res.data.attachments ---", res.data.attachments);
              setattachmentsList(res.data.attachments);
              setBidDetails(res.data.biddetails);
              var timer = 0;
              subscribeToTimer((err, timestamp) => {
                let { dTime: serverTime } = timestamp;
                serverTime = moment(serverTime).valueOf();
                setServerTimeSocket(serverTime);
                timer = getTimeInterval(
                  moment(res.data.results.date_closed).valueOf(),
                  serverTime
                )
                setTimer(timer);
                //getBidDetails(params);
              });
            }
          })
          .catch((error) => {
            console.log(error);
          });
          
      }, []);

      function getBidDetails(params) {
        Axios.post(process.env.REACT_APP_API + 'getbiddetails', params)
          .then((res) => {
            if (res.data.success === 'yes') {
              setBidHisstoryList(res.data.results);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }

      const handleClick = async (nextBid, incremente, id) => {
        let bid =nextBid
        
        if (
          !isNaN(amount) &&
          parseFloat(bid) <= parseFloat(amount)
        ) {
          let params = {
            id: id,
            bid_increment: incremente,
            wsprice: amount,
          };
          Axios.post(process.env.REACT_APP_API + 'mobileconfirmForward', params)
          .then((res) => {
            if (res.data.success === 'yes') {
              console.log("success bid");
              //setBidHisstoryList(res.data.results);
            }
          })
          .catch((error) => {
            console.log(error);
          });
        } else {
          alert(`Enter more than US $ ${bid}`);
        }
        setAmount("");
      };

      const handleOnChange = (e) => {
        setAmount(e.target.value);
      }

    return (
     <main>
  <section>
    <div className="container">
      <div className="single_properties_title">
        <h2>
          {propertyDetails.title}
        </h2>
      </div>
    </div>
   
    <div className="container properties_single_pg">
      {/*Carousel Wrapper*/}
      <div id="carousel-thumb" className="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
        {/*Slides*/}
        <div className="carousel-inner" role="listbox">
          <div className="carousel-item active">
            <img className="d-block w-100" src="../images/slider-1.jpg" alt="First slide" />
            <div className="carousel_heart">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
              {/* <img src="../images/heart-uncheck.svg" alt="" id="slider1" onclick="checked()"> */}
            </div>
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src="../images/slider-1-1-new.jpg" alt="Second slide" />
            <div className="carousel_heart">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
          {/* <div class="carousel-item">
          <img class="d-block w-100" src="../images/slider-1-2.jpg" alt="Third slide">
          <div class="carousel_heart">
            <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
          </div>
        </div> */}
        </div>
        
        {/*/.Controls*/}
        {attachmentsList.length > 0 
        ? 
        <ol className="carousel-indicators owl-carousel owl-theme thum-slider" id="thum_slider">
        {attachmentsList.map((item,index)=> {
          return(
            <li data-target="#carousel-thumb" data-slide-to={0}><img className="d-block w-100" src="../images/slider-1-5.jpg" /></li>
            )
          })}
       
        </ol>
        :
        ""
        }
        
      </div>
      {/*/.Carousel Wrapper*/}
    </div>
  </section>
  <section className>
    <div className="container">
      <div className="Properties_second_Section_page">
        <div className="Properties_Bid_details_One">
          <p>Lot No.: <strong>{propertyDetails.auctionid}</strong></p>
          <p>Category : <strong>Residential</strong></p>
          <p>Sub - Category : <strong>House</strong></p>
      <p>Biding Deposit : <strong>{propertyDetails.content_head4 > 0 ? '€' + propertyDetails.content_head4 : "-"}</strong></p>
        </div>
        <div className="bid_price_enter">
          <div className="start_price_bid_properties">
            <p>
              Start Price :
            </p>
            <div className>
              
              <input
               type="text"
               class="form-control" 
               placeholder={propertyDetails.sprice > 0 ? '€' + propertyDetails.sprice : "-"}
               pattern="^\d*(.\d{0,2})?$"
               onChange={handleOnChange}
                onKeyDown={(event) => {
                  if (event.key === "Enter") {
                    event.preventDefault();
                    handleClick(
                      (parseFloat(bidDetails.next_bid)),
                       bidDetails.incrementamt,
                       propertyDetails.id
                    );
                  }
                }
              } />
            </div>
            <div className="text-center">
              <button className="submit_offer_start_btn" data-toggle="modal" data-target="#myModal_no_page_load_first">
                Submit offer
              </button>
            </div>
          </div>
        </div>
        <div className="bid_Deposit_properties_single">
          <div className="start_price_bid_properties">
            <p>
              Bid Deposit : *Fully refundable for Unsuccessful bidder
            </p>
            <div className>
              <label htmlFor>
                €5,000
              </label>
            </div>
            <div className="text-center">
              <button className="submit_offer_start_btn" onclick="window.location.href='after-bid_properties.html'">
                Registration Opens on 02/04 01:00PM
              </button>
            </div>
          </div>
        </div>
        <div className="bid_timer_start_properties_single">
          <div className="bid_timer_start_properties_single_title">
            <img src="../images/clock_properties.svg" alt="clock_properties" />
            <span>BID STARTS IN</span>
          </div>
          <div className="bid_timer_timer" id="countdown">
            <div>

              <p id="day">
                {timer.days}
              </p>
              <span>
                DAYS
              </span>
            </div>
            <div>
              <p id="hour">
                {timer.hours}
              </p>
              <span>
                HOURS
              </span>
            </div>
            <div>
              <p id="min">
                {timer.minutes}
              </p>
              <span>
                MINUTES
              </span>
            </div>
            <div>
              <p id="sec">
                {timer.seconds}
              </p>
              <span>
                SECONDS
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="Start_lable_Property_page">
        <p>Starts at {moment(propertyDetails.date_closed).format("LLLL")}</p>
      </div>
    </div>
  </section>
  {bidHisstoryList!=undefined && bidHisstoryList.length > 0 ? 
      <section>
      <div className="container">
        <div className="table_heading">
            <div className="table_history">
              <table>
                <tbody>
                  <tr>
                    <td>
                      Username : test
                    </td>
                    <td>
                      Date & Time : 8/6/2020 4:07 am
                    </td>
                    <td>
                      <span>Amount : €800</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <label>Bid History</label>
        </div>
      </div>
      </section>
  : null}
  
  <section className="container">
    <div className="social_icon_share starting_social_icon">
      <span>Share :
      </span>
      <a href="#" style={{marginRight: 27}}>
        <img src="../images/linkedin-in-brands.png" alt="alt-img"  />
      </a>
      <a href="#" style={{marginRight: 27}}>
        <img src="../images/youtube_properties.svg" alt="alt-img"  />
      </a>
      <a href="#" style={{marginRight: 27}}>
        <img src="../images/facebook-f-brands.png" alt="alt-img"  />
      </a>
      <a href="#" style={{marginRight: 27}}>
        <img src="../images/instagram-sketched.png" alt="alt-img"  />
      </a>
      <a href="#">
        <img src="../images/twitter-brands.png" alt="alt-img"  />
      </a>
    </div>
  </section>
  <section className="four_big_btn_section">
    <div className="container">
      <div className="row">
        <div className="col-lg-3 col-md-3">
          <button onclick="window.location.href='Buyers-guide.html'" className="btn_properti_View">
            <img src="../images/guide.svg" alt="alt-img"  />
            <span>
              Buyer's Guide
            </span>
          </button>
        </div>
        <div className="col-lg-3 col-md-3">
          <button onclick="window.location.href='faq.html'" className="btn_properti_View mr_58">
            <img src="../images/Outline.svg" alt="alt-img"  />
            <span>
              FAQ
            </span>
          </button>
        </div>
        <div className="col-lg-3 col-md-3">
          <button data-target="#view_properties" data-toggle="modal" className="btn_properti_View">
            <img src="../images/real-estate.svg" alt="alt-img"  />
            <span>
              View property
            </span>
          </button>
        </div>
        <div className="col-lg-3 col-md-3">
          <button onclick=" window.open('https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf','_blank')" className="btn_properti_View">
            <img src="../images/print-properties_main.svg" alt="alt-img"  />
            <span>
              Print Brochure
            </span>
          </button>
        </div>
      </div>
      {/* <div class="four_big_btn_First_btn">
    
      </div> */}
    </div>
  </section>
  <section className="tab_view_properties_details">
    <div className="container">
      <div className="tab_Propertie_single">
        <div className="nav flex-column nav-pills tab_nav_properties" id="v-pills-tab" role="tablist" aria-orientation="vertical">
          <a className="nav-link active" id="v-pills-Description-tab" data-toggle="pill" href="#v-pills-Description" role="tab" aria-controls="v-pills-Description" aria-selected="true">Description</a>
          <a className="nav-link" id="v-pills-Brochure-tab" data-toggle="pill" href="#v-pills-Brochure" role="tab" aria-controls="v-pills-Brochure" aria-selected="false">Brochure</a>
          <a className="nav-link legal_nav_link" href="legal-document.html#legal-document_page">
            Legal Documents</a>
          <a className="nav-link" id="v-pills-Map-tab" data-toggle="pill" href="#v-pills-Map" role="tab" aria-controls="v-pills-Map" aria-selected="false">Map</a>
        </div>
        <div className="tab-content Tab_details_content" id="v-pills-tabContent">
          <div className="tab-pane fade show active tab_inner_Section_details_most" id="v-pills-Description" role="tabpanel" aria-labelledby="v-pills-Description-tab">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim diam quis enim lobortis scelerisque. Lobortis mattis aliquam faucibus purus in. Feugiat in fermentum posuere urna nec tincidunt praesent semper. Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Dis parturient montes nascetur ridiculus mus mauris. 
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim diam quis enim lobortis scelerisque. Lobortis mattis aliquam faucibus purus in. Feugiat in fermentum posuere urna nec tincidunt praesent semper. Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Dis parturient montes nascetur ridiculus mus mauris. 
            </p>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Dignissim diam quis enim lobortis scelerisque. Lobortis mattis aliquam faucibus purus in. Feugiat in fermentum posuere urna nec tincidunt praesent semper. Ut enim blandit volutpat maecenas volutpat blandit aliquam etiam erat. Dis parturient montes nascetur ridiculus mus mauris. 
            </p>
          </div>
          <div className="tab-pane fade tab_inner_Section_details_most" id="v-pills-Brochure" role="tabpanel" aria-labelledby="v-pills-Brochure-tab">
            <div className="tab_brouchure">
              <ul>
                <li>
                  <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank">
                    Brochure_one.pdf
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="tab-pane fade tab_inner_Section_details_most" id="v-pills-Legal" role="tabpanel" aria-labelledby="v-pills-Legal-tab">
            <div className="tab_brouchure">
              <ul>
                <li>
                  <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank">
                    Brochure_one.pdf
                  </a>
                </li>
                <li>
                  <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank">
                    Brochure_one.pdf
                  </a>
                </li>
                <li>
                  <a href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf" target="_blank">
                    Brochure_one.pdf
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="tab-pane fade tab_map" id="v-pills-Map" role="tabpanel" aria-labelledby="v-pills-Map-tab">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27050.039502475036!2d-1.5744919973476545!3d53.18362487776462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x25a3b1142c791a9%3A0xc4f8a0433288257a!2sUnited%20Kingdom!5e0!3m2!1sen!2sin!4v1593769316045!5m2!1sen!2sin" width="100%" height={450} frameBorder={0} style={{border: 0}} allowFullScreen aria-hidden="false" tabIndex={0} />
          </div>
        </div>
      </div>
    </div>
  </section>
  <section className="similar_properties_single">
    <div className="container">
      <h2>Similar Properties</h2>
    </div>
    <div className="container">
      <div className="row">
        <div className="col-md-3 col-lg-3">
          <div className="property_item">
            <a href="Properties_item.html">
              <div className="property_item_img">
                <img src="../images/1.jpg" alt={1} className="img-fluid" />
                <div className="property-rate_in_img">
                  <span>
                    Start Price
                  </span>
                  <h6>
                    €150,000.00
                  </h6>
                </div>
                {/* <div class="similar_heart_fav">
                <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
              </div> */}
              </div>
              <div className="property_item_details">
                <h3>
                  3 Bedroom Apartment
                </h3>
                <span>
                  Limassol
                </span>
                <p>RESIDENTIAL</p>
                <div className="property_item_timing">
                  <img src="../images/timer.svg" alt="timer" className />
                  <span>2M:13D:5M:32S</span>
                </div>
              </div>
            </a>
            <div className="similar_heart_fav">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
        </div>
        <div className="col-md-3 col-lg-3">
          <div className="property_item">
            <a href="Properties_item.html">
              <div className="property_item_img">
                <img src="../images/1.jpg" alt={1} className="img-fluid" />
                <div className="property-rate_in_img">
                  <span>
                    Start Price
                  </span>
                  <h6>
                    €150,000.00
                  </h6>
                </div>
                {/* <div class="similar_heart_fav">
                <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
              </div> */}
              </div>
              <div className="property_item_details">
                <h3>
                  3 Bedroom Apartment
                </h3>
                <span>
                  Limassol
                </span>
                <p>RESIDENTIAL</p>
                <div className="property_item_timing">
                  <img src="../images/timer.svg" alt="timer" className />
                  <span>2M:13D:5M:32S</span>
                </div>
              </div>
            </a>
            <div className="similar_heart_fav">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
        </div>
        <div className="col-md-3 col-lg-3">
          <div className="property_item">
            <a href="Properties_item.html">
              <div className="property_item_img">
                <img src="../images/1.jpg" alt={1} className="img-fluid" />
                <div className="property-rate_in_img">
                  <span>
                    Start Price
                  </span>
                  <h6>
                    €150,000.00
                  </h6>
                </div>
                {/* <div class="similar_heart_fav">
                <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
              </div> */}
              </div>
              <div className="property_item_details">
                <h3>
                  3 Bedroom Apartment
                </h3>
                <span>
                  Limassol
                </span>
                <p>RESIDENTIAL</p>
                <div className="property_item_timing">
                  <img src="../images/timer.svg" alt="timer" className />
                  <span>2M:13D:5M:32S</span>
                </div>
              </div>
            </a>
            <div className="similar_heart_fav">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
        </div>
        <div className="col-md-3 col-lg-3">
          <div className="property_item">
            <a href="Properties_item.html">
              <div className="property_item_img">
                <img src="../images/1.jpg" alt={1} className="img-fluid" />
                <div className="property-rate_in_img">
                  <span>
                    Start Price
                  </span>
                  <h6>
                    €150,000.00
                  </h6>
                </div>
                {/* <div class="similar_heart_fav">
                <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
              </div> */}
              </div>
              <div className="property_item_details">
                <h3>
                  3 Bedroom Apartment
                </h3>
                <span>
                  Limassol
                </span>
                <p>RESIDENTIAL</p>
                <div className="property_item_timing">
                  <img src="../images/timer.svg" alt="timer" className />
                  <span>2M:13D:5M:32S</span>
                </div>
              </div>
            </a>
            <div className="similar_heart_fav">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

    )
}
