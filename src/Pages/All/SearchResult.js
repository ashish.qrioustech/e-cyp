import React, { useState, useEffect } from 'react';
import DropdownComponent from '../../Components/Dropdown/DropdownComponent';
import Axios from 'axios';
import { Fade } from 'react-bootstrap';
import PropTypes from 'prop-types';
import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';

export default function SearchResult({ location }) {
  const history = useHistory();
  const [gridView, setGridView] = useState(false);

  const [Data, setData] = useState([]);
  const [dropData] = useState([
    { id: 1, value: 'Any' },
    { id: 2, value: 'Choose Area / Village' },
    { id: 3, value: 'Choose Sub-Type' },
    { id: 4, value: 'Choose Category' },
    { id: 5, value: 'Choose Neighbourhood' },
    { id: 6, value: 'Choose Remove listings' },
  ]);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setData(location.data && location.data.length ? location.data : []);
  }, [location]);
  const Req = {
    // category: ['residential'],
    fromprice: '0',
    toprice: '50000000',
    // city: ['losangeles'],
  };
  const handleSearchProperty = () => {
    Axios.post(process.env.REACT_APP_API + 'mobilesearch', {
      ...Req,
    })
      .then((res) => {
        if (res.data.success === 'yes') {
          history.push({ pathname: '/search-result', data: res.data.results });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <main style={{ backgroundColor: '#f1f1f1' }}>
      <section className="second-page_bg second_page_1_bg">
        <h2 style={{ textTransform: 'uppercase' }}>Result</h2>
      </section>
      {/* section search start */}

      <section
        className="Second-Page_Discover"
        style={{ paddingBottom: '50px' }}
      >
        <div className="container search_container second_page_discover_info pb-5">
          <h3>Discover your perfect property in Cyprus</h3>
          <div className="search_dp_section">
            <div className="row">
              <div className="col-md-3">
                <DropdownComponent
                  label={'DISTRICT'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Any"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'AREA / VILLAGE'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Area / Village"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'TYPE'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Any"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'SUB TYPE'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Sub-Type"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'CATEGORY'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Category"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <label>PRICE</label>
                <div className="slider-range_Price_section">
                  <span>Price €0 - €10,000,000+</span>
                </div>
                <div className="slider-range__Price">
                  <div id="slider-range" />
                </div>
              </div>
              <div className="col-md-3">
                <label>LOT NUMBER</label>
                <input
                  type="text"
                  name="LOTNo"
                  id="LOTNo"
                  style={{ width: '100%' }}
                  placeholder="Enter Lot Number"
                />
              </div>
              <div className="col-md-3">
                <label>&nbsp;</label>
                <br />
                <button className="searc_sc_btn" onClick={handleSearchProperty}>
                  <img src="images/search.svg" alt="img" />
                  Search
                </button>
              </div>
            </div>
          </div>
          <div className="rest_show_search_section">
            <a href="/reset">Reset</a>

            <a id="advance-search" onClick={() => setOpen(!open)}>
              Advance Search
            </a>
          </div>
          <Fade
            in={open}
            className='search_dp_section'
            style={{ maxHeight: `${open ? '1000px' : '0px'}` }}
          >
            <div id="example-fade-text">
              <div className="row">
                <div className="col-md-3">
                  <DropdownComponent
                    label={'NEIGHBOURHOOD'}
                    value={''}
                    drop={'down'}
                    filterDropdown={false}
                    className={'dropdown'}
                    inputClassName="btn-Search_drop_D"
                    labelClassName={''}
                    noOptionsMessage={''}
                    error={''}
                    defaultDropName="Choose Neighbourhood"
                    isDisabled={false}
                    data={dropData}
                  />
                </div>
                <div className="col-md-3">
                  <label>LAND</label>
                  <div className="slider-range_Price_section">
                    <span>
                      m<small>2</small> 0 - 59000
                    </span>
                  </div>
                  <div className="slider-range__Price">
                    <div id="land" />
                  </div>
                </div>
                <div className="col-md-3">
                  <DropdownComponent
                    label={'BEDROOMS'}
                    value={''}
                    drop={'down'}
                    filterDropdown={false}
                    className={'dropdown'}
                    inputClassName="btn-Search_drop_D"
                    labelClassName={''}
                    noOptionsMessage={''}
                    error={''}
                    defaultDropName="Any"
                    isDisabled={false}
                    data={dropData}
                  />
                </div>
                <div className="col-md-3">
                  <DropdownComponent
                    label={'NEW / USED'}
                    value={''}
                    drop={'down'}
                    filterDropdown={false}
                    className={'dropdown'}
                    inputClassName="btn-Search_drop_D"
                    labelClassName={''}
                    noOptionsMessage={''}
                    error={''}
                    defaultDropName="Choose Sub-Type"
                    isDisabled={false}
                    data={dropData}
                  />
                </div>
                <div className="col-md-3">
                  <DropdownComponent
                    label={'REMOVE LISTINGS'}
                    value={''}
                    drop={'down'}
                    filterDropdown={false}
                    className={'dropdown'}
                    inputClassName="btn-Search_drop_D"
                    labelClassName={''}
                    noOptionsMessage={''}
                    error={''}
                    defaultDropName="Choose Remove listings"
                    isDisabled={false}
                    data={dropData}
                  />
                </div>
                <div className="col-md-3">
                  <label>COVERED AREA</label>
                  <div className="slider-range_Price_section">
                    <span>
                      m<small>2</small> 0 - 59000
                    </span>
                  </div>
                  <div className="slider-range__Price">
                    <div id="coverd-area" />
                  </div>
                </div>
                <div className="col-md-3">
                  <DropdownComponent
                    label={'BEDROOMS'}
                    value={''}
                    drop={'down'}
                    filterDropdown={false}
                    className={'dropdown'}
                    inputClassName="btn-Search_drop_D"
                    labelClassName={''}
                    noOptionsMessage={''}
                    error={''}
                    defaultDropName="Any"
                    isDisabled={false}
                    data={dropData}
                  />
                </div>
                <div className="col-md-3">
                  <DropdownComponent
                    label={'NEW / USED'}
                    value={''}
                    drop={'down'}
                    filterDropdown={false}
                    className={'dropdown'}
                    inputClassName="btn-Search_drop_D"
                    labelClassName={''}
                    noOptionsMessage={''}
                    error={''}
                    defaultDropName="Choose Sub-Type"
                    isDisabled={false}
                    data={dropData}
                  />
                </div>
              </div>
            </div>
          </Fade>
        </div>
      </section>

      {/* section search end */}
      <section className="gird-properties">
        <div className="container text-center">
          <h4>Cyprus Residency</h4>
        </div>
        <div className="container">
          <div className="short-list">
            <a className="mt-3">
              <img
                style={{ cursor: 'pointer' }}
                src={gridView ? 'images/grid.svg' : 'images/grid-normal.svg'}
                alt="img"
                onClick={() => {
                  setGridView(true);
                }}
              />
            </a>
            <a className="mt-3">
              <img
                style={{ cursor: 'pointer' }}
                src={
                  gridView ? 'images/list-view.svg' : 'images/list-active.svg'
                }
                alt="img"
                onClick={() => {
                  setGridView(false);
                }}
              />
            </a>
            {/* <div className="dropdown">
              <button
                className="btn btn-short-dp dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Short by
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <a className="dropdown-item" href="#">
                  Popular
                </a>
                <a className="dropdown-item" href="#">
                  Low - High
                </a>
                <a className="dropdown-item" href="#">
                  High - Low
                </a>
              </div>
            </div>
          */}
          </div>

          <div className="grid_list_properties">
            <div className="row">
              {Data.length ? (
                Data.map((x) => {
                  return gridView ? (
                    <div className="col-md-3" key={x.id}>
                      <Link to={"/propertyview/"+x.id} className="card" href="properties_item.html">
                        <div className="property_item_img">
                          <img
                            src={x.avatarorg}
                            alt={x.avatarorg}
                            className="img-fluid"
                          />
                          <div className="property-rate_in_img">
                            <span>{x.starttext}</span>
                            <h6>€{x.pricestart}.00</h6>
                          </div>
                        </div>
                        <div className="card_details_properties">
                          <h4>{x.title}</h4>
                          <p>{x.description}</p>
                          <span>{x.auctiontype}</span>
                          <h5>Offer Price : €{x.pricestart}</h5>
                        </div>
                      </Link>
                    </div>
                  ) : (
                    <div
                      className="list_view_property_deti_info w-100"
                      key={x.id}
                    >
                      <div
                        className="list_view_property_img"
                        style={{
                          backgroundImage: `url(${x.avatarorg})`,
                          backgroundSize: 'cover',
                        }}
                      >
                        <div className="favorite_heart_icon_list">
                          <img
                            src="images/heart-uncheck.svg"
                            alt="hart"
                            // onclick="this.src='images/active-hart.svg'"
                          />
                        </div>
                      </div>
                      <div className="list_view_property_full_info">
                        <div className="list_view_property-title_info">
                          <div className="list_view_property-title">
                            <h4>{x.title}</h4>
                            <span>
                              Starts Monday, 02/02/2020 at 01:00:00 pm
                            </span>
                          </div>
                          <div className="list_view_property-info">
                            <div className="d-flex justify-content-between">
                              <div className="lot_no_list">
                                <span>Lot No. :</span>
                                <strong>{x.content_head1}</strong>
                              </div>
                              <div className="lot_no_list">
                                <span>Category :</span>
                                <strong>Residential</strong>
                              </div>
                              <div className="lot_no_list">
                                <span>Sub-category :</span>
                                <strong>{x.auctiontype}</strong>
                              </div>
                            </div>
                            <div className="description_list_view">
                              <p>
                                <strong>Description</strong>
                              </p>
                              <p>{x.description}</p>
                            </div>
                          </div>
                        </div>
                        <div className="start_price_list_view">
                          <div className="start_price">
                            <span>{x.starttext} :</span>
                            <span>$ {x.pricestart}</span>
                          </div>
                          <button
                            className="view_details_btn"
                            // onclick="window.location.href='properties_item.html'"
                          >
                            View Details
                          </button>
                        </div>
                      </div>
                    </div>
                  );
                })
              ) : (
                <div className="col-12 text-center">
                  <h1>No Data Found</h1>
                </div>
              )}
            </div>
          </div>
        </div>
      </section>
      <section className="pagination">
        <div className="container">
          <div className="pagination_second-pg">
            <a href="#" className="active">
              01
            </a>
            {/* <a href="#">02</a>
            <a href="#">03</a>
            <a href="#">04</a> */}
            {/* <a href="#" className="multi-pagination">
              <img src="images/three-dot.svg" alt="img" />
            </a> */}
            {/* <a href="#">99</a> */}
          </div>
        </div>
      </section>
    </main>
  );
}

SearchResult.defaultProps = {
  location: [],
};
SearchResult.propTypes = {
  location: PropTypes.object,
};
