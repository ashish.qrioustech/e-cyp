import React from 'react';
import SearchComponentWhite from '../../Components/SearchComponent/SearchComponentWhite';
import { Link, useHistory } from 'react-router-dom';
import OwlCarousel from 'react-owl-carousel2';

export default function City() {
  const history = useHistory();
  const options = {
    autoplay: true,
    loop: true,
    margin: 30,
    navClass: ['d-none', 'd-none'],
    nav: false,
    navBtn: false,
    dots: true,
    responsive: {
      0: {
        items: 1,
      },
      600: {
        items: 3,
      },
      1000: {
        items: 4,
      },
    },
  };
  return (
    <main>
      <section className="second-page_bg">
        <h2>EXPLORE MORE IN LIMASSOL</h2>
      </section>
      {/* section search start */}
      <SearchComponentWhite />
      {/* section search end */}
      {/* properties auction */}
      <section className="Properties_for_auction">
        <div className="container">
          <div className="properties_auction_title">
            <h2>Properties for Auction</h2>
            <p>Properties for Auction</p>
          </div>
          <OwlCarousel options={options}>
            <div className="property_item">
              <Link to={'/propertyview/5282'} className="properties-items_link">
                <div className="property_item_img">
                  <img src="../images/1.jpg" alt={1} className="img-fluid" />
                  <div className="property-rate_in_img">
                    <span>Start Price</span>
                    <h6>€150,000.00</h6>
                  </div>
                </div>
                <div className="property_item_details">
                  <h3>3 Bedroom Apartment</h3>
                  <span>Limassol</span>
                  <p>RESIDENTIAL</p>
                  <div className="property_item_timing">
                    <img src="../images/timer.svg" alt="timer" className />
                    <span>2M:13D:5M:32S</span>
                  </div>
                </div>
              </Link>
            </div>
          </OwlCarousel>

          {/* <div className="properties_auction_item_slider">
            <div className="owl-carousel owl-theme" id="property_slider">
              <div className="property_item">
                <a
                  href="properties_item.html"
                  className="properties-items_link"
                >
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </a>
              </div>
              <div className="property_item">
                <a
                  href="properties_item.html"
                  className="properties-items_link"
                >
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </a>
              </div>
              <div className="property_item">
                <a
                  href="properties_item.html"
                  className="properties-items_link"
                >
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </a>
              </div>
              <div className="property_item">
                <a
                  href="properties_item.html"
                  className="properties-items_link"
                >
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </a>
              </div>
              <div className="property_item">
                <a
                  href="properties_item.html"
                  className="properties-items_link"
                >
                  <div className="property_item_img">
                    <img src="../images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="property_item_details">
                    <h3>3 Bedroom Apartment</h3>
                    <span>Limassol</span>
                    <p>RESIDENTIAL</p>
                    <div className="property_item_timing">
                      <img src="../images/timer.svg" alt="timer" className />
                      <span>2M:13D:5M:32S</span>
                    </div>
                  </div>
                </a>
              </div>
            </div>
          </div>*/}
        </div>
      </section>
      {/* properties auction */}
      <section className="feature_second_pg_sec">
        <div className="container">
          <div className="row">
            <div className="col-md-6 col-lg-3">
              <a href="#" className="feature_link">
                <div className="feature_second_pg_details">
                  <div className="feature_second_pg_img">
                    <img
                      src="../images/cyprus-3629039_1920.jpg"
                      alt
                      className="img-fluid"
                    />
                  </div>
                  <div className="feature_second_pg_title">
                    <h3>
                      Featured
                      <br /> Properties
                    </h3>
                    <img src="../images/arrow-secod-pg.svg" alt="alt-img"  />
                  </div>
                </div>
              </a>
            </div>
            <div className="col-md-6 col-lg-3">
              <a href="#" className="feature_link">
                <div className="feature_second_pg_details">
                  <div className="feature_second_pg_img">
                    <img
                      src="../images/cyprus-2640099_1920.jpg"
                      alt
                      className="img-fluid"
                    />
                  </div>
                  <div className="feature_second_pg_title">
                    <h3>
                      Upcoming
                      <br /> Auctions
                    </h3>
                    <img src="../images/arrow-secod-pg.svg" alt="alt-img"  />
                  </div>
                </div>
              </a>
            </div>
            <div className="col-md-6 col-lg-3">
              <a href="#" className="feature_link">
                <div className="feature_second_pg_details">
                  <div className="feature_second_pg_img">
                    <img
                      src="../images/church-2615744_1920.jpg"
                      alt
                      className="img-fluid"
                    />
                  </div>
                  <div className="feature_second_pg_title">
                    <h3>
                      Recently sold
                      <br /> Properties
                    </h3>
                    <img src="../images/arrow-secod-pg.svg" alt="alt-img"  />
                  </div>
                </div>
              </a>
            </div>
            <div className="col-md-6 col-lg-3">
              <a href="make-an-offer.html" className="feature_link">
                <div className="feature_second_pg_details">
                  <div className="feature_second_pg_img">
                    <img
                      src="../images/danny-feng-NLJNGP4r7Oo-unsplash.jpg"
                      alt
                      className="img-fluid"
                    />
                  </div>
                  <div className="feature_second_pg_title">
                    <h3>
                      Make an
                      <br /> Offer
                    </h3>
                    <img src="../images/arrow-secod-pg.svg" alt="alt-img"  />
                  </div>
                </div>
              </a>
            </div>
          </div>
        </div>
      </section>
      {/* feature four block */}
      {/* love our auction */}
      <section className="love_our_Auction">
        <h2>Love our Auction !</h2>
        <div className="love_our_auction_details">
          <div className="love_auction_box">
            <img src="../images/Group 8549.svg" alt="alt-img"  />
            <p>Subscribe</p>
          </div>
          <div className="love_auction_box">
            <Link to="/buyers-guide">
              <img src="../images/Group 8558.svg" alt="alt-img"  />
              <p>Buyer's Guide</p>
            </Link>
          </div>
          <div className="love_auction_box">
            <Link to="/sell-with-us">
              <img src="../images/Group 8592.svg" alt="alt-img"  />
              <p>Sell with us</p>
            </Link>
          </div>
        </div>
      </section>
      {/* love our auction */}
      {/* explore More */}
      <section className="explore_more_Second_page">
        <div className="container">
          <div className="explore_details">
            <h3>Explore More in</h3>
            <div className="dropdown">
              <button
                className="btn btn-drop-explore dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Limassol
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <a className="dropdown-item" href="#">
                  Action
                </a>
                <a className="dropdown-item" href="#">
                  Another action
                </a>
                <a className="dropdown-item" href="#">
                  Something else here
                </a>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* explore More */}
      {/* news-blog */}
      <section className="news_blog_second-page">
        <div className="container">
          <h2>News &amp; Blog</h2>
          <div className="row">
            <div className="col-md-6 col-lg-4">
              <div className="news_blog_details">
                <img
                  src="../images/blog-1.jpg"
                  alt="blog-1"
                  className="img-fluid News_banner_second_pg"
                />
                <h5>Lorem ipsum dolor sit amet, consectetur</h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut...
                </p>
                <div className="blog-owner">
                  <div className="blog-owner-details">
                    <img src="../images/bloger-1.jpg" alt="blog" />
                    <div className="blog-owner-name">
                      <p>Sam Michale</p>
                      <span>02nd Jan 2020</span>
                    </div>
                  </div>
                  <button onClick={() => history.push('/news_blog')}>
                    Read more
                  </button>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-4">
              <div className="news_blog_details">
                <img
                  src="../images/blog-1.jpg"
                  alt="blog-1"
                  className="img-fluid News_banner_second_pg"
                />
                <h5>Lorem ipsum dolor sit amet, consectetur</h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut...
                </p>
                <div className="blog-owner">
                  <div className="blog-owner-details">
                    <img src="../images/bloger-1.jpg" alt="alt-img"  />
                    <div className="blog-owner-name">
                      <p>Sam Michale</p>
                      <span>02nd Jan 2020</span>
                    </div>
                  </div>
                  <button onClick={() => history.push('/news_blog')}>
                    Read more
                  </button>
                </div>
              </div>
            </div>
            <div className="col-md-6 col-lg-4">
              <div className="news_blog_details">
                <img
                  src="../images/blog-1.jpg"
                  alt="blog-1"
                  className="img-fluid News_banner_second_pg"
                />
                <h5>Lorem ipsum dolor sit amet, consectetur</h5>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut...
                </p>
                <div className="blog-owner">
                  <div className="blog-owner-details">
                    <img src="../images/bloger-1.jpg" alt="alt-img"  />
                    <div className="blog-owner-name">
                      <p>Sam Michale</p>
                      <span>02nd Jan 2020</span>
                    </div>
                  </div>
                  <button onClick={() => history.push('/news_blog')}>
                    Read more
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      {/* news-blog */}
    </main>
  );
}
