import React from 'react';
import SearchComponentWhite from '../../Components/SearchComponent/SearchComponentWhite';
import { Link } from 'react-router-dom';
import Commercial from '../../images/commercial-property1.jpg'

export default function InvestmentCommerciaProperties() {
  return (
    <main style={{ backgroundColor: '#f1f1f1' }}>
      <section
        className="second-page_bg second_page_1_bg"
        style={{ backgroundImage: `url(${Commercial})` }}
      >
        <h2>Investment Commercia Properties</h2>
      </section>
      {/* section search start */}
      <SearchComponentWhite />

      {/* section search end */}
      <section className="gird-properties">
        <div className="container text-center">
          <h4>Investment Commercia Properties</h4>
        </div>
        <div className="container">
          <div className="short-list">
            <a href="#">
              <img src="images/grid.svg" alt="alt-img"  />
            </a>
            <a href="#">
              <img src="images/list-view.svg" alt="alt-img"  />
            </a>
            <div className="dropdown">
              <button
                className="btn btn-short-dp dropdown-toggle"
                type="button"
                id="dropdownMenuButton"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
              >
                Short by
              </button>
              <div
                className="dropdown-menu"
                aria-labelledby="dropdownMenuButton"
              >
                <a className="dropdown-item" href="#">
                  Popular
                </a>
                <a className="dropdown-item" href="#">
                  Low - High
                </a>
                <a className="dropdown-item" href="#">
                  High - Low
                </a>
              </div>
            </div>
          </div>
          <div className="grid_list_properties">
            <div className="row">
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1(2).jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>Detached House</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-3.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1(2).jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>Detached House</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-3.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1(2).jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>Detached House</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-1.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
              <div className="col-md-3">
                <Link className="card" to="/propertyview/5687">
                  <div className="property_item_img">
                    <img src="images/1-3.jpg" alt={1} className="img-fluid" />
                    <div className="property-rate_in_img">
                      <span>Start Price</span>
                      <h6>€150,000.00</h6>
                    </div>
                  </div>
                  <div className="card_details_properties">
                    <h4>3 Bedroom Apartment</h4>
                    <p>Limassol</p>
                    <span>RESIDENTIAL</span>
                    <h5>Offer Price : €1250</h5>
                  </div>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </section>
      <section className="pagination">
        <div className="container">
          <div className="pagination_second-pg">
            <a href="#" className="active">
              01
            </a>
            <a href="#">02</a>
            <a href="#">03</a>
            <a href="#">04</a>
            <a href="#" className="multi-pagination">
              <img src="images/three-dot.svg" alt="alt-img"  />
            </a>
            <a href="#">99</a>
          </div>
        </div>
      </section>
    </main>
  );
}
