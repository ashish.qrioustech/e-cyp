import React from 'react';

export default function CyprusResidency() {
  return (
    <main>
      <section
        className="second-page_bg second_page_1_bg"
        style={{ backgroundImage: 'url(images/cyprus-bg.jpg)' }}
      >
        <h2 style={{ textTransform: 'uppercase' }}>Cyprus Residency</h2>
      </section>
      <section className="Cyprus-Passport pt-5">
        <div className="container">
          <div className="contact-title mb-5">
            <h2>Cyprus Passport/Eu Citizenship Program</h2>
          </div>
          <div className="Cyprus-Passport-Details">
            <h4 className="mb-4">
              GET AN EUROPEAN PASSPORT WITH 2 MILLION INVESTMENT
            </h4>
            <p>
              The Cypriot Government has built up various motivators to draw in
              unfamiliar direct venture into the nation. One of these is a
              citizenship-by-venture program that awards full Cypriot
              citizenship to those that put more than EUR 2 million in land and
              who meet certain different prerequisites. Fruitful candidates gain
              the option to live, work and study in every one of the 28 EU part
              nations.{' '}
            </p>
            <p>
              In September 2016 , the Government of Cyprus acquainted changes
              with its citizenship-by-speculation program. The progressions
              incorporate the decrease of the base venture add up to EUR 2
              million (from EUR 2,5 million) and the likelihood to incorporate
              the guardians of the fundamental candidate gave that an extra EUR
              500,000 + VAT will be put resources into the acquisition of a
              private living arrangement in Cyprus.{' '}
            </p>
            <p>
              Any non-Cypriot resident may apply for Cypriot citizenship through
              Naturalization by Exception on the off chance that they meet one
              of the speculation standards introduced underneath. The capability
              may either be done actually or through an
              organization/organizations in which the candidate goes about as an
              investor or even as a high-positioning ranking director.
            </p>
          </div>
          <div className="Cyprus-Passport-Details mt-4 trems-condition">
            <h4 className="mb-4">MAIN BENEFITS</h4>
            <ul>
              <li>
                Programmed EU citizenship with the option to live, work, and
                study anyplace in the EU{' '}
              </li>
              <li>
                Without visa travel to 159 nations and domains, including
                Canada, Hong Kong, and Switzerland{' '}
              </li>
              <li>
                The citizenship is allowed to the entire family – spouse, wife,
                all monetarily reliant kids up to the age of 28.{' '}
              </li>
              <li>
                Lifetime citizenship; can be passed onto people in the future by
                plummet{' '}
              </li>
              <li>Quick and basic system (3 months) </li>
              <li>Double citizenship is permitted </li>
              <li>
                Securing of citizenship in Cyprus isn't accounted for to
                different nations{' '}
              </li>
              <li>Exclusive requirement of living </li>
              <li>
                Cyprus identification positioned seventeenth on the planet as
                indicated by the Visa Restrictions Index
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section className="Cyprus-Passport pt-5">
        <div className="container">
          <div className="contact-title mb-5">
            <h2>Cyprus Permanent Residency Program</h2>
          </div>
          <div className="Cyprus-Passport-Details">
            <h4 className="mb-4">
              ACQUIRE CYPRUS PERMANENT RESIDENCY WITH 300K INVESTMENT
            </h4>
            <p>
              Non-EU residents who buy property in Cyprus can profit by another
              and quickened method for getting a Permanent Residence Permit. The
              guardians (his or potentially her spouse's) of Permanent Residency
              holders or future candidates can likewise apply for a license.
              Given this new strategy and the investment funds of thousands of
              Euros because of the annulment of move expenses, presently is the
              best an ideal opportunity to purchase property in Cyprus.{' '}
            </p>
            <p>
              To discover how Emmeric Consultants can help you in the method of
              acquiring your license, it would be ideal if you get in touch with
              us.
            </p>
          </div>
          <div className="Cyprus-Passport-Details mt-4">
            <h4 className="mb-4">WHY IMMIGRATE TO CYPRUS</h4>
            <p>
              Arranged in the eastern Mediterranean Sea at the intersection of
              Europe, Africa, and Asia, the Republic of Cyprus is viewed as one
              of the biggest universal business and transportation places for
              Europe, Russia, and the Middle East.
            </p>
            <p>
              Its entrance into the European Union in 2004 makes Cyprus a
              sheltered domain for habitation and ventures. With its nearly all
              year daylight, all around created financial foundation, tranquil
              way of life, and low crime percentage, Cyprus is the ideal spot to
              appreciate life without limit.{' '}
            </p>
            <p>
              The nation's movement laws give speculators a supreme chance to
              put resources into the Cyprus economy and addition Permanent
              Residence licenses, and with the ongoing disclosure of huge stores
              of petroleum gas off the shore of Cyprus, the open doors are many.
            </p>
          </div>
        </div>
      </section>
      <section className="Cyprus-Passport py-5">
        <div className="container">
          <div className="contact-title mb-5">
            <h2>Cyprus Permanent Residency Benefits</h2>
          </div>
          <div className="Cyprus-Passport-Details trems-condition">
            <ul>
              <li>
                {' '}
                Cyprus Residence Permit is perpetual and no requirement for
                future reevaluation. It is a lifetime grant. Cyprus Residence
                Permit is perpetual and no requirement for future reevaluation.
                It is a lifetime grant.{' '}
              </li>
              <li>
                {' '}
                Cyprus Permanent Residence applies to the whole family
                (guardians, youngsters, grandparents).{' '}
              </li>
              <li>
                {' '}
                Cyprus Permanent Residents have the right (under specific
                conditions) to apply for a Cypriot visa following their physical
                nearness for a sum of 5 years inside a multi year time frame.{' '}
              </li>
              <li>
                {' '}
                When the Cyprus Permanent Residence Permit is gotten the
                candidate and his family remembered for his Immigration license
                will be qualified for apply for a Schengen Visa through any
                Schengen European Embassy. It is critical to take note of tat
                Cyprus is relied upon to enter the Schengen Zone soon.{' '}
              </li>
              <li>
                {' '}
                Cyprus has a casual, calm way of life with the most reduced
                crime percentage in Europe.{' '}
              </li>
              <li> No Inheritance Tax applies in Cyprus. </li>
              <li>
                {' '}
                Cyprus offers a perfect family condition, empowering your kids
                to profit by superb schools and colleges while human services
                and framework are all best in class.{' '}
              </li>
              <li>
                {' '}
                Cyprus is one of the most mainstream goals for setting up
                organizations with just a 12.5% corporate duty rate, and twofold
                tax assessment deals with very nearly 60 nations.{' '}
              </li>
              <li>
                {' '}
                Hazard free Freehold property guarantees perpetual property
                rights
              </li>
            </ul>
          </div>
        </div>
      </section>
      <section className="Most_visited_place_bg">
        <div className="container">
          <div className="row d-flex align-items-center">
            <div className="col-md-8">
              <h1>View Cyprus Residency</h1>
            </div>
            <div className="col-md-4 text-center">
              <button onclick="location.href='cyprus_residency-grid.html';">
                Visit Residency
              </button>
            </div>
          </div>
        </div>
      </section>
      <section className="contact_cyprus_residency py-5">
        <div className="container">
          <h2 className="text-center mb-4">Contact</h2>
          <div className="row d-flex justify-content-center">
            <div className="col-md-6">
              <form className="contact-form_form">
                <input type="text" placeholder="Name" />
                <div className="row">
                  <div className="col-lg-6">
                    <input type="text" placeholder="Email" />
                  </div>
                  <div className="col-lg-6">
                    <input type="text" placeholder="Phone" />
                  </div>
                </div>
                <textarea
                  name="contact_Messages"
                  id="contact_Messages"
                  rows={5}
                  placeholder="Meassage"
                  defaultValue={''}
                />
                <div className="text-center">
                  <button>Submit</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </main>
  );
}
