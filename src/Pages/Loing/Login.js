import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Axios from 'axios';
import {
  storeToken,
  getTempTokenOTP,
  storeTempTokenOTP,
  storeTimeSpand,
} from '../../util/storeData';
// import { BaseUrl } from '../../config/BaseUrl';
import Swal from 'sweetalert2';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getGlobalLangSelecter } from '../../selector/selector';

function Login(props) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(true);
  const [usernameerror, setusernameerror] = useState('');
  const [passworderror, setpassworderror] = useState('');
  const history = useHistory();
  // const dotenv = require('dotenv');
  // const env = dotenv.config().parsed;
  useEffect(() => {
    if (getTempTokenOTP()) {
      history.push('/otp');
    }
  }, []);
  const Request = {
    username,
    password,
    isOTP: true,
  };

  const validate = () => {
    if (!username) {
      setusernameerror('Please Enter the Email Address');
      return false;
    } else {
      return true;
    }
  };

  const validate1 = () => {
    if (!password) {
      setpassworderror('Please Enter Password');
      return false;
    } else {
      return true;
    }
  };

  const handelLogin = (e) => {
    e.preventDefault();
    const checkvalid = validate();
    const checkvalid1 = validate1();
    // console.log('base url--->', process.env.REACT_APP_SOCKET_URL);
    if (checkvalid && checkvalid1) {
      if (username && password) {
        Axios.post(
          `${process.env.REACT_APP_SOCKET_URL}mobileapi/login`,
          Request,
          { headers: { domain: 'cyprus.auctionsoftware.com' } }
        )
          .then((res) => {
            if (res.data.status === 'yes') {
              //storeToken(res.data.data);
              Swal.fire({
                title: res.data.message,
                icon: 'success',
                timer: 1500,
                showConfirmButton: false,
                position: 'center',
              });
              storeTempTokenOTP(res.data.data);
              storeTimeSpand(new Date().getTime());
              history.push('/otp');
              // history.push('/otp?token=' + res.data.data);
            }
            if (res.data.message === 'Invalid username or password') {
              Swal.fire({
                title: res.data.message,
                icon: 'warning',
                timer: 1500,
                showConfirmButton: false,
                position: 'center',
              });
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }
      setusernameerror('');
      setpassworderror('');
    }
  };
  return (
    <section className="login_reg_verify">
      <div className="container">
        <div className="">
          <div className="">
            <div className="login_row">
              <div className="icon-section_login login-column">
                <div className="text-center icon-section_login_img">
                  <a href="/">
                    <img
                      src="images/login-log.png"
                      alt="login-log"
                      className="img-fluid"
                    />
                  </a>
                </div>
                <div className="icon-section_login_link">
                  <h5>
                    {props.Language.e_cyprus}.{props.Language.com}
                  </h5>
                  <p>{props.Language.dont_have_acc}?</p>
                  <Link to="sign-up">{props.Language.register_here}</Link>
                </div>
              </div>
              <div className="login_form_pg login-column">
                <h3>{props.Language.caps_login}</h3>
                <form className="login_form_section">
                  <div className="form-group margin-20">
                    <label for="exampleInputEmail1">
                      {props.Language.email_ADD}
                    </label>
                    <input
                      type="email"
                      className="form-login"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Enter your email address"
                      name="email"
                      onChange={(e) => {
                        setUsername(e.target.value);
                        setusernameerror('');
                      }}
                      value={username}
                    />
                    <div style={{ fontSize: 12, color: 'red', marginTop: 5 }}>
                      {usernameerror}
                    </div>
                  </div>
                  <div className="form-group">
                    <label for="exampleInputPassword1">
                      {props.Language.p_s}
                    </label>
                    <div className="pass-view">
                      <input
                        type={showPassword ? 'password' : 'text'}
                        className="form-login"
                        id="exampleInputPassword1"
                        placeholder="Enter your password"
                        name="password"
                        onChange={(e) => {
                          setPassword(e.target.value);
                          setpassworderror('');
                        }}
                        value={password}
                      />
                      <div style={{ fontSize: 12, color: 'red', marginTop: 5 }}>
                        {passworderror}
                      </div>
                      <div
                        className="view-pass-eye"
                        onClick={() => setShowPassword(!showPassword)}
                      >
                        <img src="images/eye-regular.svg" alt="" />
                      </div>
                    </div>
                  </div>
                  <div className="form-check">
                    <label className="check_box_cn">
                      {props.Language.remem}
                      <input type="checkbox" checked="checked" />
                      <span className="checkmark"></span>
                    </label>
                  </div>
                  <button
                    type="submit"
                    className="btn btn-login-submit"
                    onClick={handelLogin}
                  >
                    {props.Language.l_g}
                  </button>
                </form>
                <div className="forgote-Login-section text-center">
                  <p>{props.Language.f_g}</p>
                  <p>
                    {props.Language.recov}{' '}
                    <Link to="/forgot-password">{props.Language.hereee}</Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Login);
