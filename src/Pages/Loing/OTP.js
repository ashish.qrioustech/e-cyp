import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Axios from 'axios';
import { storeToken, getTempTokenOTP } from '../../util/storeData';
import Swal from 'sweetalert2';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getGlobalLangSelecter } from '../../selector/selector';

const OTP = (props) => {
  const [otp, setOtp] = useState('');
  const history = useHistory();
  useEffect(() => {
    const opt = getTempTokenOTP();

    if (!opt) {
      history.push('/login');
    }
  }, []);
  const handleLogin = (e) => {
    e.preventDefault();
    if (otp.length !== 4 && !isNaN(otp)) {
      return;
    }
    Axios.post(`${process.env.REACT_APP_SOCKET_URL}mobileapi/verifyotp`, {
      token: getTempTokenOTP(),
      // isVerifyNumber: getParameterByName('isVerifyNumber') ? true : false,
      otp: otp,
    })
      .then((res) => {
        if (res.data.status === 'yes') {
          storeToken(res.data.data);
          Swal.fire({
            title: res.data.message,
            icon: 'success',
            timer: 1500,
            showConfirmButton: false,
            position: 'center',
          });
          history.push('/');
        } else {
          Swal.fire({
            title: res && res.data ? res.data.message : 'Something went wrong',
            icon: 'warning',
            timer: 1500,
            showConfirmButton: false,
            position: 'center',
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // function getParameterByName(name) {
  //   var url = window.location.href;
  //   name = name.replace(/[\[\]]/g, '\\$&');
  //   var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
  //     results = regex.exec(url);
  //   if (!results) return null;
  //   if (!results[2]) return '';
  //   return decodeURIComponent(results[2].replace(/\+/g, ' '));
  // }

  return (
    <section className="login_reg_verify">
      <div className="container">
        <div className>
          <div className>
            <div className="login_row">
              <div
                className="icon-section_login login-column"
                style={{ height: 600 }}
              >
                <div className="text-center icon-section_login_img">
                  <img
                    src="images/login-log.png"
                    alt="login-log"
                    className="img-fluid"
                  />
                </div>
                <div className="icon-section_login_link">
                  <h5>
                    {props.Language.e_cyprus}.{props.Language.com}
                  </h5>
                  <p>{props.Language.dont_have_acc}?</p>
                  <Link to="/login">{props.Language.login}</Link>
                </div>
              </div>
              <div className="login_form_pg login-column">
                <h3>{props.Language.otp}</h3>
                <div className="forgot-password">
                  <p>{props.Language.enter_the_otp}</p>
                </div>
                <form className="login_form_section">
                  <div className="form-group margin-20">
                    <label htmlFor="otp">{props.Language.otp}</label>
                    <input
                      type="text"
                      className="form-login"
                      id="otp"
                      autoFocus
                      aria-describedby="otp"
                      placeholder="Enter OTP"
                      maxLength="4"
                      onChange={(e) => setOtp(e.target.value)}
                      value={otp}
                    />
                  </div>
                  <button
                    type="submit"
                    className="btn btn-login-submit"
                    onClick={handleLogin}
                  >
                    {props.Language.sen}
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(OTP);
