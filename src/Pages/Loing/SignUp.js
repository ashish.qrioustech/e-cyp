import React, { useState } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Axios from 'axios';
import { validdateEmail } from '../../util/validation';
// import { BaseUrl } from '../../config/BaseUrl';
import Swal from 'sweetalert2';
import { getGlobalLangSelecter } from '../../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

function SignUp(props) {
  console.log(props,'******888888888888888888888')
  const [isPassword, setIsPassword] = useState(true);
  const [salutation, setSalutation] = useState('Mrs');
  const [fName, setFName] = useState('');
  const [lName, setLName] = useState('');
  const [email, setEmail] = useState('');
  const [phoneCode, setPhoneCode] = useState('+357');
  const [phoneNo, setPhoneNo] = useState('');
  const [password, setPassword] = useState('');
  const [cPassword, setCPassword] = useState('');
  // const dotenv = require('dotenv');
  const history = useHistory();

  //validation 
  const [fnameerror,setfnameerror] = useState('');
  const [lnameerror,setlnameerror] = useState('');
  const [emailerror,setemailerror] = useState('');
  const [phonenoerror,setphonenoerror] = useState('');
  const [passworderror,setpassworderror] = useState('');
  const [cPassworderror,setCPassworderror] = useState('');
 const validate1 = ()=>{
  if(!fName){
  setfnameerror('Please Enter First Name')
  return false;
  }
  return true;
  }
  const validate2 = ()=>{
  if(!lName){
  setlnameerror('Please Enter Last Name')
  return false;
  }
  return true;
  }
  const validate3 = ()=>{
  if(!email){
  setemailerror('Please Enter Email Address');
  return false;
  }
  return true;
  }
  const validate4 = ()=>{
  if(!phoneNo){
  setphonenoerror('Please Enter Phone Number');
  return false;
  }
  return true;
  }
  const validate5 = ()=>{
  if(!password){
  setpassworderror('Please Enter Password');
  return false;
  }
  return true;
  }
  const validate6 = ()=>{
  if(!cPassword){
  setCPassworderror('Please Enter Confirm Password');
  return false;
  }
  return true;
 }
 const validate7 = ()=>{
  if(password !== cPassword){
  setCPassworderror('Password doesnot match')
  setpassworderror('')
  return false;
  }
  return true;
 }
 const validate8 = ()=>{
   if(!fName.match(/^[a-zA-Z ]+$/) && fName ){
  setfnameerror('First Name Should Contain Only Letters!')
   return false
   }
   return true
 };
 const validate9 = ()=>{
  if(!lName.match(/^[a-zA-Z ]+$/) && lName){
  setlnameerror('Last Name Should Contain Only Letters!')
  return false
  }
  return true
};
const validate10 = () => {
  if(phoneNo && !phoneNo.match(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/)){
    setphonenoerror('Phone number is not valid!')
    return false
  }
  return true
}
const validate11 = () => {
  if(password.length < 8 && password){
    setpassworderror('Password needs to be more than 8 characters!')
    setCPassworderror('')
    return false
  }
  return true
}
const validate12 = () => {
  if(email && !email.match(/\S+@\S+\.\S+/)){
   setemailerror('Email is Invalid');
   return false
  }
  return true
}

  const handelSubmit = (e) => {
    e.preventDefault();
    const validatecheck1 = validate1();
    const validatecheck2 = validate2();
    const validatecheck3 = validate3();
    const validatecheck4 = validate4();
    const validatecheck5 = validate5();
    const validatecheck6 = validate6();
    const validatecheck7 = validate7();
    const validatecheck8 = validate8();
    const validatecheck9 = validate9();
    const validatecheck10 = validate10();
    const validatecheck11 = validate11();
    const validatecheck12 = validate12();

    if(validatecheck1 && validatecheck2 && validatecheck3 && validatecheck4 && validatecheck5 && validatecheck6 && validatecheck7 && validatecheck8 && validatecheck9 && validatecheck10 && validatecheck11 && validatecheck12){
    if (
      salutation ||
      fName ||
      lName ||
      validdateEmail(email) ||
      phoneCode ||
      phoneNo ||
      password
    ) {
      if (password === cPassword) {
        // https://forwardapi.auctionsoftware.com/mobileapi/
        // console.log("register===>",Request)
        // return
        Axios.post(`${process.env.REACT_APP_SOCKET_URL}mobileapi/register`, {...Request,status : 'unverified',isOTP : true})
          .then((res) => {
            if (res.data.status === 'yes') {
              Swal.fire({
                title: res.data.message,
                icon: 'success',
                timer:1500,
                showConfirmButton: false,
                position:'center'
              })
              history.push('/otp?token='+res.data.data+'&isVerifyNumber=true');
            }else{
              Swal.fire({
                title: res.data.message,
                icon: 'error',
                timer:1500,
                showConfirmButton: false,
                position:'center'
              })
            }
            
          })
          .catch((error) => {
            console.log(error);
          });
      } else {
        Swal.fire({
          title: 'Password not match',
          icon: 'error',
          timer:1500,
          showConfirmButton: false,
          position:'center'
        })
        
      }
    }
    setfnameerror('') 
    setlnameerror('') 
    setemailerror('') 
    setphonenoerror('') 
    setpassworderror('') 
    setCPassworderror('')
  }
  };
  const Request = {
    first_name: `${salutation} ${fName}`,
    last_name: lName,
    email,
    phone: phoneNo,
    country: phoneCode,
    password,
    city: '',
    state: '',
    address1: '',
    zip: '',
  };
  const handelShowPassword = () => {
    setIsPassword(!isPassword);
  };
  return (
    <section className="login_reg_verify">
      <div className="container">
        <div className="">
          <div className="">
            <div className="login_row registration_pg_row">
              <div className="icon-section_login login-column">
                <div className="text-center icon-section_login_img">
                  <a href='/'>
                  <img
                    src="../images/login-log.png"
                    alt="login-log"
                    className="img-fluid"
                  />
                  </a>
                </div>
                <div className="icon-section_login_link">
                  <h5>{props.Language.e_cyprus}.{props.Language.com}</h5>
                  <p>{props.Language.Already_have}</p>
  <Link to="/login">{props.Language.Login_here}</Link>
                </div>
              </div>
              <div className="login_form_pg login-column">
  <h3>{props.Language.register}</h3>
                <form className="login_form_section registration_form_section">
                  <div className="form-group d-flex align-items-center">
                    <label for="Email" style={{ marginBottom: '0px' }}>
                      {props.Language.salutation}:
                    </label>
                    <div className="Country_code">
                      <select
                        name="CountryCode"
                        id="CountryCode"
                        onClick={(e) => {
                          setSalutation(e.target.value);
                        }}
                      >
                        <option value="mr">{props.Language.mr}</option>
                        <option value="mrs">{props.Language.mrs}</option>
                        <option value="miss">{props.Language.miss}</option>
                      </select>
                    </div>
                  </div>
                  <div className="form-group">
                      <label for="exampleInputEmail1">{props.Language.first_name}</label>
                    <input
                      type="text"
                      className="form-login"
                      id="exampleInputEmail1"
                      placeholder="Enter your first name"
                      onChange={(e) => {
                        setFName(e.target.value);
                        setfnameerror('');
                      }}
                      value={fName}
                    />
                     <div style={{fontSize:12,color:"red",marginTop:5}}>
                       {fnameerror}
                    </div>
                  </div>
                  <div className="form-group">
                    <label for="exampleInputEmail1">{props.Language.last_name}</label>
                    <input
                      type="text"
                      className="form-login"
                      id="exampleInputEmail1"
                      placeholder="Enter your last name"
                      onChange={(e) => {
                        setLName(e.target.value);
                        setlnameerror('');
                      }}
                      value={lName}
                    />
                     <div style={{fontSize:12,color:"red",marginTop:5}}>
                       {lnameerror}
                    </div>
                  </div>
                  <div className="form-group">
                    <label for="exampleInputEmail1">{props.Language.email_addr}</label>
                    <input
                      type="email"
                      className="form-login"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Enter your email address"
                      onChange={(e) => {
                        setEmail(e.target.value);
                        setemailerror('');
                      }}
                      value={email}
                    />
                     <div style={{fontSize:12,color:"red",marginTop:5}}>
                       {emailerror}
                    </div>
                  </div>
                  <div className="form-group">
                    <label for="exampleInputEmail1">{props.Language.phone}</label>
                    <div className="registre_pg_dp_phone">
                      <select onClick={(e) => setPhoneCode(e.target.value)}>
                        <option value="+357">Cyprus (+357)</option>
                        <option data-countryCode="DZ" value="213">
                          Algeria (+213)
                        </option>
                        <option data-countryCode="AD" value="376">
                          Andorra (+376)
                        </option>

                        <option data-countryCode="IN" value="91">
                          India (+91)
                        </option>
                        <option data-countryCode="ID" value="62">
                          Indonesia (+62)
                        </option>
                        <option data-countryCode="IR" value="98">
                          Iran (+98)
                        </option>
                        <option data-countryCode="IQ" value="964">
                          Iraq (+964)
                        </option>
                        <option data-countryCode="IE" value="353">
                          Ireland (+353)
                        </option>
                        <option data-countryCode="IL" value="972">
                          Israel (+972)
                        </option>
                        <option data-countryCode="IT" value="39">
                          Italy (+39)
                        </option>
                        <option data-countryCode="JM" value="1876">
                          Jamaica (+1876)
                        </option>
                        <option data-countryCode="JP" value="81">
                          Japan (+81)
                        </option>
                        <option data-countryCode="JO" value="962">
                          Jordan (+962)
                        </option>
                        <option data-countryCode="KZ" value="7">
                          Kazakhstan (+7)
                        </option>
                        <option data-countryCode="KE" value="254">
                          Kenya (+254)
                        </option>
                        <option data-countryCode="KI" value="686">
                          Kiribati (+686)
                        </option>
                        <option data-countryCode="KP" value="850">
                          Korea North (+850)
                        </option>
                        <option data-countryCode="KR" value="82">
                          Korea South (+82)
                        </option>
                        <option data-countryCode="KW" value="965">
                          Kuwait (+965)
                        </option>
                        <option data-countryCode="KG" value="996">
                          Kyrgyzstan (+996)
                        </option>
                        <option data-countryCode="LA" value="856">
                          Laos (+856)
                        </option>
                        <option data-countryCode="LV" value="371">
                          Latvia (+371)
                        </option>
                        <option data-countryCode="LB" value="961">
                          Lebanon (+961)
                        </option>
                        <option data-countryCode="LS" value="266">
                          Lesotho (+266)
                        </option>
                        <option data-countryCode="LR" value="231">
                          Liberia (+231)
                        </option>
                        <option data-countryCode="LY" value="218">
                          Libya (+218)
                        </option>
                        <option data-countryCode="LI" value="417">
                          Liechtenstein (+417)
                        </option>
                        <option data-countryCode="LT" value="370">
                          Lithuania (+370)
                        </option>
                        <option data-countryCode="LU" value="352">
                          Luxembourg (+352)
                        </option>
                        <option data-countryCode="MO" value="853">
                          Macao (+853)
                        </option>
                        <option data-countryCode="MK" value="389">
                          Macedonia (+389)
                        </option>
                        <option data-countryCode="MG" value="261">
                          Madagascar (+261)
                        </option>
                        <option data-countryCode="MW" value="265">
                          Malawi (+265)
                        </option>
                        <option data-countryCode="MY" value="60">
                          Malaysia (+60)
                        </option>
                        <option data-countryCode="MV" value="960">
                          Maldives (+960)
                        </option>
                        <option data-countryCode="ML" value="223">
                          Mali (+223)
                        </option>
                        <option data-countryCode="MT" value="356">
                          Malta (+356)
                        </option>
                        <option data-countryCode="MH" value="692">
                          Marshall Islands (+692)
                        </option>
                        <option data-countryCode="MQ" value="596">
                          Martinique (+596)
                        </option>
                        <option data-countryCode="MR" value="222">
                          Mauritania (+222)
                        </option>
                        <option data-countryCode="YT" value="269">
                          Mayotte (+269)
                        </option>
                        <option data-countryCode="MX" value="52">
                          Mexico (+52)
                        </option>
                        <option data-countryCode="FM" value="691">
                          Micronesia (+691)
                        </option>
                        <option data-countryCode="MD" value="373">
                          Moldova (+373)
                        </option>
                        <option data-countryCode="MC" value="377">
                          Monaco (+377)
                        </option>
                        <option data-countryCode="MN" value="976">
                          Mongolia (+976)
                        </option>
                        <option data-countryCode="MS" value="1664">
                          Montserrat (+1664)
                        </option>
                        <option data-countryCode="MA" value="212">
                          Morocco (+212)
                        </option>
                        <option data-countryCode="MZ" value="258">
                          Mozambique (+258)
                        </option>
                        <option data-countryCode="MN" value="95">
                          Myanmar (+95)
                        </option>
                        <option data-countryCode="NA" value="264">
                          Namibia (+264)
                        </option>
                        <option data-countryCode="NR" value="674">
                          Nauru (+674)
                        </option>
                        <option data-countryCode="NP" value="977">
                          Nepal (+977)
                        </option>
                        <option data-countryCode="NL" value="31">
                          Netherlands (+31)
                        </option>
                        <option data-countryCode="NC" value="687">
                          New Caledonia (+687)
                        </option>
                        <option data-countryCode="NZ" value="64">
                          New Zealand (+64)
                        </option>
                        <option data-countryCode="NI" value="505">
                          Nicaragua (+505)
                        </option>
                        <option data-countryCode="NE" value="227">
                          Niger (+227)
                        </option>
                        <option data-countryCode="NG" value="234">
                          Nigeria (+234)
                        </option>
                        <option data-countryCode="NU" value="683">
                          Niue (+683)
                        </option>
                        <option data-countryCode="NF" value="672">
                          Norfolk Islands (+672)
                        </option>
                        <option data-countryCode="NP" value="670">
                          Northern Marianas (+670)
                        </option>
                        <option data-countryCode="NO" value="47">
                          Norway (+47)
                        </option>
                        <option data-countryCode="OM" value="968">
                          Oman (+968)
                        </option>
                        <option data-countryCode="PW" value="680">
                          Palau (+680)
                        </option>
                        <option data-countryCode="PA" value="507">
                          Panama (+507)
                        </option>
                        <option data-countryCode="PG" value="675">
                          Papua New Guinea (+675)
                        </option>
                        <option data-countryCode="PY" value="595">
                          Paraguay (+595)
                        </option>
                        <option data-countryCode="PE" value="51">
                          Peru (+51)
                        </option>
                        <option data-countryCode="PH" value="63">
                          Philippines (+63)
                        </option>
                        <option data-countryCode="PL" value="48">
                          Poland (+48)
                        </option>
                        <option data-countryCode="PT" value="351">
                          Portugal (+351)
                        </option>
                        <option data-countryCode="PR" value="1787">
                          Puerto Rico (+1787)
                        </option>
                        <option data-countryCode="QA" value="974">
                          Qatar (+974)
                        </option>
                        <option data-countryCode="RE" value="262">
                          Reunion (+262)
                        </option>
                        <option data-countryCode="RO" value="40">
                          Romania (+40)
                        </option>
                        <option data-countryCode="RU" value="7">
                          Russia (+7)
                        </option>
                        <option data-countryCode="RW" value="250">
                          Rwanda (+250)
                        </option>
                        <option data-countryCode="SM" value="378">
                          San Marino (+378)
                        </option>
                        <option data-countryCode="ST" value="239">
                          Sao Tome &amp; Principe (+239)
                        </option>
                        <option data-countryCode="SA" value="966">
                          Saudi Arabia (+966)
                        </option>
                        <option data-countryCode="SN" value="221">
                          Senegal (+221)
                        </option>
                        <option data-countryCode="CS" value="381">
                          Serbia (+381)
                        </option>
                        <option data-countryCode="SC" value="248">
                          Seychelles (+248)
                        </option>
                        <option data-countryCode="SL" value="232">
                          Sierra Leone (+232)
                        </option>
                        <option data-countryCode="SG" value="65">
                          Singapore (+65)
                        </option>
                        <option data-countryCode="SK" value="421">
                          Slovak Republic (+421)
                        </option>
                        <option data-countryCode="SI" value="386">
                          Slovenia (+386)
                        </option>
                        <option data-countryCode="SB" value="677">
                          Solomon Islands (+677)
                        </option>
                        <option data-countryCode="SO" value="252">
                          Somalia (+252)
                        </option>
                        <option data-countryCode="ZA" value="27">
                          South Africa (+27)
                        </option>
                        <option data-countryCode="ES" value="34">
                          Spain (+34)
                        </option>
                        <option data-countryCode="LK" value="94">
                          Sri Lanka (+94)
                        </option>
                        <option data-countryCode="SH" value="290">
                          St. Helena (+290)
                        </option>
                        <option data-countryCode="KN" value="1869">
                          St. Kitts (+1869)
                        </option>
                        <option data-countryCode="SC" value="1758">
                          St. Lucia (+1758)
                        </option>
                        <option data-countryCode="SD" value="249">
                          Sudan (+249)
                        </option>
                        <option data-countryCode="SR" value="597">
                          Suriname (+597)
                        </option>
                        <option data-countryCode="SZ" value="268">
                          Swaziland (+268)
                        </option>
                        <option data-countryCode="SE" value="46">
                          Sweden (+46)
                        </option>
                        <option data-countryCode="CH" value="41">
                          Switzerland (+41)
                        </option>
                        <option data-countryCode="SI" value="963">
                          Syria (+963)
                        </option>
                        <option data-countryCode="TW" value="886">
                          Taiwan (+886)
                        </option>
                        <option data-countryCode="TJ" value="7">
                          Tajikstan (+7)
                        </option>
                        <option data-countryCode="TH" value="66">
                          Thailand (+66)
                        </option>
                        <option data-countryCode="TG" value="228">
                          Togo (+228)
                        </option>
                        <option data-countryCode="TO" value="676">
                          Tonga (+676)
                        </option>
                        <option data-countryCode="TT" value="1868">
                          Trinidad &amp; Tobago (+1868)
                        </option>
                        <option data-countryCode="TN" value="216">
                          Tunisia (+216)
                        </option>
                        <option data-countryCode="TR" value="90">
                          Turkey (+90)
                        </option>
                        <option data-countryCode="TM" value="7">
                          Turkmenistan (+7)
                        </option>
                        <option data-countryCode="TM" value="993">
                          Turkmenistan (+993)
                        </option>
                        <option data-countryCode="TC" value="1649">
                          Turks &amp; Caicos Islands (+1649)
                        </option>
                        <option data-countryCode="TV" value="688">
                          Tuvalu (+688)
                        </option>
                        <option data-countryCode="UG" value="256">
                          Uganda (+256)
                        </option>
                        <option data-countryCode="UA" value="380">
                          Ukraine (+380)
                        </option>
                        <option data-countryCode="AE" value="971">
                          United Arab Emirates (+971)
                        </option>
                        <option data-countryCode="UY" value="598">
                          Uruguay (+598)
                        </option>
                        <option data-countryCode="UZ" value="7">
                          Uzbekistan (+7)
                        </option>
                        <option data-countryCode="VU" value="678">
                          Vanuatu (+678)
                        </option>
                        <option data-countryCode="VA" value="379">
                          Vatican City (+379)
                        </option>
                        <option data-countryCode="VE" value="58">
                          Venezuela (+58)
                        </option>
                        <option data-countryCode="VN" value="84">
                          Vietnam (+84)
                        </option>
                        <option data-countryCode="VG" value="84">
                          Virgin Islands - British (+1284)
                        </option>
                        <option data-countryCode="VI" value="84">
                          Virgin Islands - US (+1340)
                        </option>
                        <option data-countryCode="WF" value="681">
                          Wallis &amp; Futuna (+681)
                        </option>
                        <option data-countryCode="YE" value="969">
                          Yemen (North)(+969)
                        </option>
                        <option data-countryCode="YE" value="967">
                          Yemen (South)(+967)
                        </option>
                        <option data-countryCode="ZM" value="260">
                          Zambia (+260)
                        </option>
                        <option data-countryCode="ZW" value="263">
                          Zimbabwe (+263)
                        </option>
                      </select>
                      <input
                        type="text"
                        className="form-login"
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                        placeholder="Enter your phone number"
                        onChange={(e) => {
                          setPhoneNo(e.target.value);
                          setphonenoerror('');
                        }}
                        value={phoneNo}
                      />
                    </div>
                    <div style={{fontSize:12,color:"red",marginTop:5}}>
                       {phonenoerror}
                    </div>
                  </div>
                  <div className="form-group">
                      <label for="exampleInputPassword1">{props.Language.p_s}</label>
                    <div className="pass-view">
                      <input
                        type={isPassword ? 'password' : 'text'}
                        className="form-login"
                        id="exampleInputPassword1"
                        placeholder="Enter your password"
                        onChange={(e) => {
                          setPassword(e.target.value);
                          setpassworderror('');
                        }}
                        value={password}
                      />
                       <div style={{fontSize:12,color:"red",marginTop:5}}>
                       {passworderror}
                    </div>
                      <div
                        className="view-pass-eye"
                        onClick={handelShowPassword}
                      >
                        <img src="images/eye-regular.svg" alt="" />
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                      <label for="exampleInputPassword1">{props.Language.c_f}</label>
                    <input
                      type={isPassword ? 'password' : 'text'}
                      className="form-login"
                      id="confirm_Password"
                      name="confirm_Password"
                      placeholder="Confirm your password"
                      onChange={(e) => {
                        setCPassword(e.target.value);
                        setCPassworderror('')
                      }}
                      value={cPassword}
                    />
                     <div style={{fontSize:12,color:"red",marginTop:5}}>
                       {cPassworderror}
                    </div>
                  </div>
                  <div className="form-check reg_pg_check">
                    <label className="check_box_cn">
                    {props.Language.I_agree} <a href="#">{props.Language.ter} &amp; {props.Language.condition}</a>
                      <input type="checkbox" checked="checked" />
                      <span className="checkmark"></span>
                    </label>
                  </div>
                  <button
                    type="submit"
                    className="btn btn-login-submit"
                    onClick={handelSubmit}
                  >
                    {props.Language.create_my}
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);