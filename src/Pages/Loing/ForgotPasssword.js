import React, { useState } from 'react';
import { validdateEmail } from '../../util/validation';
import { Link, useHistory } from 'react-router-dom';
import Axios from 'axios';
import Swal from 'sweetalert2';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getGlobalLangSelecter } from '../../selector/selector';

 function ForgotPasssword(props) {
  const [email, setEmail] = useState('');
  const history = useHistory();
  const Req = {
    email:email
  };
  

  const handleForgotPasswd = (e) => {
    e.preventDefault();  
    console.log(Req.email)
    if (validdateEmail(email)) {
      const token = localStorage.getItem('auctionToken');
      Axios.post('https://forwardapi.auctionsoftware.com/mobileapi/forgot_password',Req,{headers:  {"Authorization": `Bearer ${token}`,"domain":"cyprus.auctionsoftware.com"}})
        .then((res) => {
          console.log(res.data);
          if (res.data.status === 'yes') {
            Swal.fire({
              title:res.data.message,
              icon: 'success',
              timer:1500,
              showConfirmButton: false,
              position:'center'
            })
            history.push('/login');
            
          } else {
             Swal.fire({
        title:'Email Address is Invalid',
        icon: 'warning',
        timer:1500,
        showConfirmButton: false,
        position:'center'
      })
          }
          //   console.log(res.data.message);
        })
        .catch((error) => {
          console.log(error);
        });
    } else if(email === '') {
      Swal.fire({
        title:'Please Enter the Email Address',
        icon: 'warning',
        timer:1500,
        showConfirmButton: false,
        position:'center'
      })
    } else {
      Swal.fire({
        title:'Email Address Invalid',
        icon: 'warning',
        timer:1500,
        showConfirmButton: false,
        position:'center'
      })
    }
  };
  return (
    <section className="login_reg_verify">
      <div className="container">
        <div className>
          <div className>
            <div className="login_row">
              <div
                className="icon-section_login login-column"
                style={{ height: 600 }}
              >
                <div className="text-center icon-section_login_img">
                  <img
                    src="images/login-log.png"
                    alt="login-log"
                    className="img-fluid"
                  />
                </div>
                <div className="icon-section_login_link">
                  <h5>{props.Language.e_cyprus}.{props.Language.com}</h5>
                  <p>{props.Language.dont_have_acc}?</p>
                  <Link to="/login">{props.Language.login}</Link>
                </div>
              </div>
              <div className="login_form_pg login-column">
  <h3>{props.Language.forg}</h3>
                <div className="forgot-password">
                  <p>{props.Language.enter_the_e}.</p>
                  <span>{props.Language.we_will_e}.</span>
                </div>
                <form className="login_form_section">
                  <div className="form-group margin-20">
                    <label htmlFor="exampleInputEmail1">{props.Language.	
email_ADD}</label>
                    <input
                      type="email"
                      className="form-login"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Enter your email address"
                      onChange={(e) => setEmail(e.target.value)}
                      value={email}
                    />
                  </div>
                  {/* <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <div class="pass-view">
                      <input type="password" class="form-login" id="exampleInputPassword1" placeholder="Enter your password">
                      <a href="#" class="view-pass-eye">
                        <img src="images/eye-regular.svg" alt="">
                      </a>
                    </div>
                  </div> */}
                  {/* <div class="form-check">
                    <label class="check_box_cn">Remember me
                        <input type="checkbox" checked="checked">
                        <span class="checkmark"></span>
                      </label>
                  </div> */}
                  <button
                    type="submit"
                    className="btn btn-login-submit"
                    onClick={handleForgotPasswd}
                  >
                    {props.Language.sen}
                  </button>
                </form>
                {/* <div class="forgote-Login-section text-center">
                    <p>
                        Forgot password?
                    </p>
                    <p>
                        Recover your password <a href="#">here</a>
                    </p>
                </div> */}
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}


const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasssword);
