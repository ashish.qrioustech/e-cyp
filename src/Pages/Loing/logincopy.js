import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Axios from 'axios';
import { storeToken } from '../../util/storeData';
// import { BaseUrl } from '../../config/BaseUrl';
import Swal from 'sweetalert2';

function Login() {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showPassword, setShowPassword] = useState(true);
  const [usernameerror,setusernameerror] = useState({});
  const [passworderror, setpassworderror] = useState({});
  const history = useHistory();
  // const dotenv = require('dotenv');
  // const env = dotenv.config().parsed;
  // useEffect(() => {
  //   // Axios.post(BaseUrl + "login", {}, "cyprus.auctionsoftware.com").then(
  //   //   (res) => {
  //   //     console.log(res);
  //   //   }
  //   // );
  // }, []);
  const Request = {
    username,
    password,
  };

  const validate = ()=>{
  
  if(!username ){
    setusernameerror('Please Enter the Email Address')
    return false
  }
  else {
    return true;
  }
   
  };

  
  const handelLogin = (e) => {
    e.preventDefault();
    const checkvalid = validate();
    if(checkvalid){
      if (username && password) {
        Axios.post('https://forwardapi.auctionsoftware.com/mobileapi/' + 'login', Request,{headers:  {"domain":"cyprus.auctionsoftware.com"}})
          .then((res) => {
            if (res.data.status === 'yes') {
              storeToken(res.data.data);
              Swal.fire({
                title: 'Login Successful',
                icon: 'success',
                timer:1500,
                showConfirmButton: false,
                position:'center'
              })
              history.push('/');
            }
            if(res.data.message==='Invalid username or password'){
            Swal.fire({
              title: res.data.message,
              icon: 'warning',
              timer:1500,
              showConfirmButton: false,
              position:'center'
            })
          }
          })
          .catch((error) => {
            console.log(error);
          });
      }
      setusernameerror('')
      setpassworderror('')
    }
    
  };
  return (
    <section className="login_reg_verify">
      <div className="container">
        <div className="">
          <div className="">
            <div className="login_row">
              <div className="icon-section_login login-column">
                <div className="text-center icon-section_login_img">
                  <a href="/">
                  <img
                    src="images/login-log.png"
                    alt="login-log"
                    className="img-fluid"
                  />
                  </a>
                </div>
                <div className="icon-section_login_link">
                  <h5>e-cyprusauctions.com</h5>
                  <p>Don't have an account?</p>
                  <Link to="sign-up">Register here</Link>
                </div>
              </div>
              <div className="login_form_pg login-column">
                <h3>LOGIN</h3>
                <form className="login_form_section">
                  <div className="form-group margin-20">
                    <label for="exampleInputEmail1">Email address</label>
                    <input
                      type="email"
                      className="form-login"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Enter your email address"
                      name="email"
                      onChange={(e) => setUsername(e.target.value)}
                      value={username}
                    />
                    <div style={{fontSize:12,color:"red",marginTop:5}}>
                       {usernameerror}
                    </div>
                  </div>
                  <div className="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <div className="pass-view">
                      <input
                        type={showPassword ? 'password' : 'text'}
                        className="form-login"
                        id="exampleInputPassword1"
                        placeholder="Enter your password"
                        name="password"
                        onChange={(e) => setPassword(e.target.value)}
                        value={password}
                      />
                       <div style={{fontSize:12,color:"red",marginTop:5}}>
                       {passworderror}
                    </div>
                      <div
                        className="view-pass-eye"
                        onClick={() => setShowPassword(!showPassword)}
                      >
                        <img src="images/eye-regular.svg" alt="" />
                      </div>
                    </div>
                  </div>
                  <div className="form-check">
                    <label className="check_box_cn">
                      Remember me
                      <input type="checkbox" checked="checked" />
                      <span className="checkmark"></span>
                    </label>
                  </div>
                  <button
                    type="submit"
                    className="btn btn-login-submit"
                    onClick={handelLogin}
                  >
                    Login
                  </button>
                </form>
                <div className="forgote-Login-section text-center">
                  <p>Forgot password?</p>
                  <p>
                    Recover your password{' '}
                    <Link to="/forgot-password">here</Link>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}
export default Login;
