import React from 'react'
import { useEffect, useState } from 'react';

import { Link, useParams } from 'react-router-dom';
import { Tab, Nav } from 'react-bootstrap';
import { GoogleMap, Marker } from "react-google-maps"
import Maps from '../../Components/Maps'
import Axios from 'axios';
import moment from "moment";
import { subscribeToTimer, bidUpdate } from "../../util/Services/index";
import { getTimeInterval} from "../../util/Common";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import {
  // getAuthToken,
  getAuctionToken,
  // getPropId,
} from "../../util/storeData/index";
import jwt_decode from 'jwt-decode'

export default function PropertiesItems() {
   let {id} = useParams();
  const [propertyDetails, setPropertyDetails] = useState({});
  const [attachmentsList, setattachmentsList] = useState([]);
  const [legaldocs, setlegaldocs] = useState([]);
  const [brochure, setbrochure] = useState([]);  
  const [serverTimeSocket, setServerTimeSocket] = useState("");
  const [bidHisstoryList, setBidHisstoryList] = useState([]);
  const [bidDetails, setBidDetails] = useState([]);
  const [amount, setAmount] = useState("");
  const [timer, setTimer] = useState({});
  const [propertyId, setpropertyId] = useState({});
  const [userId, setUserId] = useState({});
  const [increment, setIncrementAmount] = useState({});
  
  
    
    useEffect(() => {
        let params = {
          product_id: id
        }
        let userId;
        let auctionToken= localStorage.getItem('auctionToken');
        if(auctionToken){
          let currentUerData= jwt_decode(auctionToken);
          userId = currentUerData.id
          setUserId(userId)
        }
        let header = {};
        header["authorization"] = "Bearer " + auctionToken;
        Axios.post(process.env.REACT_APP_API + 'getprodetails', params, header)
          .then((res) => {
            if (res.data.success === 'yes') {
              setPropertyDetails(res.data.results);
              setpropertyId(res.data.results.id);
              userId && updateBidDetails(res.data.results.id,userId)
              setIncrementAmount(res.data.incrementamt);
              console.log("res.data.results.incrementamt ---", res.data.incrementamt);
              let params = {
                product_id: res.data.results.id
              }
              getBidDetails(params);
              console.log("res.data.results.id --", res.data.results.id);
              console.log("setpropertyId ---", propertyId);
              //console.log("res.data.attachments ---", res.data.attachments);
              setattachmentsList(res.data.attachments);
              let attachments = res.data.attachments
              let legaldocs = (attachments).filter(function(attachments) {
                // attachments=> attachments.type == "legaldoc"
                if(attachments.type == "legaldoc") {
                  return attachments.src = process.env.REACT_APP_LEGALDOC_IMAGE_URL + attachments.src
                }
              });
              setlegaldocs(legaldocs);
              console.log("legaldoc ======", legaldocs);
              let brochure = (attachments).filter(function(attachments) {
                if(attachments.type == "brochure") {
                  return attachments.src = process.env.REACT_APP_LEGALDOC_IMAGE_URL + attachments.src
                }
              });
              setbrochure(brochure);
              setBidDetails(res.data.biddetails);
              
              var timer = 0;
              subscribeToTimer((err, timestamp) => {
                let { dTime: serverTime } = timestamp;
                serverTime = moment(serverTime);
                setServerTimeSocket(serverTime);
                timer = getTimeInterval(
                  moment(res.data.results.date_added),
                  moment(res.data.results.date_closed),
                  serverTime
                )
                setTimer(timer);
                
              });
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }, []);

      const updateBidDetails = (propertyId,userId) =>{
        console.log("bidUpdateData ----", propertyId);
        bidUpdate((err, bidUpdateData) => {
          if(err){
            console.log("socket res------", err);
          } else {
            console.log("propertyId -----", propertyId);
            console.log("bidUpdateData -----", bidUpdateData);
            if(propertyId == bidUpdateData.id) {
              let params = {
                product_id : propertyId
              }
              getBidDetails(params);
              if(userId == bidUpdateData.bpop_cbidder){
                // console.log("working======>",bidUpdateData.error)
                if(bidUpdateData.status=="failed") {
                  console.log("working======>",bidUpdateData.error)
                  bidUpdateData.error && toast.warning(bidUpdateData.error);
                } else if(bidUpdateData.status=="success") {
                  bidUpdateData.bpop_error && toast.success(bidUpdateData.bpop_error);
                }
              }
              bidUpdateData.bpop_bidstatus && toast.info(bidUpdateData.bpop_bidstatus)
              setAmount('')
            }
          }
          console.log("bidUpdateData ----", bidUpdateData);
        });
      }

      // useEffect(() => {
      //   console.log("bidUpdateData ----", propertyId);
      //   bidUpdate((err, bidUpdateData) => {
      //     if(err){
      //       console.log("socket res------", err);
      //     } else {
      //       console.log("propertyId -----", propertyId);
      //       console.log("bidUpdateData -----", bidUpdateData);
      //       if(propertyId == bidUpdateData.id) {
      //         let params = {
      //           product_id : propertyId
      //         }
      //         getBidDetails(params);
      //         if(userId == bidUpdateData.bpop_cbidder){
      //           if(bidUpdateData.status=="failed") {
      //             bidUpdateData.error && toast.error(bidUpdateData.error);
      //           } else if(bidUpdateData.status=="success") {
      //             bidUpdateData.bpop_error && toast.success(bidUpdateData.bpop_error);
      //           }
      //         }
      //         bidUpdateData.bpop_bidstatus && toast.info(bidUpdateData.bpop_bidstatus)
      //         setAmount('')
      //       }
      //     }
      //     console.log("bidUpdateData ----", bidUpdateData);
      //   });
      // }, [propertyId]);

      function getBidDetails(params) {
        Axios.post(process.env.REACT_APP_API + 'getbiddetails', params)
          .then((res) => {
            if (res.data.success === 'yes') {
              setBidHisstoryList(res.data.results);
            }
          })
          .catch((error) => {
            console.log(error);
          });
      }

      const handleClick = async (nextBid, incremente, id) => {
        let bid =nextBid;
        console.log("bid ===", parseFloat(bid));
        console.log("amount ----", parseFloat(amount));
        console.log("incremente ----", incremente);
        
        if (
          !isNaN(amount) &&
          parseFloat(bid) <= parseFloat(amount)
        ) {
          let params = {
            id: id,
            wsprice: amount,
            userid: userId,
            bid_increment: increment
          };
          console.log("params ----", params);
          let auctionToken= localStorage.getItem('auctionToken');
          let header = {};
          header["authorization"] = "Bearer " + auctionToken;
          Axios.post(process.env.REACT_APP_API + 'mobileconfirmForward', params, {headers: header})
          .then((res) => {
            if (res.data.success === 'yes') {
              console.log("success bid");
              //setBidHisstoryList(res.data.results);
            }
          })
          .catch((error) => {
            console.log(error);
          });
        } else {
          alert(`Enter more than US $ ${bid}`);
        }
        setAmount("");
      };

      function handleBidSubmit () {
        console.log("handle bid submit ----------------");
      }

      const handleOnChange = (e) => {
        setAmount(e.target.value);
      }

    return (
     <main>
       <ToastContainer/>
  <section>
    <div className="container">
      <div className="single_properties_title">
        <h2>
          {propertyDetails.title}
        </h2>
      </div>
    </div>
   
    <div className="container properties_single_pg">
      {/*Carousel Wrapper*/}
      <div id="carousel-thumb" className="carousel slide carousel-fade carousel-thumbnails" data-ride="carousel">
        {/*Slides*/}
        <div className="carousel-inner" role="listbox">
          <div className="carousel-item active">
            <img className="d-block w-100" src="../images/slider-1.jpg" alt="First slide" />
            <div className="carousel_heart">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
              {/* <img src="../images/heart-uncheck.svg" alt="" id="slider1" onclick="checked()"> */}
            </div>
          </div>
          <div className="carousel-item">
            <img className="d-block w-100" src="../images/slider-1-1-new.jpg" alt="Second slide" />
            <div className="carousel_heart">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
          {/* <div class="carousel-item">
          <img class="d-block w-100" src="../images/slider-1-2.jpg" alt="Third slide">
          <div class="carousel_heart">
            <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
          </div>
        </div> */}
        </div>
        
        {/*/.Controls*/}
        {attachmentsList.length > 0 
        ? 
        <ol className="carousel-indicators owl-carousel owl-theme thum-slider" id="thum_slider">
        {attachmentsList.map((item,index)=> {
          return(
            <li data-target="#carousel-thumb" data-slide-to={0}><img className="d-block w-100" src="../images/slider-1-5.jpg" /></li>
            )
          })}
       
        </ol>
        :
        ""
        }
        
      </div>
      {/*/.Carousel Wrapper*/}
    </div>
  </section>
  <section className>
    <div className="container">
      <div className="Properties_second_Section_page">
        <div className="Properties_Bid_details_One">
          <p>Lot No.: <strong>{propertyDetails.auctionid}</strong></p>
          <p>Category : <strong>Residential</strong></p>
          <p>Sub - Category : <strong>House</strong></p>
      <p>Biding Deposit : <strong>{propertyDetails.content_head4 > 0 ? '€' + propertyDetails.content_head4 : "-"}</strong></p>
        </div>
        <div className="bid_price_enter">
          <div className="start_price_bid_properties">
            <p>
              Start Price :
            </p>
            <div className>
              
              <input
               type="text"
               class="form-control" 
               placeholder={propertyDetails.sprice > 0 ? '€' + propertyDetails.sprice : "-"}
               pattern="^\d*(.\d{0,2})?$"
               onChange={handleOnChange}
               value={amount}
                onKeyDown={(event) => {
                  if (event.key === "Enter") {
                    event.preventDefault();
                    handleClick(
                      (parseFloat(bidDetails.next_bid)),
                       bidDetails.incrementamt,
                       propertyDetails.id
                    );
                  }
                }
              } />
            </div>
            <div className="text-center">
              {timer.text=="ENDS"
              ? <button className="submit_offer_start_btn" onClick={(e)=> {e.preventDefault(); handleClick ((parseFloat(bidDetails.next_bid)),
                bidDetails.incrementamt,
                propertyDetails.id)}}>
                  Submit offer
                </button>
              : <button className="submit_offer_start_btn" onClick={handleBidSubmit} disabled={true}>
                  Submit offer
                </button>
              
              }
              
            </div>
          </div>
        </div>
        <div className="bid_Deposit_properties_single">
          <div className="start_price_bid_properties">
            <p>
              Bid Deposit : *Fully refundable for Unsuccessful bidder
            </p>
            <div className>
              <label htmlFor>
              {propertyDetails.content_head4 > 0 ? '€' + propertyDetails.content_head4 : "-"}
              </label>
            </div>
            <div className="text-center">
              <button className="submit_offer_start_btn" onclick="window.location.href='after-bid_properties.html'">
                Registration Opens on 02/04 01:00PM
              </button>
            </div>
          </div>
        </div>
        <div className="bid_timer_start_properties_single">
          <div className="bid_timer_start_properties_single_title">
            <img src="../images/clock_properties.svg" alt="clock_properties" />
            <span>BID {timer.text} IN</span>
          </div>
          <div className="bid_timer_timer" id="countdown">
            <div>

              <p id="day">
                {timer.days}
              </p>
              <span>
                DAYS
              </span>
            </div>
            <div>
              <p id="hour">
                {timer.hours}
              </p>
              <span>
                HOURS
              </span>
            </div>
            <div>
              <p id="min">
                {timer.minutes}
              </p>
              <span>
                MINUTES
              </span>
            </div>
            <div>
              <p id="sec">
                {timer.seconds}
              </p>
              <span>
                SECONDS
              </span>
            </div>
          </div>
        </div>
      </div>
      <div className="Start_lable_Property_page">
        <p>{timer.smallText} at {moment(propertyDetails.date_closed).format("LLLL")}</p>
      </div>
    </div>
  </section>
  {bidHisstoryList!=undefined && bidHisstoryList.length > 0 ? 
      <section>
      <div className="container">
        <div className="table_heading">
            <div className="table_history">
              <table>
                <tbody>
                  <tr>
                    <td>
                      Username : test
                    </td>
                    <td>
                      Date & Time : 8/6/2020 4:07 am
                    </td>
                    <td>
                      <span>Amount : €800</span>
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>
            <label>Bid History</label>
        </div>
      </div>
      </section>
  : null}
  
  <section className="container">
    <div className="social_icon_share starting_social_icon">
      <span>Share :
      </span>
      <a href="#" style={{marginRight: 27}}>
        <img src="../images/linkedin-in-brands.png" alt="alt-img"  />
      </a>
      <a href="#" style={{marginRight: 27}}>
        <img src="../images/youtube_properties.svg" alt="alt-img"  />
      </a>
      <a href="#" style={{marginRight: 27}}>
        <img src="../images/facebook-f-brands.png" alt="alt-img"  />
      </a>
      <a href="#" style={{marginRight: 27}}>
        <img src="../images/instagram-sketched.png" alt="alt-img"  />
      </a>
      <a href="#">
        <img src="../images/twitter-brands.png" alt="alt-img"  />
      </a>
    </div>
  </section>
  <section className="four_big_btn_section">
    <div className="container">
      <div className="row">
        <div className="col-lg-3 col-md-3">
          <Link to="/buyers-guide">
              <button className="btn_properti_View">
                <img src="../images/guide.svg" alt="alt-img"  />
                <span>Buyer's Guide</span>
              </button>
          </Link>
        </div>
        <div className="col-lg-3 col-md-3">
        <Link to={"/faq/"+propertyId}>
          <button className="btn_properti_View mr_58">
            <img src="../images/Outline.svg" alt="alt-img"  />
            <span>
              FAQ
            </span>
          </button>
        </Link>
          
        </div>
        <div className="col-lg-3 col-md-3">
          <button data-target="#view_properties" data-toggle="modal" className="btn_properti_View">
            <img src="../images/real-estate.svg" alt="alt-img"  />
            <span>
              View property
            </span>
          </button>
        </div>
        <div className="col-lg-3 col-md-3">
          <button onclick=" window.open('https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf','_blank')" className="btn_properti_View">
            <img src="../images/print-properties_main.svg" alt="alt-img"  />
            <span>
              Print Brochure
            </span>
          </button>
        </div>
      </div>
      {/* <div class="four_big_btn_First_btn">
    
      </div> */}
    </div>
  </section>
  
  <section className="tab_view_properties_details">
        <div className="container">
          <div className="tab_Propertie_singlesss">
            <Tab.Container
              id="left-tabs-example"
              defaultActiveKey="Description"
            >
              <div className="row">
                <div className="col-sm-3 px-0 tab_nav_propertiesss">
                  <Nav
                    className="nav flex-column nav-pills"
                    variant="pills"
                    className="flex-column"
                  >
                    <Nav.Link eventKey="Description">Description</Nav.Link>
                    <Nav.Link eventKey="Brochure">Brochure</Nav.Link>
                    <Nav.Link eventKey="Legal-Documents">
                      Legal Documents
                    </Nav.Link>

                    <Nav.Link eventKey="Map">Map</Nav.Link>
                  </Nav>
                </div>
                <div className="col-sm-9 px-0 tab_inner_Section_details_most">
                  <Tab.Content>
                    <Tab.Pane eventKey="Description">
                      <div className="tab_inner_Section_details_most">
                        <p>
                        {propertyDetails.description}
                        </p>
                        
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="Brochure">
                      <div className="tab_inner_Section_details_most">
                        <div className="tab_brouchure"> 
                          <ul>
                          {brochure.map((item, index)=>{
                            return(<li>
                              <a
                                href={item.src}
                                target="_blank"
                              >
                                {"Brouchure -"+ index}
                              </a>
                            </li>)
                              
                          })}
                          </ul>
                         </div>
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="Legal-Documents">
                    <div className="tab_inner_Section_details_most">
                        <div className="tab_brouchure"> 
                        <ul>
                          {legaldocs.map((item, index)=>{
                            return(<li>
                              <a
                                href={item.src}
                                target="_blank"
                              >
                                {"Legal Document -"+ index}
                              </a>
                            </li>)
                              
                          })}                            
                        </ul>
                        </div>
                      </div>
                    </Tab.Pane>
                    <Tab.Pane eventKey="Map">
                    {/* <GoogleMap
                      defaultZoom={8}
                      defaultCenter={{ lat: -34.397, lng: 150.644 }}
                    >
                      <Marker position={{ lat: -34.397, lng: 150.644 }} />
                   </GoogleMap> */}
                   <Maps lat={propertyDetails.custom_field_1} lng={propertyDetails.custom_field_2}/>
                    {/* <div className="tab_inner_Section_details_most"> */}
                        {/* <div className="tab_brouchure">  */}
                        {/* <iframe
                          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27050.039502475036!2d-1.5744919973476545!3d53.18362487776462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x25a3b1142c791a9%3A0xc4f8a0433288257a!2sUnited%20Kingdom!5e0!3m2!1sen!2sin!4v1593769316045!5m2!1sen!2sin"
                          width="100%"
                          height={450}
                          frameBorder={0}
                          style={{ border: 0 }}
                          allowFullScreen
                          aria-hidden="false"
                          tabIndex={0}
                        /> */}
                      {/* </div> */}
                    </Tab.Pane>
                  </Tab.Content>
                </div>
              </div>
            </Tab.Container>
          </div>
          {/* <div className="tab_Propertie_single">
            <div
              className="nav flex-column nav-pills tab_nav_properties"
              id="v-pills-tab"
              role="tablist"
              aria-orientation="vertical"
            >
              <a
                className="nav-link active"
                id="v-pills-Description-tab"
                data-toggle="pill"
                href="#v-pills-Description"
                role="tab"
                aria-controls="v-pills-Description"
                aria-selected="true"
              >
                Description
              </a>
              <a
                className="nav-link"
                id="v-pills-Brochure-tab"
                data-toggle="pill"
                href="#v-pills-Brochure"
                role="tab"
                aria-controls="v-pills-Brochure"
                aria-selected="false"
              >
                Brochure
              </a>
              <a
                className="nav-link legal_nav_link"
                id="v-pills-Legal-tab"
                data-toggle="pill"
                href="#v-pills-Legal"
                role="tab"
                aria-controls="v-pills-Legal"
                aria-selected="false"
              >
                <button
                  data-target="#legal_document_popup"
                  data-toggle="modal"
                ></button>
                Legal Documents
              </a>
              <a
                className="nav-link"
                id="v-pills-Map-tab"
                data-toggle="pill"
                href="#v-pills-Map"
                role="tab"
                aria-controls="v-pills-Map"
                aria-selected="false"
              >
                Map
              </a>
            </div>
            <div
              className="tab-content Tab_details_content"
              id="v-pills-tabContent"
            >
              <div
                className="tab-pane fade show active tab_inner_Section_details_most"
                id="v-pills-Description"
                role="tabpanel"
                aria-labelledby="v-pills-Description-tab"
              >
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Dignissim diam quis enim lobortis scelerisque. Lobortis mattis
                  aliquam faucibus purus in. Feugiat in fermentum posuere urna
                  nec tincidunt praesent semper. Ut enim blandit volutpat
                  maecenas volutpat blandit aliquam etiam erat. Dis parturient
                  montes nascetur ridiculus mus mauris.
                </p>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Dignissim diam quis enim lobortis scelerisque. Lobortis mattis
                  aliquam faucibus purus in. Feugiat in fermentum posuere urna
                  nec tincidunt praesent semper. Ut enim blandit volutpat
                  maecenas volutpat blandit aliquam etiam erat. Dis parturient
                  montes nascetur ridiculus mus mauris.
                </p>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Dignissim diam quis enim lobortis scelerisque. Lobortis mattis
                  aliquam faucibus purus in. Feugiat in fermentum posuere urna
                  nec tincidunt praesent semper. Ut enim blandit volutpat
                  maecenas volutpat blandit aliquam etiam erat. Dis parturient
                  montes nascetur ridiculus mus mauris.
                </p>
              </div>
              <div
                className="tab-pane fade tab_inner_Section_details_most"
                id="v-pills-Brochure"
                role="tabpanel"
                aria-labelledby="v-pills-Brochure-tab"
              >
                <div className="tab_brouchure">
                  <ul>
                    <li>
                      <a
                        href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                        target="_blank"
                      >
                        Brochure_one.pdf
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div
                className="tab-pane fade tab_inner_Section_details_most"
                id="v-pills-Legal"
                role="tabpanel"
                aria-labelledby="v-pills-Legal-tab"
              >
                <div className="tab_brouchure">
                  <ul>
                    <li>
                      <a
                        href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                        target="_blank"
                      >
                        Brochure_one.pdf
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                        target="_blank"
                      >
                        Brochure_one.pdf
                      </a>
                    </li>
                    <li>
                      <a
                        href="https://www.w3.org/WAI/ER/tests/xhtml/testfiles/resources/pdf/dummy.pdf"
                        target="_blank"
                      >
                        Brochure_one.pdf
                      </a>
                    </li>
                  </ul>
                </div>
              </div>
              <div
                className="tab-pane fade tab_map"
                id="v-pills-Map"
                role="tabpanel"
                aria-labelledby="v-pills-Map-tab"
              >
                <iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d27050.039502475036!2d-1.5744919973476545!3d53.18362487776462!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x25a3b1142c791a9%3A0xc4f8a0433288257a!2sUnited%20Kingdom!5e0!3m2!1sen!2sin!4v1593769316045!5m2!1sen!2sin"
                  width="100%"
                  height={450}
                  frameBorder={0}
                  style={{ border: 0 }}
                  allowFullScreen
                  aria-hidden="false"
                  tabIndex={0}
                />
              </div>
            </div>
          </div> */}
        </div>
      </section>


  <section className="similar_properties_single">
    <div className="container">
      <h2>Similar Properties</h2>
    </div>
    <div className="container">
      <div className="row">
        <div className="col-md-3 col-lg-3">
          <div className="property_item">
            <a href="Properties_item.html">
              <div className="property_item_img">
                <img src="../images/1.jpg" alt={1} className="img-fluid" />
                <div className="property-rate_in_img">
                  <span>
                    Start Price
                  </span>
                  <h6>
                    €150,000.00
                  </h6>
                </div>
                {/* <div class="similar_heart_fav">
                <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
              </div> */}
              </div>
              <div className="property_item_details">
                <h3>
                  3 Bedroom Apartment
                </h3>
                <span>
                  Limassol
                </span>
                <p>RESIDENTIAL</p>
                <div className="property_item_timing">
                  <img src="../images/timer.svg" alt="timer" className />
                  <span>2M:13D:5M:32S</span>
                </div>
              </div>
            </a>
            <div className="similar_heart_fav">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
        </div>
        <div className="col-md-3 col-lg-3">
          <div className="property_item">
            <a href="Properties_item.html">
              <div className="property_item_img">
                <img src="../images/1.jpg" alt={1} className="img-fluid" />
                <div className="property-rate_in_img">
                  <span>
                    Start Price
                  </span>
                  <h6>
                    €150,000.00
                  </h6>
                </div>
                {/* <div class="similar_heart_fav">
                <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
              </div> */}
              </div>
              <div className="property_item_details">
                <h3>
                  3 Bedroom Apartment
                </h3>
                <span>
                  Limassol
                </span>
                <p>RESIDENTIAL</p>
                <div className="property_item_timing">
                  <img src="../images/timer.svg" alt="timer" className />
                  <span>2M:13D:5M:32S</span>
                </div>
              </div>
            </a>
            <div className="similar_heart_fav">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
        </div>
        <div className="col-md-3 col-lg-3">
          <div className="property_item">
            <a href="Properties_item.html">
              <div className="property_item_img">
                <img src="../images/1.jpg" alt={1} className="img-fluid" />
                <div className="property-rate_in_img">
                  <span>
                    Start Price
                  </span>
                  <h6>
                    €150,000.00
                  </h6>
                </div>
                {/* <div class="similar_heart_fav">
                <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
              </div> */}
              </div>
              <div className="property_item_details">
                <h3>
                  3 Bedroom Apartment
                </h3>
                <span>
                  Limassol
                </span>
                <p>RESIDENTIAL</p>
                <div className="property_item_timing">
                  <img src="../images/timer.svg" alt="timer" className />
                  <span>2M:13D:5M:32S</span>
                </div>
              </div>
            </a>
            <div className="similar_heart_fav">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
        </div>
        <div className="col-md-3 col-lg-3">
          <div className="property_item">
            <a href="Properties_item.html">
              <div className="property_item_img">
                <img src="../images/1.jpg" alt={1} className="img-fluid" />
                <div className="property-rate_in_img">
                  <span>
                    Start Price
                  </span>
                  <h6>
                    €150,000.00
                  </h6>
                </div>
                {/* <div class="similar_heart_fav">
                <img src="../images/heart-uncheck.svg" alt=""  onclick="this.src='../images/active-hart.svg'">
              </div> */}
              </div>
              <div className="property_item_details">
                <h3>
                  3 Bedroom Apartment
                </h3>
                <span>
                  Limassol
                </span>
                <p>RESIDENTIAL</p>
                <div className="property_item_timing">
                  <img src="../images/timer.svg" alt="timer" className />
                  <span>2M:13D:5M:32S</span>
                </div>
              </div>
            </a>
            <div className="similar_heart_fav">
              <img src="../images/heart-uncheck.svg" alt onclick="this.src='../images/active-hart.svg'" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>

    )
}
