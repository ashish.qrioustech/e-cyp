import React from 'react';
import { Dropdown } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import './CustomeDropdown.css';
import PropTypes from 'prop-types';
import Swal from 'sweetalert2';

export default function CustomeDropdown({
  data,
  className,
  isDisabled,
  inputClassName,
  dropTitleName,
}) {
  const dataaa = (data) => {
    if (data === 1) {
      localStorage.removeItem('language');
      localStorage.setItem('language', 'English');
      window.location.reload();
      Swal.fire({
        title: 'Please Wait Converting Language',
        timer: 3000,
        showConfirmButton: false,
        position: 'center',
      });
    } else if (data === 2) {
      localStorage.removeItem('language');
      localStorage.setItem('language', 'Russian');
      window.location.reload();
      Swal.fire({
        title: 'Please Wait Converting Language',
        timer: 3000,
        showConfirmButton: false,
        position: 'center',
      });
    } else if (data === 3) {
      localStorage.removeItem('language');
      localStorage.setItem('language', 'Greek');
      window.location.reload();
      Swal.fire({
        title: 'Please Wait Converting Language',
        timer: 3000,
        showConfirmButton: false,
        position: 'center',
      });
    } else if (data === 4) {
      localStorage.removeItem('language');
      localStorage.setItem('language', 'Spanish');
      window.location.reload();
      Swal.fire({
        title: 'Please Wait Converting Language',
        timer: 3000,
        showConfirmButton: false,
        position: 'center',
      });
    } else {
      localStorage.removeItem('language');
      localStorage.setItem('language', 'Chinese');
      window.location.reload();
      Swal.fire({
        title: 'Please Wait Converting Language',
        timer: 3000,
        showConfirmButton: false,
        position: 'center',
      });
    }
  };

  return (
    <div className="custome-dropdown-select">
      <Dropdown className={className}>
        <Dropdown.Toggle
          className={inputClassName}
          style={{ background: 'transparent', border: '0px' }}
          disabled={isDisabled}
        >
          {dropTitleName}
        </Dropdown.Toggle>

        <Dropdown.Menu>
          {data.map((x) => {
            return (
              <Link
                key={x.id}
                className="dropdown-item"
                to={x.path}
                onClick={(e) => {
                  e.preventDefault();
                  dataaa(x.id);
                }}
              >
                {x.value}
              </Link>
            );
          })}
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}

CustomeDropdown.defaultProps = {
  className: '',

  isDisabled: false,
  data: [],
  inputClassName: '',
  dropTitleName: 'Drop',
};
CustomeDropdown.propTypes = {
  data: PropTypes.instanceOf(Array),

  className: PropTypes.string,

  inputClassName: PropTypes.string,
  isDisabled: PropTypes.bool,
  dropTitleName: PropTypes.string,
};
