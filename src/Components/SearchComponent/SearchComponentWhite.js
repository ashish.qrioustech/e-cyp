import React, { useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';

import DropdownComponent from '../Dropdown/DropdownComponent';
import fetchClient from '../../util/axiosConfig';
import { Slider } from '@material-ui/core';
import { useHistory } from 'react-router';
import {
  getgetDistrictSelecter,
  getCitySelector,
  getselectionSelector,
  getTypeSelector,
  getSubTypeSelector,
  getCatagorySelector,
} from '../../selector/selector';
import {
  getSearchDropdownListApiAction,
  setSelectionListAction,
} from '../../actions/searchAction';

function SearchComponentWhite({
  callSearchDropApi,
  setSelection,
  getDistrict,
  getselection,
  getCity,
  getType,
  getSubType,
  getCatagory,
}) {
  const history = useHistory();
  const content = useRef(null);
  const [slider, setSlider] = useState([0, 100]);

  const [setActive, setActiveState] = useState('');
  const [setHeight, setHeightState] = useState('0px');
  const [dropData] = useState([
    { id: 1, value: 'Any' },
    { id: 2, value: 'Choose Area / Village' },
    { id: 3, value: 'Choose Sub-Type' },
    { id: 4, value: 'Choose Category' },
    { id: 5, value: 'Choose Neighbourhood' },
    { id: 6, value: 'Choose Remove listings' },
  ]);
  useEffect(() => {
    callSearchDropApi();
    // setSelection()
  }, []);
  const Req = {
    fromprice: '0',
    toprice: '50000000',
  };
  const handleSearchProperty = () => {
    fetchClient
      .post('mobilesearch', {
        ...Req,
      })
      .then((res) => {
        if (res.data.success === 'yes') {
          history.push({ pathname: '/search-result', data: res.data.results });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleSelectDistrict = (id) => {
    let data = { ...getselection };
    data = { ...data, state: id };
    setSelection(data);
  };
  const handleSelectCity = (id) => {
    let data = { ...getselection };
    data = { ...data, city: id };
    setSelection(data);
  };
  const handleSelectType = (id) => {
    let data = { ...getselection };
    data = { ...data, category: id };
    setSelection(data);
  };
  const handleSelectSubType = (id) => {
    let data = { ...getselection };
    data = { ...data, subcategory: id };
    setSelection(data);
  };

  const handleSelectCategory = (id) => {
    let data = { ...getselection };
    data = { ...data, contenthead1: id };
    setSelection(data);
  };
  const handleRange = (value) => {
    let data = { ...getselection };

    data = { ...data, fromprice: value[0] * 10000, toprice: value[1] * 10000 };

    setSelection(data);
  };

  function toggleAccordion() {
    setActiveState(setActive === '' ? 'active' : '');
    setHeightState(
      setActive === 'active'
        ? '0px'
        : `${content.current && content.current.scrollHeight}px`
    );
  }
  return (
    <section className="Second-Page_Discover">
      <div className="container search_container second_page_discover_info">
        <h3>Discover your perfect property in Cyprus</h3>
        <div className="search_dp_section">
          <div className="row">
            <div className="col-md-3 custome-padding-white">
              <DropdownComponent
                label={'DISTRICT'}
                value={getselection.state.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown btn-Search_drop_D'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Any"
                isDisabled={false}
                data={getDistrict}
                onSelect={(index) => {
                  handleSelectDistrict(
                    getDistrict.filter((x) => {
                      return x.location_id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3 custome-padding-white">
              <DropdownComponent
                label="AREA / VILLAGE"
                value={getselection.city.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown btn-Search_drop_D'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Any"
                isDisabled={false}
                data={getCity}
                onSelect={(index) => {
                  handleSelectCity(
                    getCity.filter((x) => {
                      return x.id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3 custome-padding-white">
              <DropdownComponent
                label="TYPE"
                value={getselection.category.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown btn-Search_drop_D'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Any"
                isDisabled={false}
                data={getType}
                onSelect={(index) => {
                  handleSelectType(
                    getType.filter((x) => {
                      return x.id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3 custome-padding-white">
              <DropdownComponent
                label="SUB TYPE"
                value={getselection.subcategory.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown btn-Search_drop_D'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Any"
                isDisabled={false}
                data={getSubType}
                onSelect={(index) => {
                  handleSelectSubType(
                    getSubType.filter((x) => {
                      return x.id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3 custome-padding-white">
              <DropdownComponent
                label="CATEGORY"
                value={getselection.contenthead1.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown btn-Search_drop_D'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Any"
                isDisabled={false}
                data={getCatagory}
                onSelect={(index) => {
                  handleSelectCategory(
                    getCatagory.filter((x) => {
                      return x.id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3 custome-padding-white">
              <label>PRICE</label>
              <div className="slider-range_Price_section">
                <span>Price €0 - €10,000,000+</span>
              </div>
              <Slider
                className="slider-range__Price"
                value={slider}
                onChange={(e, id) => {
                  setSlider(id);
                  handleRange(id);
                }}
                // valueLabelDisplay="auto"
                aria-labelledby="range-slider"
                // getAriaValueText={handleRange}
              />
              {/* <div className="slider-range__Price">
                <div id="slider-range" />
              </div> */}
            </div>
            <div className="col-md-3 custome-padding-white">
              <label>LOT NUMBER</label>
              <input
                type="text"
                name="LOTNo"
                id="LOTNo"
                style={{ width: '100%', color: '#595959' }}
                placeholder="Enter Lot Number"
              />
            </div>
            <div className="col-md-3 custome-padding-white">
              <label>&nbsp;</label>
              <br />
              <button className="searc_sc_btn">
                <img src="../images/search.svg" alt="alt-img"  />
                Search
              </button>
            </div>
          </div>
        </div>
        <div className="rest_show_search_section" style={{ paddingBottom: 15 }}>
          <a href="#">Reset</a>
          <a id="advance-search" onClick={toggleAccordion}>
            {/* <Accordion.Toggle as={Button} variant="link" eventKey="0"> onClick={() => setOpen(!open)}*/}
            {setActive === '' ? 'Advance Search' : 'Hide'}
            {/* </Accordion.Toggle> */}
          </a>
        </div>
        <div
          ref={content}
          style={{
            maxHeight: `${setHeight}`,
            overflow: `${setActive === '' ? 'hidden' : ''}`,
          }}
          className="search_dp_section"
        >
          {/* <div
            className={`search_dp_section accordion__content`}
            style={{ maxHeight: `${open ? '1000px' : '-10px'}` }}
          > */}

          <div id="example-fade-text">
            <div className="row">
              <div className="col-md-3">
                <DropdownComponent
                  label={'NEIGHBOURHOOD'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Neighbourhood"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <label>LAND</label>
                <div className="slider-range_Price_section">
                  <span>
                    m<small>2</small> 0 - 59000
                  </span>
                </div>
                <div className="slider-range__Price">
                  <div id="land" />
                </div>
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'BEDROOMS'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Any"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'NEW / USED'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Sub-Type"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'REMOVE LISTINGS'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Remove listings"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <label>COVERED AREA</label>
                <div className="slider-range_Price_section">
                  <span>
                    m<small>2</small> 0 - 59000
                  </span>
                </div>
                <div className="slider-range__Price">
                  <div id="coverd-area" />
                </div>
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'BEDROOMS'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Any"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'NEW / USED'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Sub-Type"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

SearchComponentWhite.defaultProps = {
  getDistrict: [],
  getCity: [],
  getselection: {},
  getType: [],
  // getSubType: [],
  getCatagory: [],
};
SearchComponentWhite.propTypes = {
  callSearchDropApi: PropTypes.func.isRequired,
  setSelection: PropTypes.func.isRequired,
  getDistrict: PropTypes.instanceOf(Array),
  getCity: PropTypes.instanceOf(Array),
  getselection: PropTypes.instanceOf(Object),
  getType: PropTypes.instanceOf(Array),
  getSubType: PropTypes.instanceOf(Array),
  getCatagory: PropTypes.instanceOf(Array),
};
const mapStateToProps = (state) => ({
  getDistrict: getgetDistrictSelecter(state),
  getCity: getCitySelector(state),
  getselection: getselectionSelector(state),
  getType: getTypeSelector(state),

  getSubType: getSubTypeSelector(state),
  getCatagory: getCatagorySelector(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      callSearchDropApi: getSearchDropdownListApiAction,
      setSelection: setSelectionListAction,
    },
    dispatch
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SearchComponentWhite);
