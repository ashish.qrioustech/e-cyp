import React, { useState, useRef, useEffect } from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { Slider } from '@material-ui/core';

import DropdownComponent from '../Dropdown/DropdownComponent';
import fetchClient from '../../util/axiosConfig';
import { useHistory } from 'react-router';
import {
  getgetDistrictSelecter,
  getCitySelector,
  getselectionSelector,
  getTypeSelector,
  getSubTypeSelector,
  getCatagorySelector,
} from '../../selector/selector';
import {
  getSearchDropdownListApiAction,
  setSelectionListAction,
} from '../../actions/searchAction';
function SearchComponent({
  callSearchDropApi,
  setSelection,
  getDistrict,
  getselection,
  getCity,
  getType,
  getSubType,
  getCatagory,
}) {
  const history = useHistory();
  const content = useRef(null);
  const [slider, setSlider] = useState([0, 100]);

  const [setActive, setActiveState] = useState('');
  const [setHeight, setHeightState] = useState('0px');
  const [dropData] = useState([
    { id: 1, value: 'Any' },
    { id: 2, value: 'Choose Area / Village' },
    { id: 3, value: 'Choose Sub-Type' },
    { id: 4, value: 'Choose Category' },
    { id: 5, value: 'Choose Neighbourhood' },
    { id: 6, value: 'Choose Remove listings' },
  ]);
  useEffect(() => {
    callSearchDropApi();
    // setSelection()
  }, []);
  const Req = {
    fromprice: '0',
    toprice: '50000000',
  };
  const handleSearchProperty = () => {
    fetchClient
      .post('mobilesearch', {
        ...Req,
      })
      .then((res) => {
        if (res.data.success === 'yes') {
          history.push({ pathname: '/search-result', data: res.data.results });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const handleSelectDistrict = (id) => {
    let data = { ...getselection };
    data = { ...data, state: id };
    setSelection(data);
  };
  const handleSelectCity = (id) => {
    let data = { ...getselection };
    data = { ...data, city: id };
    setSelection(data);
  };
  const handleSelectType = (id) => {
    let data = { ...getselection };
    data = { ...data, category: id };
    setSelection(data);
  };
  const handleSelectSubType = (id) => {
    let data = { ...getselection };
    data = { ...data, subcategory: id };
    setSelection(data);
  };

  const handleSelectCategory = (id) => {
    let data = { ...getselection };
    data = { ...data, contenthead1: id };
    setSelection(data);
  };
  const handleRange = (value) => {
    let data = { ...getselection };

    data = { ...data, fromprice: value[0] * 10000, toprice: value[1] * 10000 };

    setSelection(data);
  };
  function toggleAccordion() {
    setActiveState(setActive === '' ? 'active' : '');
    setHeightState(
      setActive === 'active'
        ? '0px'
        : `${content.current && content.current.scrollHeight}px`
    );
  }
  return (
    <section className="search_Main_pg" style={{ marginTop: '-10px' }}>
      {/* <Accordion defaultActiveKey="0"> */}
      {/* {console.log('getDistrict', getDistrict)} */}
      {/* {  console.log(getCity)} */}
      <div className="container search_container">
        <h3>Discover your perfect property in Cyprus</h3>
        <div className="search_dp_section">
          <div className="row">
            <div className="col-md-3">
              <DropdownComponent
                label={'DISTRICT'}
                value={getselection.state.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Any"
                isDisabled={false}
                data={getDistrict}
                onSelect={(index) => {
                  handleSelectDistrict(
                    getDistrict.filter((x) => {
                      return x.location_id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3">
              <DropdownComponent
                label={'AREA / VILLAGE'}
                value={getselection.city.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Choose Area / Village"
                isDisabled={false}
                data={getCity}
                onSelect={(index) => {
                  handleSelectCity(
                    getCity.filter((x) => {
                      return x.id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3">
              <DropdownComponent
                label={'TYPE'}
                value={getselection.category.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Any"
                isDisabled={false}
                data={getType}
                onSelect={(index) => {
                  handleSelectType(
                    getType.filter((x) => {
                      return x.id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3">
              <DropdownComponent
                label={'SUB TYPE'}
                value={getselection.subcategory.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Choose Sub-Type"
                isDisabled={false}
                data={getSubType}
                onSelect={(index) => {
                  handleSelectSubType(
                    getSubType.filter((x) => {
                      return x.id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3">
              <DropdownComponent
                label={'CATEGORY'}
                value={getselection.contenthead1.value}
                drop={'down'}
                filterDropdown={false}
                className={'dropdown'}
                inputClassName="btn-Search_drop_D"
                labelClassName={''}
                noOptionsMessage={''}
                error={''}
                defaultDropName="Choose Category"
                isDisabled={false}
                data={getCatagory}
                onSelect={(index) => {
                  handleSelectCategory(
                    getCatagory.filter((x) => {
                      return x.id === Number(index);
                    })[0]
                  );
                }}
              />
            </div>
            <div className="col-md-3">
              <label>PRICE</label>

              <div className="slider-range_Price_section">
                <span>
                  Price €{getselection.fromprice} - €{getselection.toprice}{' '}
                  {slider[1] === 100 ? '+' : ''}
                </span>
              </div>
              <Slider
                className="slider-range__Price"
                value={slider}
                onChange={(e, id) => {
                  setSlider(id);
                  handleRange(id);
                }}
                // valueLabelDisplay="auto"
                aria-labelledby="range-slider"
                // getAriaValueText={handleRange}
              />
              {/* <div className="slider-range__Price">
                <div id="slider-range" />
              </div> */}
            </div>
            <div className="col-md-3">
              <label>LOT NUMBER</label>
              <input
                type="text"
                name="LOTNo"
                id="LOTNo"
                style={{ width: '100%' }}
                placeholder="Enter Lot Number"
              />
            </div>
            <div className="col-md-3">
              <label>&nbsp;</label>
              <br />
              <button className="searc_sc_btn" onClick={handleSearchProperty}>
                <img src="images/search.svg" alt="img" />
                Search
              </button>
            </div>
          </div>
        </div>
        <div className="rest_show_search_section">
          <a href="#">Reset</a>

          <a id="advance-search" onClick={toggleAccordion}>
            {setActive === '' ? 'Advance Search' : 'Hide'}
          </a>
        </div>
        <div
          ref={content}
          style={{
            maxHeight: `${setHeight}`,
            overflow: `${setActive === '' ? 'hidden' : ''}`,
          }}
          className="search_dp_section"
        >
          <div id="example-fade-text">
            <div className="row">
              <div className="col-md-3">
                <DropdownComponent
                  label={'NEIGHBOURHOOD'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Neighbourhood"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <label>LAND</label>
                <div className="slider-range_Price_section">
                  <span>
                    m<small>2</small> 0 - 59000
                  </span>
                </div>
                <div className="slider-range__Price">
                  <div id="land" />
                </div>
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'BEDROOMS'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Any"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'NEW / USED'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Sub-Type"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'REMOVE LISTINGS'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Remove listings"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <label>COVERED AREA</label>
                <div className="slider-range_Price_section">
                  <span>
                    m<small>2</small> 0 - 59000
                  </span>
                </div>
                <div className="slider-range__Price">
                  <div id="coverd-area" />
                </div>
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'BEDROOMS'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Any"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
              <div className="col-md-3">
                <DropdownComponent
                  label={'NEW / USED'}
                  value={''}
                  drop={'down'}
                  filterDropdown={false}
                  className={'dropdown'}
                  inputClassName="btn-Search_drop_D"
                  labelClassName={''}
                  noOptionsMessage={''}
                  error={''}
                  defaultDropName="Choose Sub-Type"
                  isDisabled={false}
                  data={dropData}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container">
        <div className="social-icon-search-sc">
          <a href="https://twitter.com/CyprusAuctions" target="_blank">
            <img src="images/linkedin.svg" alt="img" />
          </a>
          <a href="https://www.facebook.com/CyprusAuctions/" target="_blank">
            <img src="images/facebook.svg" alt="img" />
          </a>
          <a
            href="https://www.linkedin.com/in/cyprus-auctions-7474071b0/"
            target="_blank"
          >
            <img src="images/twitter.svg" alt="img" />
          </a>
        </div>
      </div>
      <div className="after_search_shap">
        <img src="images/search-bottom.svg" alt="img" className="img-fluid" />
      </div>
      {/* </Accordion> */}
    </section>
  );
}

SearchComponent.defaultProps = {
  getDistrict: [],
  getCity: [],
  getselection: {},
  getType: [],
  // getSubType: [],
  getCatagory: [],
};
SearchComponent.propTypes = {
  callSearchDropApi: PropTypes.func.isRequired,
  setSelection: PropTypes.func.isRequired,
  getDistrict: PropTypes.instanceOf(Array),
  getCity: PropTypes.instanceOf(Array),
  getselection: PropTypes.instanceOf(Object),
  getType: PropTypes.instanceOf(Array),
  getSubType: PropTypes.instanceOf(Array),
  getCatagory: PropTypes.instanceOf(Array),
};
const mapStateToProps = (state) => ({
  getDistrict: getgetDistrictSelecter(state),
  getCity: getCitySelector(state),
  getselection: getselectionSelector(state),
  getType: getTypeSelector(state),

  getSubType: getSubTypeSelector(state),
  getCatagory: getCatagorySelector(state),
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      callSearchDropApi: getSearchDropdownListApiAction,
      setSelection: setSelectionListAction,
    },
    dispatch
  );

export default connect(mapStateToProps, mapDispatchToProps)(SearchComponent);
