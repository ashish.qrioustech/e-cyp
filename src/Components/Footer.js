import React, { useState } from 'react';
//import axios from 'axios';
import jsonp from 'jsonp';
import Swal from 'sweetalert2';
import queryString from 'query-string';
import { getGlobalLangSelecter } from '../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import MessageIcon from '../images/message-icon.svg';
import SubscribeIcon from '../images/subscribe-icon.svg';
import ECYPRUSAUCTIONS from '../images/logo-E-CYPRUSAUCTIONS-150x200mm.svg';
import LinkedinIcon from '../images/linkedin-in-brands.svg';
import YoutubeIcon from '../images/youtube.svg';
import FacebookIcon from '../images/facebook-f-brands.svg';
import InstagramIcon from '../images/instagram-sketched.svg';
import TwitterIcon from '../images/twitter-brands.svg';
import { Link } from 'react-router-dom';

function Footer(props) {
  const [subEmail, setemail] = useState('');

  const submitemail = (e) => {
    e.preventDefault();
    if (!/\S+@\S+\.\S+/.test(subEmail)) {
      return Swal.fire({
        title: 'Please enter a valid email!',
        icon: 'error',
        showCancelButton: false,
        showConfirmButton: false,
      });
    }
    const url =
      '//auctionsoftware.us10.list-manage.com/subscribe/post-json?u=dbcd94d4828a302cbffc75f62&id=819cffb305';
    const data = {
      EMAIL: subEmail,
    };
    jsonp(
      `${url}&${queryString.stringify(data)}`,
      { param: 'c' },
      (err, data) => {
        if (err) {
          return console.log(err);
        }
        if (data.result === 'success') {
          return Swal.fire({
            title: data.msg,
            icon: 'success',
            showCancelButton: false,
            showConfirmButton: false,
          });
        }
        if (data.result === 'error') {
          return Swal.fire({
            title: data.msg.split('<')[0],
            icon: 'warning',
            showCancelButton: false,
            showConfirmButton: false,
          });
        }
      }
    );
    setemail('');
  };

  return (
    <footer>
      <section className="messages-icon">
        <div className="container">
          <div className="d-flex justify-content-end">
            <a href="#">
              <img src={MessageIcon} alt="message" className="img-fluid" />
            </a>
          </div>
        </div>
      </section>
      <section className="ft_subscribe">
        <div className="container">
          <div className="subscribe-row">
            <div className="subscribe-head-img">
              <img
                src={SubscribeIcon}
                alt="mess-gallery-link"
                className="img-fluid"
              />
              <p>{props.Language.subs}</p>
            </div>
            <div className="subscribe-form">
              <form action>
                <input
                  type="email"
                  name="subEmail"
                  id="subEmail"
                  placeholder="Enter your email"
                  value={subEmail}
                  onChange={(e) => {
                    setemail(e.target.value);
                  }}
                />
                <button onClick={submitemail}>
                  {props.Language.subscribe}
                </button>
              </form>
            </div>
          </div>
        </div>
      </section>
      <section className="ft_footer_main">
        <div className="container">
          <div className="row">
            <div className="col-lg-5 col-md-9">
              <div className="company_details_fb">
                <div>
                  <img
                    src={ECYPRUSAUCTIONS}
                    alt="logo"
                    className="img-fluid footer-logo"
                  />
                </div>
                <div className="company_address_Details">
                  <h3>e-Cyprusauctions.com</h3>
                  <p>
                    Tel: <a href="tel:80000808"> 80000808</a>
                  </p>
                  <p>
                    Tel: <a href="tel:+35725760781">+35725760781</a>
                  </p>
                  <p>
                    Fax: <a href="fax:+35725760604">+35725760604</a>
                  </p>
                  <p>
                    <a href="mailto:info@e-cyprusauctions.com">
                      info@e-cyprusauctions.com
                    </a>
                  </p>
                </div>
              </div>
            </div>
            <div className="col-lg-2 col-md-3">
              <h4>{props.Language.lin}</h4>
              <p>
                <Link to="/auctions">{props.Language.auctions}</Link>
              </p>
              <p>
                <Link to="/how-it-works">{props.Language.h_t_b_sm}</Link>
              </p>
              <p>
                <Link to="/about-us">{props.Language.abt_us_sm}</Link>
              </p>
              <p>
                <Link to="/contact">{props.Language.contact_us}</Link>
              </p>
              {/* <p><a href="news_blog.html">News & Blog</a></p> */}
            </div>
            <div className="col-lg-2 col-md-9 tab-pad-2">
              <h4>{props.Language.leg}</h4>
              <p>
                <Link to="/trems-condition">
                  {props.Language.terms_condtion}
                </Link>
              </p>
              <p>
                <Link to="/privacy-policy">{props.Language.p_pl}</Link>
              </p>
            </div>
            <div className="col-lg-3 col-md-3 tab-pad-2">
              <h4>{props.Language.foll_u_on}</h4>
              <div className="social-icon-ft">
                <a
                  href="https://www.linkedin.com/in/cyprus-auctions-7474071b0/"
                  target="_blank"
                >
                  <img src={LinkedinIcon} alt="temp" />
                </a>
                <a href="#" target="_blank">
                  <img src={YoutubeIcon} alt="temp" />
                </a>
                <a
                  href="https://www.facebook.com/CyprusAuctions/"
                  target="_blank"
                >
                  <img src={FacebookIcon} alt="temp" />
                </a>
                <a
                  href="https://www.instagram.com/cyprusauctions/"
                  target="_blank"
                >
                  <img src={InstagramIcon} alt="temp" />
                </a>
                <a href="#" target="_blank">
                  <img src={TwitterIcon} alt="temp" />
                </a>
              </div>
            </div>
          </div>
          <div className="content-ft-main">
            <span className="text-center">
              <strong>
                {props.Language.fo_x}, {props.Language.a_lic}.{' '}
                {props.Language.fo_x_smr} &amp; {props.Language.lic_a} -
                R.N.488,L.N.344/E.
              </strong>
            </span>
          </div>
        </div>
        <section className="ft_copy_right">
          <span>{props.Language.a_r_r} @ 2020</span>
        </section>
      </section>
    </footer>
  );
}

const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
