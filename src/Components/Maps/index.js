import React, { Component, Fragment } from "react";

import {
    withScriptjs,
    withGoogleMap,
    GoogleMap,
    Marker
} from "react-google-maps";


class Map extends Component{

    static defaultProps = {
        googleMapURL: "https://maps.googleapis.com/maps/api/js?key=AIzaSySEDDFRGFQSw2OjzDEE1-tDsN7vw&v=3.exp&libraries=geometry,drawing,places",
    }

    constructor(props) {
        super(props);
    }



    CMap = withScriptjs(withGoogleMap(props =>
        <GoogleMap
          defaultZoom={8}
          defaultCenter={props.center}//{{ lat: props.lat ? Number(props.lat) : 0.00, lng: props.lng ? Number(props.lng) : 0.00 }}
        >
            {props.children}
        </GoogleMap>
      ));



    render() {
        return (
            <Fragment>
                <this.CMap
                    googleMapURL={this.props.googleMapURL}
                    loadingElement={<div style={{ height: `100%` }} />}
                    containerElement={<div style={{ height: `700px` }} />}
                    mapElement={<div style={{ height: `100%` }} />}
                    center= {{ lat: this.props.lat ? Number(this.props.lat) : 0.00, lng: this.props.lng ? Number(this.props.lng) : 0.00 }} 
                >
                    <Marker
                        position={{ lat: this.props.lat ? Number(this.props.lat) : 0.00, lng: this.props.lng ? Number(this.props.lng) : 0.00 }} 
                    />
                </this.CMap>
            </Fragment>
        );
    }
}


export default Map;