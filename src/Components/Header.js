import React, { useEffect } from 'react';
// import PropertiesShortList from '../Pages/All/PropertiesShortList';
import { useHistory, Link } from 'react-router-dom';
import {
  getAuctionToken,
  storeToken,
  storeTempTokenOTP,
  storeTimeSpand,
} from '../util/storeData';
import CustomeDropdown from './CustomeDropdown/CustomeDropdown';
import callheadertp from '../images/call-header-tp.svg';
import mailheadertp from '../images/mail-header-tp.svg';
import ECYPRUSAUCTIONS from '../images/logo-E-CYPRUSAUCTIONS-150x200mm.svg';
import { getGlobalLangSelecter, getIsLoginStatus } from '../selector/selector';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

// import { Dropdown } from 'react-bootstrap';

function Header(props) {
  const newBlock = [
    { id: 1, value: 'Menu', path: '/menu' },
    { id: 2, value: 'Menu_one', path: '/menu-one' },
    { id: 3, value: 'Menu_two', path: '/menu-two' },
  ];
  const lang = [
    { id: 1, value: 'English', path: '/' },
    { id: 2, value: 'Russian', path: '/' },
    { id: 3, value: 'Greek', path: '/' },
    { id: 4, value: 'Spanish', path: '/' },
    { id: 5, value: 'Chinese', path: '/' },
  ];
  const history = useHistory();
  // const tokenAuth = getAuctionToken();
  useEffect(() => {}, );
  return (
    <header className>
      <section className="top-header">
        <div className="container top_header_row">
          <div className="mob-email-number">
            <div className="tp_header_email">
              <img src={mailheadertp} alt="mail" />
              <a target="_blank" href="mailto:info@e-cyprusauctions.com">
                info@e-cyprusauctions.com
              </a>
            </div>
            <div className="tp_header_email">
              <img src={callheadertp} alt="call" />
              <a target="_blank" href="tel:80000808">
                80000808
              </a>
            </div>
            <div className="tp_header_email">
              <img src={callheadertp} alt="call" />
              <a target="_blank" href="tel:+35725760781">
                +35725760781
              </a>
            </div>
            <div className="tp_header_email">
              <img src={callheadertp} alt="call" />
              <a target="_blank" href="fax:+35725760604">
                +35725760604
              </a>
            </div>
          </div>
          <div className="mob-email-number-mob-view">
            <div className="tp_header_email">
              <img src={mailheadertp} alt="mail" />
              <a target="_blank" href="mailto:info@e-cyprusauctions.com">
                info@e-cyprusauctions.com
              </a>
            </div>
            <div className="tp_header_email">
              <img src={callheadertp} alt="call" />
              <a target="_blank" href="tel:80000808">
                80000808
              </a>
            </div>
            <div className="tp_header_email">
              <img src={callheadertp} alt="call" />
              <a target="_blank" href="tel:+35725760781">
                +35725760781
              </a>
            </div>
            <div className="tp_header_email">
              <img src={callheadertp} alt="call" />
              <a target="_blank" href="fax:+35725760604">
                +35725760604
              </a>
            </div>
          </div>
          <div className="mob-login_btn">
            {props.isLogin ? (
              <div className="tp_header_sign_btn">
                <button
                  onClick={() => {
                    storeToken();
                    storeTempTokenOTP();
                    storeTimeSpand();
                    history.push('/login');
                    getAuctionToken();
                  }}
                >
                  {props.Language.logout}
                </button>
              </div>
            ) : (
              <>
                <div className="tp_header_login_btn">
                  <button onClick={() => history.push('/login')}>
                    {props.Language.l_g}
                  </button>
                </div>
                <div className="tp_header_sign_btn">
                  <button onClick={() => history.push('/sign-up')}>
                    {props.Language.signup_s}
                  </button>
                </div>
              </>
            )}
            <div className="property_short_list_header">
              <button
                onClick={() => history.push('/properties-shortlist-grid')}
              >
                {props.Language.profile_shor}
              </button>
            </div>
            {getAuctionToken() ? (
              <div className="tp_header_sign_btn">
                <button onClick={() => history.push('/profile')}>
                  {/* Properties Shortlist */} {props.Language.prof}
                </button>
              </div>
            ) : (
              <></>
            )}

            {/* <div className="tp_header_login_btn">
          <button onclick="window.location.href='login.html'">Login</button>
        </div>
        <div className="tp_header_sign_btn">
          <button onclick="window.location.href='registration.html'">Sign up</button>
        </div>
        <div className="property_short_list_header">
          <button onclick="window.location.href='properties-shortlist-grid.html'">Properties Shortlist</button>
        </div> */}
          </div>
        </div>
      </section>
      <section className="main-header home_pg_nav all_page_nav">
        <div className="container">
          <div className="desk-top-menu">
            <nav className="navbar navbar-expand-lg navbar-light e-cyprus-main-menu">
              <Link
                className="navbar-brand logo-main-logo"
                to="/"
                style={{ color: '#fff' }}
              >
                <div className="main-logo-costome">
                  <img
                    src={ECYPRUSAUCTIONS}
                    alt
                    className="img-fluid header-logo"
                  />
                  {/* <span>e-Cyprusauctions.com</span> */}
                </div>
              </Link>
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon" />
              </button>
              <div
                className="collapse navbar-collapse"
                id="navbarSupportedContent"
              >
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link className="nav-link" to="/auctions">
                      {' '}
                      {props.Language.auc}
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/how-it-works" className="nav-link">
                      {' '}
                      {props.Language.ho_t}
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link to="/about-us" className="nav-link">
                      {props.Language.abt_u}
                    </Link>
                  </li>

                  <li className="nav-item">
                    <Link to="/contact" className="nav-link">
                      {props.Language.con}
                    </Link>
                  </li>
                  <li className="nav-item">
                    <CustomeDropdown
                      data={lang}
                      className="mt-1"
                      isDisabled={false}
                      inputClassName="btn btn-language_main_nav border border-white"
                      dropTitleName="Language"
                    />
                    {/* <div className="dropdown">
                      <button
                        className="btn btn-language_main_nav dropdown-toggle"
                        type="button"
                        id="dropdownMenuButton"
                        data-toggle="dropdown"
                        aria-haspopup="true"
                        aria-expanded="false"
                      >
                        English
                      </button>
                      <div
                        className="dropdown-menu"
                        aria-labelledby="dropdownMenuButton"
                      >
                        <a className="dropdown-item" href="#">
                          English
                        </a>
                        <a className="dropdown-item" href="#">
                          Russian
                        </a>
                        <a className="dropdown-item" href="#">
                          Greek
                        </a>
                        <a className="dropdown-item" href="#">
                          Chinese
                        </a>
                      </div>
                    </div>
                  */}
                  </li>
                </ul>
              </div>
            </nav>
          </div>
          <div className="mobile-nav">
            <a className="navbar-brand" href="index.html">
              <div className="mob-main-logo-costome">
                <img
                  src={ECYPRUSAUCTIONS}
                  alt
                  className="img-fluid header-logo"
                />
                <span>
                  {props.Language.e_cyprus}.{props.Language.com}
                </span>
              </div>
            </a>
            <a
              style={{ fontSize: 30, cursor: 'pointer' }}
              href="#my-menu"
              className="toggle-btn"
            >
              ☰{/* &#x2630; */}
            </a>
            <nav id="my-menu">
              <ul>
                <li>
                  {' '}
                  {props.Language.au_h_i}
                  <Link className="nav-link" to="/auctions">
                    {' '}
                    {props.Language.auc}
                  </Link>
                </li>
                <li className="Selected">
                  <Link to="/how-it-works">{props.Language.ho_t}</Link>
                </li>
                <li>
                  <Link to="/about-us">{props.Language.abt_u}</Link>
                </li>
                {/* <li><a href="">BLOG</a></li> */}
                <li>
                  <Link to="/contact">{props.Language.con}</Link>
                </li>
                <li>
                  {/* <CustomeDropdown
                      data={lang}
                      className="mt-1"
                      isDisabled={false}
                      inputClassName="btn btn-language_main_nav border border-white"
                      dropTitleName="English"
                    /> */}
                  <span>ENGLISH</span>
                  <ul>
                    <li>
                      <a href="#">ENGLISH</a>
                    </li>
                    <li>
                      <a href="#">RUSSIAN</a>
                    </li>
                    <li>
                      <a href="#">GREEK</a>
                    </li>
                    <li>
                      <a href="#">CHINESE</a>
                    </li>
                  </ul>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      </section>
    </header>
  );
}
const mapStateToProps = (state) => ({
  Language: getGlobalLangSelecter(state),
  isLogin: getIsLoginStatus(),
});

const mapDispatchToProps = (dispatch) => bindActionCreators({}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Header);
